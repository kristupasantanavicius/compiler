#include <compiler/AST.h>
#include <compiler/Error.h>
#include <compiler/Optional.h>
#include <compiler/astDump.h>
#include <compiler/astGenerator.h>
#include <set>
#include <unordered_set>

namespace {

struct State {
  State(std::vector<std::string>& globalStrings,
        std::vector<astType*>& outputTypes,
        std::vector<astClass*>& newClasses,
        std::unordered_set<astType*>& types_without_error)
      : globalStrings(globalStrings),
        outputTypes(outputTypes),
        newClasses(newClasses),
        types_without_error(types_without_error) {}
  std::vector<std::string>& globalStrings;
  std::vector<astType*>& outputTypes;
  std::vector<astClass*>& newClasses;
  std::unordered_set<astType*>& types_without_error;
};

void generateScope(parsed::Scope& scope,
                   astScope* dst,
                   astClass* memberOf,
                   State& state);
void generateBlock(parsed::Block& block,
                   astScope* dstScope,
                   astBlock* dstBlock,
                   State& state);
astFunction* generateFunction(parsed::Function& function,
                              astScope* parentScope,
                              astClass* memberOf,
                              State& state);
astVariable* generateVariable(parsed::Variable& variable,
                              astScope* scope,
                              astClass* memberOf,
                              State& state);

astFunctionCall2* generateFunctionCall2(parsed::FunctionCall2& call,
                                        astScope* scope,
                                        State& state);

void generateClassThisVariable(astClass* aclass) {
  astVariable* variable = new astVariable();
  variable->location = aclass->location;
  variable->name = "this";
  variable->memberOf = aclass;

  astType* type = astWrapClassInType(aclass, aclass->location);
  type = astWrapTypeInMutableReference(type, aclass->location);
  variable->type = type;

  aclass->scope->thisVariable = variable;
}

astClass* generateClass(parsed::Class& aclass,
                        astScope* parentScope,
                        State& state) {
  astClass* dst = new astClass(aclass.name, parentScope);
  dst->location = aclass.location;
  generateScope(aclass.scope, dst->scope, dst, state);
  generateClassThisVariable(dst);
  dst->templatedClass = aclass.templatedClass;
  state.newClasses.push_back(dst);
  return dst;
}

astLiteral* generateLiteral(parsed::Literal& constant,
                            astScope* scope,
                            State& state);

astType* internalGenerateType(parsed::Type& type,
                              astScope* scope,
                              bool resolveType,
                              State& state) {
  std::vector<astType*>* resolvedTypes = &state.outputTypes;
  if (!resolveType) {
    resolvedTypes = nullptr;
  }
  astType* dst = astNewType(resolvedTypes, type.location, type.name, scope);
  if (type.literal) {
    dst->literal = generateLiteral(type.literal.getValue(), scope, state);
  }
  for (auto& subtype : type.subtypes) {
    dst->addSubtype(internalGenerateType(subtype, scope, false, state));
  }
  if (type.mutableReference) {
    dst->createMutableReference();
  }
  return dst;
}

astType* generateType(parsed::Type& type, astScope* scope, State& state) {
  return internalGenerateType(type, scope, true, state);
}

astExpression* generateExpression(parsed::Expression& inputExpr,
                                  astScope* scope,
                                  State& state);

astVariable* generateVariable(parsed::Variable& variable,
                              astScope* scope,
                              astClass* memberOf,
                              State& state) {
  astVariable* dst = new astVariable();
  dst->location = variable.location;
  dst->name = variable.name;
  dst->global = scope->owner.module != "";
  dst->memberOf = memberOf;
  if (variable.type) {
    dst->type = generateType(variable.type.getValue(), scope, state);
  }
  if (variable.initializeWithExpression) {
    dst->initializeWithExpression = generateExpression(
        variable.initializeWithExpression.getValue(), scope, state);
  }
  return dst;
}

astVariableRef2* generateVariableRef2(parsed::VariableRef2& ref,
                                      astScope* scope,
                                      State& state) {
  base::MaybeUnused(state);

  astVariableRef2* dst = new astVariableRef2();
  dst->location = ref.location;
  dst->scope = scope;
  dst->stringVariable = ref.name;
  return dst;
}

int findStringInVector(const std::vector<std::string>& vec,
                       const std::string& str) {
  for (int i = 0; i < (int)vec.size(); i++) {
    if (vec.at(i) == str) {
      return i;
    }
  }
  return -1;
}

astLiteral* generateLiteral(parsed::Literal& constant,
                            astScope* scope,
                            State& state) {
  astLiteral* dst = new astLiteral();
  dst->location = constant.location;
  dst->valueString = constant.valueString;
  dst->valueNumeric = constant.valueNumeric;
  switch (constant.type) {
    case parsed::LiteralType::Integer: {
      dst->type = astNewBasicType(astTypeName::Int32, constant.location);
    } break;
    case parsed::LiteralType::Float: {
      dst->type = astNewBasicType(astTypeName::Float32, constant.location);
    } break;
    case parsed::LiteralType::Bool: {
      // dst->type = astNewType(&state.outputTypes, constant.location, "bool",
      // scope);
      dst->type = astNewBasicType(astTypeName::Bool, constant.location);
    } break;
    case parsed::LiteralType::String: {
      // We have to let TypeCheck resolve these, since it will generate array
      // class instance for this array string.
      dst->type =
          astNewType(&state.outputTypes, constant.location, "[]", scope);
      dst->type->addSubtype(
          astNewType(nullptr, constant.location, "char", scope));
      // dst->type = astNewBasicType(astTypeName::Array, constant.location);
      // dst->type->addSubtype(astNewBasicType(astTypeName::Char,
      // constant.location));
      int index = findStringInVector(state.globalStrings, dst->valueString);
      if (index >= 0) {
        dst->globalStringIndex = index;
      } else {
        dst->globalStringIndex = state.globalStrings.size();
        state.globalStrings.push_back(dst->valueString);
      }
    } break;
    case parsed::LiteralType::Char: {
      dst->type = astNewBasicType(astTypeName::Char, constant.location);
    } break;
    case parsed::LiteralType::None:
      ASSERT(false);
  }
  ASSERT(dst->type);
  return dst;
}

astSymbol* generateSymbol(parsed::Symbol& symbol,
                          astScope* scope,
                          State& state) {
  astSymbol* dst = nullptr;
  if (auto ref = symbol.getVariableRef()) {
    dst = new astSymbol(generateVariableRef2(*ref, scope, state));
  } else if (auto call = symbol.getFunctionCall()) {
    dst = new astSymbol(generateFunctionCall2(*call, scope, state));
  } else if (auto constant = symbol.getConstant()) {
    dst = new astSymbol(generateLiteral(*constant, scope, state));
  } else {
    ASSERT(false);
  }

  dst->scope = scope;
  return dst;
}

int findOperatorPrecedence(parsed::Expression& expression,
                           const std::set<astOperator>& operators) {
  for (int i = 0; i < (int)expression.nodes.size(); i++) {
    parsed::ExpressionNode& node = expression.nodes.at(i);
    if (node.isOperator()) {
      astOperator op = node.getOperator().op;
      if (operators.find(op) != operators.end()) {
        return i;
      }
    }
  }
  return -1;
}

void operatorOrderingPass(parsed::Expression& expression,
                          const std::set<astOperator>& operators) {
  while (true) {
    int index = findOperatorPrecedence(expression, operators);
    if (index < 0) {
      break;
    }

    parsed::ExpressionNode* left = &expression.nodes.at(index - 1);
    parsed::ExpressionNode* nodeOperator = &expression.nodes.at(index);
    parsed::ExpressionNode* right = &expression.nodes.at(index + 1);

    parsed::Expression newScope;
    newScope.location = left->getLocation();
    newScope.nodes.push_back(*left);
    newScope.nodes.push_back(*nodeOperator);
    newScope.nodes.push_back(*right);

    auto iter = expression.nodes.begin() + index - 1;
    for (int i = 0; i < 3; i++) {
      iter = expression.nodes.erase(iter);
    }
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // !!!!!! [left, nodeOperator, right] pointers are invalid at this
    // point!!!!!!!
    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    expression.nodes.insert(iter, newScope);
  }

  expression.location.sanity();
}

struct Region {
  Region(int begin, int end) : begin(begin), end(end) {}
  int begin = -1;
  int end = -1;
};

// The idea is to group math regions, so that they are evaluated as a single
// node, eg: 10 == 4 + 4 + 7 + 3 && true becomes 10 == (4 + 4 + 7 + 3) && true
// 10 == 7 + 3 becomes 10 == (7 + 3)
Optional<Region> findMathOperatorGroup(parsed::Expression& expression) {
  int beginIndex = -1;
  for (int i = 0; i < (int)expression.nodes.size(); i++) {
    parsed::ExpressionNode& currentNode = expression.nodes.at(i);
    if (!currentNode.isOperator()) {
      continue;
    }
    astOperator op = currentNode.getOperator().op;
    if (operatorIsMath(op)) {
      if (beginIndex < 0) {
        beginIndex = i - 1;
      }
    } else {
      if (beginIndex >= 0) {
        int endIndex = i - 1;
        return Optional<Region>(Region(beginIndex, endIndex));
      }
    }
  }
  return {};
}

void groupMathOperators(parsed::Expression& expression) {
  while (auto maybeRegion = findMathOperatorGroup(expression)) {
    Region region = maybeRegion.getValue();
    int nodesToInclude = region.end - region.begin;
    parsed::Expression newScope;
    for (int j = 0; j <= nodesToInclude; j++) {
      auto iter = expression.nodes.begin() + region.begin;
      newScope.nodes.push_back(*iter);
      expression.nodes.erase(iter);
    }
    newScope.location = newScope.nodes.at(0).getLocation();
    auto iter = expression.nodes.begin() + region.begin;
    expression.nodes.insert(iter, newScope);
  }
}

astExpression* generateExpressionImpl(parsed::Expression& expression,
                                      astScope* scope,
                                      State& state);

astExpressionNode* generateExpressionNode(parsed::ExpressionNode& node,
                                          astScope* scope,
                                          State& state) {
  astExpressionNode* dst = nullptr;
  if (node.isSymbol()) {
    dst = new astExpressionNode(generateSymbol(node.getSymbol(), scope, state));
  } else if (node.isOperator()) {
    dst = new astExpressionNode(node.getOperator());
  } else if (node.isSubExpression()) {
    dst = new astExpressionNode(
        generateExpressionImpl(node.getSubExpression(), scope, state));
  } else {
    ASSERT(false);
  }

  if (ExpressionModifier* modifier = node.getModifier()) {
    dst->setModifier(*modifier);
  }

  if (parsed::Type* typeCast = node.getTypeCast()) {
    dst->setTypeCast(generateType(*typeCast, scope, state));
  }

  return dst;
}

astExpression* generateExpressionImpl(parsed::Expression& expression,
                                      astScope* scope,
                                      State& state) {
  astExpression* result = new astExpression(expression.location, nullptr);

  for (auto& node : expression.nodes) {
    astExpressionNode* newNode = generateExpressionNode(node, scope, state);
    result->nodes.push_back(newNode);
  }

  return result;
}

void groupNodesIntoScopes(parsed::Expression& expression) {
  while (expression.nodes.size() >= 3) {
    parsed::ExpressionNode* left = &expression.nodes.at(0);
    parsed::ExpressionNode* nodeOperator = &expression.nodes.at(1);
    parsed::ExpressionNode* right = &expression.nodes.at(2);

    parsed::Expression newScope;
    newScope.location = left->getLocation();
    newScope.nodes.push_back(*left);
    newScope.nodes.push_back(*nodeOperator);
    newScope.nodes.push_back(*right);

    auto iter = expression.nodes.begin();
    for (int i = 0; i < 3; i++) {
      iter = expression.nodes.erase(iter);
    }

    expression.nodes.insert(iter, newScope);
  }

  expression.location.sanity();
}

void modifyExpression(parsed::Expression& expression) {
  // EG:   5 < array.length()
  // GOOD  5 < (array.length())
  // BAD   (5 < array).length()
  static std::set<astOperator> firstPass = {
      astOperator::Dot,
  };
  operatorOrderingPass(expression, firstPass);

  // EG:   10 == 7 + 3 && true
  // GOOD  10 == (7 + 3) && true
  // BAD   (10 == 7) + 3 && true
  groupMathOperators(expression);

  // EG:    4 + 3 * 2
  // GOOD:  4 + (3 * 2)
  // BAD:   (4 + 3) * 2
  static std::set<astOperator> secondPass = {
      astOperator::Multiply,
      astOperator::Divide,
      astOperator::Modulo,
  };
  operatorOrderingPass(expression, secondPass);

  // EG:   true && 10 == 10
  // GOOD: true && (10 == 10)
  // BAD:  (true && 10) == 10
  static std::set<astOperator> thirdPass = {
      astOperator::CompareEquals, astOperator::CompareLess,
      astOperator::CompareMore,   astOperator::CompareEQLess,
      astOperator::CompareEQMore,
  };
  operatorOrderingPass(expression, thirdPass);

  groupNodesIntoScopes(expression);
}

astExpression* generateExpression(parsed::Expression& inputExpr,
                                  astScope* scope,
                                  State& state) {
  inputExpr.location.sanity();

  // Make a copy, since it will be modified.
  parsed::Expression expression = inputExpr;

  modifyExpression(expression);

  astExpression* dst = generateExpressionImpl(expression, scope, state);
  dst->location.sanity();
  return dst;
}

astStatement* generateVariableDecl(parsed::VariableDecl& decl,
                                   astScope* scope,
                                   State& state) {
  astVariableDecl* dst = new astVariableDecl(scope);
  dst->location = decl.location;
  dst->assignLocation = decl.assignLocation;
  dst->variable = generateVariable(decl.variable, scope, nullptr, state);
  if (decl.hasInitExpression) {
    dst->expression = generateExpression(decl.expression, scope, state);
  }
  scope->variables.push_back(dst->variable);
  return dst;
}

astVariableAssign* generateVariableAssign(parsed::VariableAssign& assign,
                                          astScope* scope,
                                          State& state) {
  astVariableAssign* dst = new astVariableAssign(scope);
  dst->location = assign.location;
  dst->target = generateExpression(assign.target, scope, state);
  dst->expression = generateExpression(assign.expression, scope, state);
  return dst;
}

astFunctionCall2* generateFunctionCall2(parsed::FunctionCall2& call,
                                        astScope* scope,
                                        State& state) {
  astFunctionCall2* dst = new astFunctionCall2();
  dst->location = call.location;
  dst->scope = scope;

  // To support constructors based on any type (eg: []i32() or Map<i32>()),
  // compiler has to first attempt to resolve the function call as a type.
  // If type is resolved, then it means its a constructor. If it fails, it means
  // that its a regular function call and the function should be looked up by
  // the string token of the type.
  dst->type = generateType(call.type, scope, state);
  ASSERT(dst->type);
  state.types_without_error.insert(dst->type);

  for (auto& argument : call.arguments) {
    dst->arguments.push_back(generateExpression(argument, scope, state));
  }
  for (auto& templateArgument : call.templateArguments) {
    dst->templateArguments.push_back(
        generateType(templateArgument, scope, state));
  }
  return dst;
}

astIf* generateIf(parsed::StatementIf& anIf, astScope* scope, State& state) {
  astIf* dst = astNewIf(scope);
  dst->location = anIf.location;
  dst->expression = generateExpression(anIf.expression, scope, state);

  generateScope(anIf.scope, dst->scope, nullptr, state);
  generateBlock(anIf.block, dst->scope, dst->block, state);

  dst->hasElseBlock = anIf.hasElseBlock;
  dst->elseBlockIsJustIf = anIf.elseBlockIsJustIf;
  generateScope(anIf.elseScope, dst->elseScope, nullptr, state);
  generateBlock(anIf.elseBlock, dst->elseScope, dst->elseBlock, state);
  return dst;
}

astWhile* generateWhile(parsed::StatementWhile& awhile,
                        astScope* scope,
                        State& state) {
  astWhile* dst = astNewWhile(scope);
  dst->location = awhile.location;
  dst->expression = generateExpression(awhile.expression, scope, state);
  generateScope(awhile.scope, dst->scope, nullptr, state);
  generateBlock(awhile.block, dst->scope, dst->block, state);
  return dst;
}

astReturn* generateReturn(parsed::StatementReturn& ret,
                          astScope* scope,
                          State& state) {
  astReturn* dst = new astReturn(ret.location, scope);
  if (!ret.noExpression) {
    dst->expression = generateExpression(ret.expression, scope, state);
  }
  return dst;
}

astStatementExpression* generateStatementExpression(
    parsed::StatementExpression& statement,
    astScope* scope,
    State& state) {
  astStatementExpression* dst = new astStatementExpression();
  dst->expression = generateExpression(statement.expression, scope, state);
  dst->location = dst->expression->location;
  return dst;
}

astStatementContinue* generateStatementContinue(
    parsed::StatementContinue& statement,
    astScope* scope,
    State& state) {
  base::MaybeUnused(scope);
  base::MaybeUnused(state);

  astStatementContinue* dst = new astStatementContinue();
  dst->location = statement.location;
  return dst;
}

void generateBlock(parsed::Block& block,
                   astScope* dstScope,
                   astBlock* dstBlock,
                   State& state) {
  struct Visitor : parsed::StatementVisitor {
    Visitor(State& state, astScope* dstScope)
        : state(state), dstScope(dstScope) {}
    State& state;
    astScope* dstScope;
    astStatement* output = nullptr;
    void visit(parsed::VariableDecl& decl) {
      output = generateVariableDecl(decl, dstScope, state);
    }
    void visit(parsed::VariableAssign& assign) {
      output = generateVariableAssign(assign, dstScope, state);
    }
    void visit(parsed::StatementIf& anIf) {
      output = generateIf(anIf, dstScope, state);
    }
    void visit(parsed::StatementWhile& awhile) {
      output = generateWhile(awhile, dstScope, state);
    }
    void visit(parsed::StatementReturn& ret) {
      output = generateReturn(ret, dstScope, state);
    }
    void visit(parsed::StatementExpression& statement) {
      output = generateStatementExpression(statement, dstScope, state);
    }
    void visit(parsed::StatementBreak& statement) {
      output = new astStatementBreak(statement.location);
    }
    void visit(parsed::StatementContinue& statement) {
      output = generateStatementContinue(statement, dstScope, state);
    }
  };
  for (auto& statement : block.statements) {
    Visitor visitor(state, dstScope);
    std::visit(visitor, statement);
    ASSERT(visitor.output);
    dstBlock->statements.push_back(visitor.output);
  }
}

astFunction* generateFunction(parsed::Function& function,
                              astScope* parentScope,
                              astClass* memberOf,
                              State& state) {
  astFunction* dst = new astFunction(function.name, parentScope);
  dst->location = function.location;

  if (function.name != "constructor") {
    // Constructors are 'static' function and so not part of members
    dst->memberOf = memberOf;
  }

  for (auto& parameter : function.parameters) {
    astVariable* variable =
        generateVariable(parameter, dst->scope, nullptr, state);
    dst->parameters.push_back(variable);
    dst->scope->variables.push_back(variable);
  }

  dst->returnType = generateType(function.returnType, dst->scope, state);

  dst->isExternFunction = function.isExternFunction;
  dst->isAPIFunction = function.isAPIFunction;
  dst->templatedFunction = function.templatedFunction;

  generateBlock(function.block, dst->scope, dst->block, state);

  return dst;
}

astTypeAlias* generateTypeAlias(parsed::TypeAlias& alias,
                                astScope* scope,
                                State& state) {
  astTypeAlias* dst = new astTypeAlias();
  dst->location = alias.location;
  dst->name = alias.name;
  dst->type = generateType(alias.type, scope, state);
  return dst;
}

void generateScope(parsed::Scope& scope,
                   astScope* dst,
                   astClass* memberOf,
                   State& state) {
  for (auto& aclass : scope.classes) {
    if (aclass.templatedClass) {
      dst->templatedClasses.push_back(aclass);
    } else {
      dst->classes.push_back(generateClass(aclass, dst, state));
    }
  }
  for (auto& variable : scope.variables) {
    dst->variables.push_back(generateVariable(variable, dst, memberOf, state));
  }
  for (auto& function : scope.functions) {
    if (function.templatedFunction) {
      dst->templatedFunctions.push_back(function);
    } else {
      dst->functions.push_back(
          generateFunction(function, dst, memberOf, state));
    }
  }
  for (auto& alias : scope.typeAliases) {
    dst->typeAliases.push_back(generateTypeAlias(alias, dst, state));
  }
}

}  // namespace

astClass* astGenerateClass(parsed::Class& aclass,
                           astScope* scope,
                           std::vector<std::string>& globalStrings,
                           std::vector<astType*>& outputTypes,
                           std::vector<astClass*>& newClasses,
                           std::unordered_set<astType*>& types_without_error) {
  State state(globalStrings, outputTypes, newClasses, types_without_error);
  astClass* dst = generateClass(aclass, scope, state);
  return dst;
}

astFunction* astGenerateFunction(
    parsed::Function& function,
    astScope* scope,
    astClass* memberOf,
    std::vector<std::string>& globalStrings,
    std::vector<astType*>& outputTypes,
    std::vector<astClass*>& newClasses,
    std::unordered_set<astType*>& types_without_error) {
  State state(globalStrings, outputTypes, newClasses, types_without_error);
  astFunction* dst = generateFunction(function, scope, memberOf, state);
  return dst;
}

SyntaxTree* generateAst(const std::string& module_name,
                        ASTCache& cache,
                        parsed::Scope& scope) {
  SyntaxTree* dst = new SyntaxTree(module_name);
  State state(cache.globalStrings, dst->allTypes, dst->allClasses,
              cache.types_without_error);
  generateScope(scope, dst->scope, nullptr, state);
  return dst;
}
