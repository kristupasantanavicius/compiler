#pragma once

#include <compiler/AST.h>
#include <string>
#include <vector>

bool runTypeCheck(SyntaxTree* ast, ASTCache& cache, astError* error);
void typeCheckTypes(SyntaxTree* ast,
                    ASTCache& cache,
                    std::vector<astError>& errors);
void resolveClassConstructors(SyntaxTree* ast,
                              ASTCache& cache,
                              std::vector<astError>& errors);
// void collapseTypeAliases(AST * ast);
void instantiateArrayClasses(SyntaxTree* ast, ASTCache& cache);
