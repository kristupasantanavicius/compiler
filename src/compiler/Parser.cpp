#include <compiler/Format.h>
#include <compiler/Parser.h>
#include <compiler/astDump.h>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <sstream>

namespace {
const std::map<std::string, Parser::Directive> sDirectiveMapping = {
    {"load", Parser::Directive::LoadFile},
    {"constructor", Parser::Directive::Constructor},
    {"copyConstructor", Parser::Directive::CopyConstructor},
    {"destructor", Parser::Directive::Destructor},
    {"char", Parser::Directive::Char},
};
}

bool Parser::parse(parsed::Scope& scope, astError* error) {
  try {
    mToken = LexerNextToken(mLexer);
    doParsing(scope);
    consume(TokenType::EndOfFile);
  } catch (ParserException& e) {
    *error = e.compilerError;
    return false;
  }
  return true;
}

void Parser::consume(TokenType type, std::string msg) {
  if (mToken.type != type) {
    if (msg.empty()) {
      msg = TokenTypeString(type);
    }
    makeError(msg);
  }
  mToken = LexerNextToken(mLexer);
}

void Parser::consumeUnchecked() {
  mToken = LexerNextToken(mLexer);
}

bool Parser::tryConsume(TokenType type) {
  if (testTokenType(type)) {
    consume(type);
    return true;
  }
  return false;
}

void Parser::makeError(const std::string& expected,
                       astLocation* actualLocation) {
  astLocation location = actualLocation ? *actualLocation : getTokenLocation();
  std::string msg =
      FORMAT("expected [@], but got [@]", expected, getTokenString());
  throw ParserException(astError(location, msg));
}

int Parser::getTokenLineNumber() {
  int lineNumber = 0;
  int charInLine = 0;
  LexerGetLineNumber(mLexer, mToken, lineNumber, charInLine);
  return lineNumber;
}

astLocation Parser::getTokenLocation() {
  astLocation location;
  LexerGetLineNumber(mLexer, mToken, location.line, location.charNumber);
  location.fileName = &mLexer->fileName;
  return location;
}

Parser::ParserState Parser::SaveState() {
  return {mLexer->scanner->currentPosition(), mToken};
}

void Parser::RestoreState(const ParserState& state) {
  mLexer->scanner->RestorePosition(state.scanner_position);
  mToken = state.token;
}

void Parser::doParsing(parsed::Scope& parsedScope) {
  while (parseCommand() || parseScope(parsedScope)) {
  }
}

std::string Parser::getDirectiveString(Directive directive) {
  for (auto iter = sDirectiveMapping.begin(); iter != sDirectiveMapping.end();
       ++iter) {
    if (iter->second == directive) {
      return iter->first;
    }
  }
  ASSERT(false);
  return "directive_no_string";
}

Parser::Directive Parser::parseDirective() {
  if (!tryConsume(TokenType::Hash)) {
    return Directive::None;
  }
  std::string strDirective = getTokenString();
  consume(TokenType::Identifier);

  auto directiveIter = sDirectiveMapping.find(strDirective);
  if (directiveIter == sDirectiveMapping.end()) {
    std::stringstream stream;
    stream << "directive: ";
    bool writeSeparator = false;
    for (auto iter = sDirectiveMapping.begin(); iter != sDirectiveMapping.end();
         ++iter) {
      if (!writeSeparator) {
        writeSeparator = true;
      } else {
        stream << " | ";
      }
      stream << iter->first;
    }
    makeError(stream.str());
    return Directive::None;
  }

  if (directiveIter->second == Directive::None) {
    makeError("Directive::None is not supposed to trigger");
  }

  return directiveIter->second;
}

bool Parser::parseCommand() {
  Directive directive = parseDirective();
  if (directive == Directive::None) {
    return false;
  }

  if (directive == Directive::LoadFile) {
    std::string fileName = getTokenString();
    consume(TokenType::String);
    mCommandsLoadFile.push_back(fileName);
    return true;
  } else {
    makeError("load file directive #" +
              getDirectiveString(Directive::LoadFile));
  }
  return false;
}

bool Parser::parseScope(parsed::Scope& parsedScope) {
  bool something = false;
  while (parseClass(parsedScope) || parseFunction(parsedScope) ||
         parseScopeVariable(parsedScope) || parseTypeAlias(parsedScope) ||
         parseScopeImpl(parsedScope)) {
    something = true;
  }
  return something;
}

bool Parser::parseScopeImpl(parsed::Scope& parsedScope) {
  if (testTokenType(TokenType::Identifier) &&
      peekToken().type == TokenType::Equals) {
    std::string identifier = getTokenString();
    astLocation location = getTokenLocation();
    consume(TokenType::Identifier);
    consume(TokenType::Equals);
    if (!parseFunction(parsedScope, &identifier, location) &&
        !parseClass(parsedScope, &identifier, location)) {
      makeError("struct or function");
      return false;
    }
    return true;
  }
  return false;
}

bool Parser::parseClassDirective(parsed::Class& aclass) {
  Directive directive = parseDirective();
  if (directive == Directive::None) {
    return false;
  }
  std::vector<Directive> validDirectives = {
      Directive::Constructor,
      Directive::CopyConstructor,
      Directive::Destructor,
  };
  auto iter =
      std::find(validDirectives.begin(), validDirectives.end(), directive);
  if (iter == validDirectives.end()) {
    std::stringstream stream;
    bool comma = false;
    for (auto& d : validDirectives) {
      if (!comma) {
        comma = true;
      } else {
        stream << ", ";
      }
      stream << getDirectiveString(d);
    }
    makeError("a class directive: " + std::string("#") + stream.str());
    return false;
  }

  switch (directive) {
    case Directive::Constructor: {
      parsed::Function function;
      function.location = getTokenLocation();
      function.name = "constructor";
      parseFunctionWithoutIdentifier(function);
      aclass.scope.functions.push_back(function);
      aclass.constructors.push_back(aclass.scope.functions.size() - 1);
    } break;

    case Directive::CopyConstructor: {
      parsed::Function function;
      function.location = getTokenLocation();
      function.name = "copyConstructor";
      parseFunctionWithoutIdentifier(function);
      aclass.scope.functions.push_back(function);
    } break;

    case Directive::Destructor: {
      parsed::Function function;
      function.location = getTokenLocation();
      function.name = "destructor";
      parseFunctionWithoutIdentifier(function);
      aclass.scope.functions.push_back(function);
    } break;

    default:
      ASSERT(false);
  }

  return true;
}

bool Parser::parseClass(parsed::Scope& parsedScope,
                        std::string* identifier,
                        astLocation location) {
  if (!tryConsume(TokenType::KeywordClass)) {
    return false;
  }

  parsed::Class parsedClass;
  if (!identifier) {
    parsedClass.location = getTokenLocation();
    parsedClass.name = getTokenString();
    consume(TokenType::Identifier, "new class name");
  } else {
    parsedClass.location = location;
    parsedClass.name = *identifier;
  }

  while (tryConsume(TokenType::Dollar)) {
    parsedClass.templatedClass = true;
    parsedClass.templateParameters.push_back(getTokenString());
    consume(TokenType::Identifier);
  }

  consume(TokenType::BodyOpen);
  do {
    parseScope(parsedClass.scope);
  } while (parseClassDirective(parsedClass));
  consume(TokenType::BodyClose, "} to close class body");

  parsedScope.classes.push_back(parsedClass);

  return true;
}

bool Parser::parseFunction(parsed::Scope& parsedScope,
                           std::string* identifier,
                           astLocation location) {
  if (!tryConsume(TokenType::KeywordFunction)) {
    return false;
  }

  parsed::Function parsedFunction;
  if (!identifier) {
    parsedFunction.location = getTokenLocation();
    parsedFunction.name = getTokenString();
    consume(TokenType::Identifier, "new function name");
  } else {
    parsedFunction.location = location;
    parsedFunction.name = *identifier;
  }

  parseFunctionWithoutIdentifier(parsedFunction);

  parsedScope.functions.push_back(parsedFunction);
  return true;
}

void Parser::parseFunctionWithoutIdentifier(parsed::Function& parsedFunction) {
  consume(TokenType::ParamsOpen);
  while (parseFunctionParameter(parsedFunction)) {
    if (!tryConsume(TokenType::Coma)) {
      break;  // Not a comma, that means the end of argument list
    }
  }
  consume(TokenType::ParamsClose);

  if (tryConsume(TokenType::Minus)) {
    consume(TokenType::More);
    Optional<parsed::Type> maybeType = parseType();
    if (maybeType) {
      parsedFunction.returnType = maybeType.getValue();
    } else {
      makeError("a type");
    }
  } else {
    parsed::Type type;
    type.location = getTokenLocation();
    type.name = "void";
    parsedFunction.returnType = type;
  }

  // TEMPLATE
  while (tryConsume(TokenType::Dollar)) {
    parsedFunction.templatedFunction = true;
    parsedFunction.templateParameters.push_back(getTokenString());
    consume(TokenType::Identifier);
  }

  if (tryConsume(TokenType::KeywordExtern)) {
    parsedFunction.isExternFunction = true;
  }
  if (tryConsume(TokenType::KeywordAPI)) {
    parsedFunction.isAPIFunction = true;
  }
  if (tryConsume(TokenType::KeywordPure)) {
    parsedFunction.isPure = true;
  }

  if (parsedFunction.isExternFunction) {
    consume(TokenType::Semicolon, "';' to end extern function definition");
  } else {
    consume(TokenType::BodyOpen);
    parseStatements(parsedFunction.scope, parsedFunction.block);
    if (!tryConsume(TokenType::BodyClose)) {
      makeError(
          "a valid statement: function call, variable declaration, etc..");
    }
  }
}

bool Parser::parseFunctionParameter(parsed::Function& parsedFunction) {
  Optional<parsed::Variable> maybeVariable = parseVariable();
  if (!maybeVariable) {
    return false;
  }
  parsed::Variable& variable = maybeVariable.getValue();
  parsedFunction.scope.variables.push_back(variable);
  parsedFunction.parameters.push_back(variable);
  return true;
}

bool Parser::parseScopeVariable(parsed::Scope& parsedScope) {
  Optional<parsed::Variable> variable = parseVariable();
  if (variable) {
    if (tryConsume(TokenType::Equals)) {
      Optional<parsed::Expression> expression = parseExpression();
      if (!expression) {
        makeError("an expression");
      }
      variable.getValue().initializeWithExpression = expression.getValue();
    }
    parsedScope.variables.push_back(variable.getValue());
    consume(TokenType::Semicolon);
    return true;
  } else {
    return false;
  }
}

Optional<parsed::Variable> Parser::parseVariable(bool typeOptional) {
  if (!(testTokenType(TokenType::Identifier) &&
        peekToken().type == TokenType::Colon)) {
    return {};
  }

  parsed::Variable variable;
  variable.location = getTokenLocation();
  variable.name = getTokenString();
  consume(TokenType::Identifier);

  consume(TokenType::Colon);

  Optional<parsed::Type> type = parseType();
  if (type) {
    variable.type = type.getValue();
  } else {
    if (!typeOptional) {
      makeError("a type");
    }
  }

  return variable;
}

bool newStatementWithoutSemicolon(parsed::Statement& statement) {
  bool value = false;
  struct Visitor : parsed::StatementVisitor {
    Visitor(bool& out) : out(out) {}
    bool& out;
    void visit(parsed::VariableDecl&) { out = false; }
    void visit(parsed::VariableAssign&) { out = false; }
    void visit(parsed::StatementReturn&) { out = false; }
    void visit(parsed::StatementIf&) { out = true; }
    void visit(parsed::StatementWhile&) { out = true; }
    void visit(parsed::StatementExpression&) { out = false; }
    void visit(parsed::StatementBreak&) { out = false; }
    void visit(parsed::StatementContinue&) { out = false; }
  };
  std::visit(Visitor(value), statement);
  return value;
}

void Parser::parseStatements(parsed::Scope& scope, parsed::Block& block) {
  base::MaybeUnused(scope);

  while (auto statement = parseStatement(block)) {
    if (newStatementWithoutSemicolon(statement.getValue()) == false) {
      consume(TokenType::Semicolon);
    }
  }
}

Optional<parsed::Statement> Parser::parseStatement(parsed::Block& block) {
  Optional<parsed::Statement> statement;
  if (auto decl = parseVariableDecl()) {
    statement = Optional<parsed::Statement>(decl.getValue());
  } else if (auto anIf = parseIfStatement()) {
    statement = Optional<parsed::Statement>(anIf.getValue());
  } else if (auto awhile = parseWhileStatement()) {
    statement = Optional<parsed::Statement>(awhile.getValue());
  } else if (auto ret = parseReturnStatement()) {
    statement = Optional<parsed::Statement>(ret.getValue());
  } else if (auto aBreak = parseBreakStatement()) {
    statement = Optional<parsed::Statement>(aBreak.getValue());
  } else if (auto aContinue = parseContinueStatement()) {
    statement = Optional<parsed::Statement>(aContinue.getValue());
  }

  if (!statement) {
    Optional<parsed::Expression> expression = parseExpression();
    if (expression) {
      if (testTokenType(TokenType::Equals)) {
        parsed::VariableAssign assign;
        assign.location = getTokenLocation();
        consume(TokenType::Equals);

        Optional<parsed::Expression> assignExpression = parseExpression();
        if (!assignExpression) {
          makeError("an expression");
        }
        assign.target = expression.getValue();
        assign.expression = assignExpression.getValue();
        statement = Optional<parsed::Statement>(assign);
      } else {
        parsed::StatementExpression expressionStatement;
        expressionStatement.expression = expression.getValue();
        statement = Optional<parsed::Statement>(expressionStatement);
      }
    }
  }

  if (statement) {
    block.statements.push_back(statement.getValue());
  }
  return statement;
}

Optional<parsed::StatementReturn> Parser::parseReturnStatement() {
  if (!testTokenType(TokenType::KeywordReturn)) {
    return {};
  }

  parsed::StatementReturn ret;
  ret.location = getTokenLocation();
  consume(TokenType::KeywordReturn);

  Optional<parsed::Expression> expression = parseExpression();
  if (expression) {
    ret.expression = expression.getValue();
  } else {
    ret.noExpression = true;
  }

  return ret;
}

Optional<parsed::StatementBreak> Parser::parseBreakStatement() {
  if (!testTokenType(TokenType::KeywordBreak)) {
    return {};
  }
  parsed::StatementBreak statement;
  statement.location = getTokenLocation();
  consume(TokenType::KeywordBreak);
  return statement;
}

Optional<parsed::StatementContinue> Parser::parseContinueStatement() {
  if (!testTokenType(TokenType::KeywordContinue)) {
    return {};
  }
  parsed::StatementContinue statement;
  statement.location = getTokenLocation();
  consume(TokenType::KeywordContinue);
  return statement;
}

Optional<parsed::StatementIf> Parser::parseIfStatement() {
  if (!testTokenType(TokenType::KeywordIf)) {
    return {};
  }

  parsed::StatementIf anIf;
  anIf.location = getTokenLocation();
  consume(TokenType::KeywordIf);

  consume(TokenType::ParamsOpen);

  if (auto expression = parseExpression()) {
    anIf.expression = expression.getValue();
  } else {
    makeError("an expression for 'if' statement");
  }

  consume(TokenType::ParamsClose);

  consume(TokenType::BodyOpen);
  parseStatements(anIf.scope, anIf.block);
  consume(TokenType::BodyClose, "a statement or '}'");

  if (tryConsume(TokenType::KeywordElse)) {
    anIf.hasElseBlock = true;
    if (testTokenType(TokenType::KeywordIf)) {
      // ELSE-IF combination. In such a case, the else block contains a single
      // IF statement inside it.
      Optional<parsed::Statement> statement = parseStatement(anIf.elseBlock);
      ASSERT(statement);
      // This is super hacky, since i don't currently know how to check for
      // specific type!
      struct Visitor : parsed::StatementVisitor {
        void visit(parsed::VariableDecl&) { ASSERT(false); }
        void visit(parsed::VariableAssign&) { ASSERT(false); }
        void visit(parsed::StatementReturn&) { ASSERT(false); }
        void visit(parsed::StatementIf&) {}  // If is the only valid statement!
        void visit(parsed::StatementWhile&) { ASSERT(false); }
        void visit(parsed::StatementExpression&) { ASSERT(false); }
        void visit(parsed::StatementBreak&) { ASSERT(false); }
        void visit(parsed::StatementContinue&) { ASSERT(false); }
      };
      std::visit(Visitor(), statement.getValue());
      anIf.elseBlockIsJustIf = true;
    } else {
      consume(TokenType::BodyOpen);
      parseStatements(anIf.elseScope, anIf.elseBlock);
      consume(TokenType::BodyClose);
    }
  }

  return anIf;
}

Optional<parsed::StatementWhile> Parser::parseWhileStatement() {
  if (!testTokenType(TokenType::KeywordWhile)) {
    return {};
  }

  parsed::StatementWhile awhile;
  awhile.location = getTokenLocation();
  consume(TokenType::KeywordWhile);
  consume(TokenType::ParamsOpen);

  if (auto expression = parseExpression()) {
    awhile.expression = expression.getValue();
  } else {
    makeError("an expression for 'while' statement");
  }

  consume(TokenType::ParamsClose);
  consume(TokenType::BodyOpen);
  parseStatements(awhile.scope, awhile.block);
  consume(TokenType::BodyClose, "a statement or '}'");

  return awhile;
}

Optional<parsed::VariableDecl> Parser::parseVariableDecl() {
  Optional<parsed::Variable> variable = parseVariable(true);
  if (!variable) {
    return {};
  }
  parsed::VariableDecl decl;
  decl.location = variable.getValue().location;
  decl.variable = variable.getValue();

  if (testTokenType(TokenType::Equals)) {
    decl.assignLocation = getTokenLocation();
    consume(TokenType::Equals);
    Optional<parsed::Expression> expression = parseExpression();
    if (expression) {
      decl.hasInitExpression = true;
      decl.expression = expression.getValue();
    } else {
      makeError("an expression");
    }
  }

  return decl;
}

#if 0
Optional<parsed::VariableAssign>
Parser::parseVariableAssign()
{
	Optional<parsed::VariableRef> ref = parseVariableRef();
	if (!ref) {
		return { };
	}

	if (!tryConsume(TokenType::Equals)) {
		// DO WE NEED ASSERT HERE?
		ASSERT(false); // WTF, NEED TO rework this. Parser should instead lookahead and decide if variable assign is possible!!!
		return { };
	}

	parsed::VariableAssign assign;
	assign.variable = ref.getValue();
	assign.location = assign.variable.location;

	Optional<parsed::Expression> expression = parseExpression();
	if (!expression) {
		makeError("an expression");
	}
	assign.expression = expression.getValue();

	return assign;
}
#endif

bool Parser::parseOperator(astOperator& op, astLocation& opLocation) {
  op = astOperator::None;
  opLocation = getTokenLocation();
  if (tryConsume(TokenType::Dot)) {
    op = astOperator::Dot;
  } else if (tryConsume(TokenType::Plus)) {
    op = astOperator::Plus;
  } else if (tryConsume(TokenType::Minus)) {
    op = astOperator::Minus;
  } else if (tryConsume(TokenType::Star)) {
    op = astOperator::Multiply;
  } else if (tryConsume(TokenType::Slash)) {
    op = astOperator::Divide;
  } else if (tryConsume(TokenType::Modulo)) {
    op = astOperator::Modulo;
  } else if (testTokenType(TokenType::Equals) &&
             peekToken().type == TokenType::Equals) {
    consume(TokenType::Equals);
    consume(TokenType::Equals);
    op = astOperator::CompareEquals;
  } else if (testTokenType(TokenType::Exclamation) &&
             peekToken().type == TokenType::Equals) {
    consume(TokenType::Exclamation);
    consume(TokenType::Equals);
    op = astOperator::CompareNotEquals;
  } else if (tryConsume(TokenType::Less)) {
    if (tryConsume(TokenType::Equals)) {
      op = astOperator::CompareEQLess;
    } else {
      op = astOperator::CompareLess;
    }
  } else if (tryConsume(TokenType::More)) {
    if (tryConsume(TokenType::Equals)) {
      op = astOperator::CompareEQMore;
    } else {
      op = astOperator::CompareMore;
    }
  } else if (testTokenType(TokenType::Ampersand) &&
             peekToken().type == TokenType::Ampersand) {
    consume(TokenType::Ampersand);
    consume(TokenType::Ampersand);
    op = astOperator::LogicalAND;
  } else if (testTokenType(TokenType::Pipe) &&
             peekToken().type == TokenType::Pipe) {
    consume(TokenType::Pipe);
    consume(TokenType::Pipe);
    op = astOperator::LogicalOR;
  }
  if (op != astOperator::None) {
    return true;
  } else {
    return false;
  }
}

Optional<parsed::Type> Parser::parseTypeCast() {
  if (!tryConsume(TokenType::KeywordAs)) {
    return {};
  } else {
    Optional<parsed::Type> typeCast = parseType();
    if (!typeCast) {
      makeError("type for cast");
    }
    return typeCast;
  }
}

void Parser::parseTypeCast(parsed::ExpressionNode& node) {
  if (auto typeCast = parseTypeCast()) {
    node.setTypeCast(typeCast.getValue());
  }
}

Optional<parsed::Expression> Parser::parseExpression() {
  astLocation initialLocation = getTokenLocation();

  parsed::Expression expression;
  expression.location = initialLocation;

  Optional<parsed::Symbol> left = parseSymbol();
  if (left) {
    parsed::ExpressionNode node(left.getValue());
    parseTypeCast(node);
    expression.nodes.push_back(node);
  }

  astOperator op = astOperator::None;
  astLocation opLocation;

  bool firstOperator = true;

  while (parseOperator(op, opLocation)) {
    Optional<ExpressionModifier> modifier;
    bool addOperator = true;

    if (firstOperator) {
      firstOperator = false;
      if (!left) {
        if (op == astOperator::Minus) {
          modifier =
              ExpressionModifier(opLocation, ExpressionModifierType::Negative);
          addOperator = false;
        } else {
          makeError("+/-", &expression.location);
        }
      }
    }

    if (!modifier) {
      if (testTokenType(TokenType::Minus)) {
        modifier = ExpressionModifier(getTokenLocation(),
                                      ExpressionModifierType::Negative);
        consume(TokenType::Minus);
      }
    }

    if (addOperator) {
      expression.nodes.push_back(
          parsed::ExpressionNode(astOperatorInstance(op, opLocation)));
    }

    Optional<parsed::Symbol> right = parseSymbol();
    if (!right) {
      makeError("a symbol");
    }
    parsed::ExpressionNode rightNode(right.getValue());
    parseTypeCast(rightNode);
    if (modifier) {
      rightNode.setModifier(modifier.getValue());
    }
    expression.nodes.push_back(rightNode);
  }

  if (expression.nodes.empty()) {
    return {};
  }

  return expression;

#if 0
	Optional<parsed::Symbol> left = parseSymbol();
	if (!left) {
		return { };
	}

	astOperator op = astOperator::None;
	astLocation opLocation;

	if (!parseOperator(op, opLocation)) {
		return Optional<parsed::Expression>(parsed::createExpressionLeaf(left.getValue()));
	}

	Optional<parsed::Expression> expressionRight = parseExpression();
	if (!expressionRight) {
		makeError("an expression");
	}

	parsed::ExpressionNode root = parsed::ExpressionNode();
	root.location = opLocation;
	root.operatorToken = op;
	root.left() = parsed::createExpressionLeaf(left.getValue());
	root.right() = expressionRight.getValue();
    return Optional<parsed::Expression>(root);
#endif
}

Optional<parsed::Literal> Parser::parseStringConstant() {
  bool character = false;
  Directive directive = parseDirective();
  if (directive != Directive::None) {
    if (directive != Directive::Char) {
      makeError("#char directive");
    }
    character = true;
  }
  if (testTokenType(TokenType::String)) {
    parsed::LiteralType type = parsed::LiteralType::String;
    if (character) {
      type = parsed::LiteralType::Char;
      if (getTokenString().size() > 1) {
        makeError("#char directive requires a single character");
      }
    }
    parsed::Literal constant(type);
    constant.location = getTokenLocation();
    constant.valueString = getTokenString();
    consume(TokenType::String);

    return Optional<parsed::Literal>(constant);
  } else {
    if (character) {
      makeError("a string to create char");
    }
  }
  return {};
}

Optional<parsed::Literal> Parser::parseIntegerLiteral() {
  if (testTokenType(TokenType::Number) ||
      (testTokenType(TokenType::Minus) &&
       peekToken().type == TokenType::Number)) {
    bool negative = false;
    if (tryConsume(TokenType::Minus)) {
      negative = true;
    }

    parsed::Literal constant(mToken.floating_number
                                 ? parsed::LiteralType::Float
                                 : parsed::LiteralType::Integer);
    constant.location = getTokenLocation();
    constant.valueString = getTokenString();
    try {
      constant.valueNumeric = std::stoi(constant.valueString);
      if (negative) {
        constant.valueNumeric = -constant.valueNumeric;
      }
    } catch (...) {
      makeError(
          "internal compiler error: failed to convert from string to numeric "
          "value");
    }
    consume(TokenType::Number);

    return Optional<parsed::Literal>(constant);
  }
  return {};
}

Optional<parsed::Symbol> Parser::parseSymbol() {
  if (auto integer = parseIntegerLiteral()) {
    return Optional<parsed::Symbol>(integer.getValue());
  } else if (testTokenType(TokenType::KeywordTrue) ||
             testTokenType(TokenType::KeywordFalse)) {
    parsed::Literal constant(parsed::LiteralType::Bool);
    constant.location = getTokenLocation();
    constant.valueString = getTokenString();
    if (tryConsume(TokenType::KeywordTrue)) {
      constant.valueNumeric = 1;
    } else if (tryConsume(TokenType::KeywordFalse)) {
      constant.valueNumeric = 0;
    } else {
      ASSERT(false);
    }

    return Optional<parsed::Symbol>(constant);
  } else if (auto string = parseStringConstant()) {
    return Optional<parsed::Symbol>(string.getValue());
  }

  else if (auto symbol = parseVariableOrFunctionCall()) {
    return symbol;
  }

  return {};
}

Optional<parsed::Symbol> Parser::parseVariableOrFunctionCall() {
  ParserState type_state = SaveState();
  Optional<parsed::Type> maybe_type = parseType(false);
  if (!maybe_type) {
    RestoreState(type_state);
    return {};
  }

  const parsed::Type& type = maybe_type.getValue();

  if (!tryConsume(TokenType::ParamsOpen)) {
    if (!type.subtypes.empty()) {
      makeError("a symbol");
    }
    parsed::VariableRef2 ref;
    ref.location = type.location;
    ref.name = type.name;
    return Optional<parsed::Symbol>(ref);
  }

#if 0
  // TEMPLATE
  // We have to save the state and restore it in case its not actually a
  // function. It could be a comparison operator, eg:
  // if (counter < foobar()) {}
  ParserState saved_state = SaveState();
  bool template_fail = false;
  std::vector<parsed::Type> template_arguments;
  if (tryConsume(TokenType::Less)) {
    do {
      Optional<parsed::Type> templateArgument = parseType();
      if (templateArgument) {
        template_arguments.push_back(templateArgument.getValue());
      } else {
        template_fail = true;
      }
    } while (tryConsume(TokenType::Coma));
    if (!tryConsume(TokenType::More)) {
      template_fail = true;
    }
  }

  if (template_fail) {
    RestoreState(saved_state);
  }

  if (template_fail || !tryConsume(TokenType::ParamsOpen)) {
    parsed::VariableRef2 ref;
    ref.location = location;
    ref.name = identifier;
    return ref;
  }
#endif

  parsed::FunctionCall2 call;
  call.location = type.location;
  // call.name = identifier;
  call.type = type;
  call.templateArguments = type.subtypes;
  while (auto expression = parseExpression()) {
    call.arguments.push_back(expression.getValue());
    if (!tryConsume(TokenType::Coma)) {
      break;
    }
  }
  consume(TokenType::ParamsClose);
  return Optional<parsed::Symbol>(call);
}

Optional<parsed::Type> Parser::parseType(bool with_errors) {
  parsed::Type type;

  if (tryConsume(TokenType::KeywordMut)) {
    type.mutableReference = true;
  }

  if (testTokenType(TokenType::BracketOpen) &&
      peekToken().type == TokenType::BracketClose) {
    // ARRAY
    type.location = getTokenLocation();
    type.name = "[]";

    consume(TokenType::BracketOpen);
    consume(TokenType::BracketClose);

    Optional<parsed::Type> subtype = parseType();
    if (subtype) {
      type.subtypes.push_back(subtype.getValue());
    } else {
      makeError("a type the array");
    }
    return type;
  } else if (testTokenType(TokenType::Star)) {
    // POINTER
    type.location = getTokenLocation();
    type.name = "*";
    consume(TokenType::Star);
    Optional<parsed::Type> subtype = parseType();
    if (subtype) {
      type.subtypes.push_back(subtype.getValue());
    } else {
      makeError("a type the array");
    }
    return type;
  } else if (testTokenType(TokenType::Ampersand)) {
    // REFERENCE
    type.location = getTokenLocation();
    type.name = "&";
    consume(TokenType::Ampersand);

    Optional<parsed::Type> subtype = parseType();
    if (subtype) {
      type.subtypes.push_back(subtype.getValue());
    } else {
      makeError("type of reference");
    }
    return type;
  } else if (testTokenType(TokenType::Identifier)) {
    type.location = getTokenLocation();
    type.name = getTokenString();
    consume(TokenType::Identifier);

    // TEMPLATE
    // We have to save the state and restore it in case its not actually a
    // function. It could be a comparison operator, eg:
    // if (counter < foobar()) {}
    ParserState saved_state = SaveState();
    bool template_fail = false;
    std::vector<parsed::Type> template_types;
    if (tryConsume(TokenType::Less)) {
      do {
        Optional<parsed::Type> subtype = parseType();
        if (subtype) {
          template_types.push_back(subtype.getValue());
        } else if (with_errors) {
          makeError("a type");
        } else {
          template_fail = true;
        }
      } while (tryConsume(TokenType::Coma));
      if (with_errors) {
        consume(TokenType::More);
      } else if (!tryConsume(TokenType::More)) {
        template_fail = true;
      }
    }
    if (template_fail) {
      RestoreState(saved_state);
    } else {
      type.subtypes = std::move(template_types);
    }
    return type;
  } else if (auto literal = parseIntegerLiteral()) {
    type.location = literal.getValue().location;
    type.literal = literal;
    return type;
  }
  return {};
}

bool Parser::parseTypeAlias(parsed::Scope& parsedScope) {
  if (!tryConsume(TokenType::KeywordTypeAlias)) {
    return false;
  }

  parsed::TypeAlias parsedAlias;
  parsedAlias.location = getTokenLocation();
  parsedAlias.name = getTokenString();

  astTypeAlias* alias = new astTypeAlias();
  alias->location = getTokenLocation();
  alias->name = getTokenString();
  consume(TokenType::Identifier);

  Optional<parsed::Type> maybeType = parseType();
  if (maybeType) {
    parsedAlias.type = maybeType.getValue();
  } else {
    makeError("expected a type");
    return false;
  }

  consume(TokenType::Semicolon);

  parsedScope.typeAliases.push_back(parsedAlias);

  return true;
}

std::string Parser::getTokenString() {
  std::string str = LexerGetString(mLexer, mToken);
  for (auto iter = str.begin(); iter != str.end(); /*EMPTY*/) {
    char character = *iter;
    if (character == '\\') {
      iter = str.erase(iter);
      if (*iter == 'n') {
        iter = str.erase(iter);
        str.insert(iter, '\10');
      }
    } else {
      ++iter;
    }
  }
  return str;
}
