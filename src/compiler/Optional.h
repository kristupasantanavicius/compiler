#pragma once

#include <compiler/Error.h>
#include <vector>

template <typename T>
class Optional {
 public:
#if 1
  Optional() {}

  Optional(const T& value) {
    // Read the comment below, where mValue is declared.
    // Reserve, because the first elemnent pushed without reserve expands vector
    // to capacity of 2, but only 1 will be used here.
    mValue.reserve(1);
    mValue.push_back(value);
  }

  T& getValue() {
    ASSERT(!mValue.empty());
    return mValue.at(0);
  }

  const T& getValue() const {
    ASSERT(!mValue.empty());
    return mValue.at(0);
  }

  T* getPointer() {
    if (isValid()) {
      return &getValue();
    } else {
      return nullptr;
    }
  }

  explicit operator bool() const { return isValid(); }

  bool isValid() const { return !mValue.empty(); }

 private:
  // The reason vector is used here, is because it seems to be the only
  // container which supports by-value copying, while also giving an indirection
  // to support forward declared types. shared_ptr, unique_ptr don't support by
  // value copying. Here is an example of code that using vector allows us to
  // do.
  /*
   struct SomeStruct;
   Optional<SomeStruct> opt;
   struct SomeStruct {
   };
   */
  std::vector<T> mValue;

#else
  // Naive, simple approach. The problem is that it calls constructor/destructor
  // for optional an empty optional value. Also, it doesn't support forwarded
  // parameters.

  Optional() {}

  Optional(const T& value) : value(value), set(true) {}

  T& getValue() {
    ASSERT(set);
    return value;
  }

  const T& getValue() const {
    ASSERT(set);
    return value;
  }

  explicit operator bool() const { return set; }

 private:
  T value;
  bool set = false;
#endif
};
