#pragma once

#include <string>

namespace module {

std::string GetModuleName(std::string fileName);

}
