#pragma once

#include <compiler/AST.h>

struct ExpressionVariable {
  ExpressionVariable(astVariable* variable) : variable(variable) {}
  ExpressionVariable(astExpression* expression) : expression(expression) {}

  astType* getType() {
    if (variable) {
      return variable->type;
    } else if (expression) {
      return expression->flatType;
    } else {
      ASSERT(false);
    }
  }

  astExpression* getUnresolvedExpression(astScope* scope) {
    // if (variable) { return astWrapVariableInExpression(variable, scope); }
    // else if (expression) { return COPY_EXPRESSION(expression); }
    if (variable) {
      return astWrapSymbolInExpression(
          astWrapVariableInSymbol(variable, scope, true));
    } else if (expression) {
      return COPY_EXPRESSION(expression);
    } else {
      ASSERT(false);
    }
  }

  astLocation getLocation() {
    if (variable) {
      return variable->location;
    } else if (expression) {
      return expression->location;
    } else {
      ASSERT(false);
    }
  }

 private:
  astVariable* variable = nullptr;
  astExpression* expression = nullptr;
};
