#include "compiler/Module.h"

namespace module {

std::string GetModuleName(std::string fileName) {
  std::size_t position = fileName.rfind("/");
  if (position != std::string::npos) {
    fileName = fileName.substr(position + 1);
  }

  position = fileName.find(".");
  if (position != std::string::npos) {
    fileName = fileName.substr(0, position);
  }

  return fileName;
}

}  // namespace module
