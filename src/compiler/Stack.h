#pragma once

#include <compiler/Error.h>
#include <vector>

template <typename T>
class Stack {
 public:
  void push(const T& t) { stack.push_back(t); }
  void pop() {
    ASSERT(!stack.empty());
    stack.pop_back();
  }

  T& getCurrent() {
    ASSERT(!stack.empty());
    return stack.back();
  }

  bool empty() { return stack.empty(); }

  const std::vector<T>& iterate() { return stack; }

  const std::vector<T>& asVector() { return stack; }

 private:
  std::vector<T> stack;
};
