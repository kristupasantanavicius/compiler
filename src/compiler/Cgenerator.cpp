#include <compiler/AST.h>
#include <compiler/Cbootstrap.h>
#include <compiler/Cgenerator.h>
#include <compiler/Error.h>
#include <compiler/Format.h>
#include <compiler/Stack.h>
#include <compiler/System.h>
#include <fstream>
#include <memory>
#include <sstream>
#include <stack>

typedef std::stringstream& OUT;

class GenerateVisitor;

struct State {
  std::string indent;

  std::unique_ptr<GenerateVisitor> visitor;
  std::stringstream out;

  void pushIndent() {
    indentLevel += 1;
    refreshIndent();
  }
  void popIndent() {
    indentLevel -= 1;
    ASSERT(indentLevel >= 0);
    refreshIndent();
  }

  Stack<astBlock*> blocks;

 private:
  void refreshIndent() {
    indent = "";
    for (int i = 0; i < indentLevel; i++) {
      // indent += (char)9;  // TAB
      indent += "  ";
    }
  }

 private:
  int indentLevel = 0;
};

const static std::string C_this = "this";
const static std::string C_arrow = "->";
const static std::string C_ampersand = "&";
const static std::string C_arrayReallocate = "arrayReallocate";

const static std::string tag_Overload = "ol";

std::string generateClassName(const astClass* aclass);
std::string generateFunctionName(const astFunction* function);
std::string generateTypeToStringWithoutStruct(astType* type);

std::string getScopeDeclaration(astScope* scope) {
  std::string decl;
  for (scope = scope->parent; scope; scope = scope->parent) {
    if (scope->owner.module != "") {
      decl = scope->owner.module + "_" + decl;
    } else if (scope->owner.aclass) {
      decl = generateClassName(scope->owner.aclass) + "_" + decl;
    } else if (scope->owner.function) {
      decl = generateFunctionName(scope->owner.function) + "_" + decl;
    }
  }
  return decl;
}

std::string getScopeDeclarationForClass(astClass* aclass) {
  ASSERT(aclass->scope);
  return getScopeDeclaration(aclass->scope) + generateClassName(aclass);
}

std::string generateFunctionName(const astFunction* function) {
  std::stringstream output;
  output << function->name;
  if (function->overloadTag != 0) {
    output << "_" << tag_Overload << function->overloadTag;
  }
  if (function->templatedFunction) {
    ASSERT(function->overloadTag == 0);  // This case was not thought about yet!
    for (auto& templateArgument : function->templateArguments) {
      output << '_' << generateTypeToStringWithoutStruct(templateArgument);
    }
  }
  return output.str();
}

std::string getScopeDeclarationForFunction(astFunction* function) {
  return getScopeDeclaration(function->scope) + generateFunctionName(function);
}

std::string generateTypeToStringWithoutStruct(astType* type) {
  if (type->name == astTypeName::Unresolved) {
    ASSERT(false);
  }
  std::stringstream stream;
  if (type->aclass) {
    stream << getScopeDeclarationForClass(type->aclass);
  } else {
    if (type->name == astTypeName::Int32) {
      stream << "int";
    } else if (type->name == astTypeName::Float32) {
      stream << "float";
    } else if (type->name == astTypeName::Bool) {
      stream << "BOOL";
    } else if (type->name == astTypeName::Char ||
               type->name == astTypeName::Int8) {
      stream << "char";
    } else if (type->name == astTypeName::Void) {
      stream << "void";
    } else if (type->name == astTypeName::Array) {
      ASSERT(!type->getSubtypes().empty());
      ASSERT(type->arrayClass);
      stream << type->arrayClass->name;
    } else if (type->name == astTypeName::Pointer) {
      ASSERT(type->getSubtypes().size() == 1);
      stream << generateTypeToStringWithoutStruct(type->getSubtypes().at(0));
      stream << "*";
    } else if (type->name == astTypeName::Reference) {
      ASSERT(type->getSubtypes().size() == 1);
      stream << "ref_";
      stream << generateTypeToStringWithoutStruct(type->getSubtypes().at(0));
    } else {
      stream << "[compiler_error;type=" << type->string << "]";
    }
  }
  return stream.str();
}

std::string generateTypeToString(astType* type) {
  std::stringstream output;
  bool reference = false;
  if (type->name == astTypeName::Reference) {
    type = type->getSubtypes().at(0);
    reference = true;
  }
  if (type->name == astTypeName::Class || type->name == astTypeName::Array) {
    output << "struct ";
  }
  output << generateTypeToStringWithoutStruct(type);
  if (reference) {
    output << "*";
  }
  return output.str();
}

std::string generateClassName(const astClass* aclass) {
  std::stringstream output;
  output << aclass->name;
  for (auto& arg : aclass->templateArguments) {
    output << "_" << generateTypeToStringWithoutStruct(arg);
  }
  return output.str();
}

std::string getTypeInfoString(astType* type) {
  std::stringstream stream;
  stream << "TypeInfo_";
  stream << generateTypeToStringWithoutStruct(type);
  return stream.str();
}

void generateVariableRef2(astVariableRef2* ref,
                          bool writeMemberCode,
                          State* state) {
  ASSERT(ref->variable);
  if (writeMemberCode) {
    if (ref->variable->memberOf) {
      if (!ref->variable
               ->thisContext()) {  // If variable is already 'this' context, we
                                   // don't write this context.
        state->out << C_this << C_arrow;
      }
    }
  }
  state->out << ref->variable->name;
}

void generateVariableWithoutAmpersand(State* state, astVariable* var) {
  if (var->memberOf) {
    state->out << C_this << C_arrow;
  }
  state->out << var->name;
}

void generateVariable(State* state, astVariable* var) {
  state->out << C_ampersand;
  generateVariableWithoutAmpersand(state, var);
}

void generateExpression(State* state, astExpression* expression);
void generateClassCopyConstructorFunctionCall(astClass* aclass, State* state);
void generateConstructor(astVariable* variable, State* state);
bool variableRequiresConstructor(astVariable* variable) {
  if (variable->type->name == astTypeName::Array) {
    ASSERT(variable->type->getSubtypes().size() == 1);
    astType* subtype = variable->type->getSubtypes().at(0);
    return subtype->name == astTypeName::Class;
  }
  return false;
}

void generateFunctionCall2(astFunctionCall2* call, State* state) {
  std::vector<bool> referenceArguments(call->arguments.size());
  std::vector<bool> dereferenceArguments(call->arguments.size());

  int argOffset = 0;

  // If we are passing 'this' context, we don't need to do reference to the
  // argument, since it will already be a pointer 'this' in C
  if (call->function->memberOf) {
    // When passing 'this' implicitly, its not part of argument list
    if (!call->passThisContext) {
      argOffset = 1;  // Skip the first argument, its not a user specified
                      // argument, its inserted by compiler.
      ASSERT(!call->arguments.empty());
      if (call->arguments.at(0)->flatType->name != astTypeName::Reference) {
        referenceArguments.at(0) = true;
      }
      // First argument in member function call is a reference to the object.
    }
  }

  for (int i = 0; i < (int)call->function->parameters.size(); i++) {
    if (call->function->parameters.at(i)->type->name ==
            astTypeName::Reference &&
        call->arguments.at(i + argOffset)->flatType->name !=
            astTypeName::Reference) {
      referenceArguments.at(i + argOffset) = true;
    }
    if (call->arguments.at(i + argOffset)->flatType->name ==
            astTypeName::Reference &&
        call->function->parameters.at(i)->type->name !=
            astTypeName::Reference) {
      dereferenceArguments.at(i + argOffset) = true;
    }
  }

  state->out
      << "VNS_CALL("
      << /*call->location.line << ", " <<*/ getScopeDeclarationForFunction(
             call->function)
      << "(";

  bool comma = false;

  if (call->passThisContext) {
    state->out << C_this;
    comma = true;
  }

  int argumentCounter = 0;
  for (auto& argument : call->arguments) {
    if (!comma) {
      comma = true;
    } else {
      state->out << ", ";
    }
    if (referenceArguments.at(argumentCounter)) {
      state->out << "&";
    }
    if (dereferenceArguments.at(argumentCounter)) {
      state->out << "*";  // Dereference the argument in C
    }
    generateExpression(state, argument);
    argumentCounter += 1;
  }

  state->out << ")";

  state->out << ", " << call->location.line << ")";
}

std::string operatorToString(astOperator op, astType* leftNodeType) {
  base::MaybeUnused(leftNodeType);

  switch (op) {
    case astOperator::Dot:
      return ".";
    case astOperator::Plus:
      return "+";
    case astOperator::Minus:
      return "-";
    case astOperator::Multiply:
      return "*";
    case astOperator::Divide:
      return "/";
    case astOperator::Modulo:
      return "%";
    case astOperator::CompareEquals:
      return "==";
    case astOperator::CompareNotEquals:
      return "!=";
    case astOperator::CompareLess:
      return "<";
    case astOperator::CompareMore:
      return ">";
    case astOperator::CompareEQLess:
      return "<=";
    case astOperator::CompareEQMore:
      return ">=";
    case astOperator::LogicalAND:
      return "&&";
    case astOperator::LogicalOR:
      return "||";
    case astOperator::None:
      ASSERT(false);
  }
  ASSERT(false);
  return "`no_op`";
}

std::string operatorPaddedToString(astOperator op, astType* leftNodeType) {
  std::stringstream out;
  bool padding = op != astOperator::Dot;
  if (padding)
    out << " ";
  out << operatorToString(op, leftNodeType);
  if (padding)
    out << " ";
  return out.str();
}

void generateSymbol(astSymbol* symbol, bool writeMemberCode, State* state) {
  if (symbol->variable2) {
    generateVariableRef2(symbol->variable2, writeMemberCode, state);
  } else if (symbol->functionCall2) {
    generateFunctionCall2(symbol->functionCall2, state);
  } else if (astLiteral* constant = symbol->constant) {
    if (constant->globalStringIndex != std::string::npos) {
      ASSERT(constant->type->name == astTypeName::Array);
      ASSERT(constant->type->getSubtypes().at(0)->name == astTypeName::Char);
      state->out << FORMAT("global_strings[@]", constant->globalStringIndex);
    } else if (constant->type->name == astTypeName::Int32) {
      state->out << symbol->constant->valueNumeric;
    } else if (constant->type->name == astTypeName::Float32) {
      state->out << symbol->constant->valueString;
    } else if (constant->type->name == astTypeName::Bool) {
      if (symbol->constant->valueNumeric == 1) {
        state->out << "true";
      } else if (symbol->constant->valueNumeric == 0) {
        state->out << "false";
      } else {
        ASSERT(false);
      }
    } else if (constant->type->name == astTypeName::Char) {
      state->out << "\'" << constant->valueString.at(0) << "\'";
    } else {
      ASSERT(false);
    }
  } else {
    ASSERT(false);
  }
}

void internalGenerateExpression(astExpression* expression,
                                astType* lastType,
                                State* state) {
  base::MaybeUnused(lastType);

  ASSERT(expression);

  if (expression->reference_of) {
    state->out << C_ampersand;
  }

  bool firstNode = true;
  int index = 0;
  for (astExpressionNode* node : expression->nodes) {
    if (ExpressionModifier* modifier = node->getModifier()) {
      if (modifier->type == ExpressionModifierType::Negative) {
        state->out << "-";
      } else {
        ASSERT(false);
      }
    }

    if (node->dereference) {
      state->out << "(*";
    }

    if (astSymbol* symbol = node->getSymbol()) {
      generateSymbol(symbol, firstNode, state);

    } else if (astOperatorInstance* op = node->getOperator()) {
      astType* leftNodeType = expression->nodes.at(index - 1)->getNodeType();
      state->out << operatorPaddedToString(op->op, leftNodeType);

    } else if (astExpression* subExpression = node->getSubExpression()) {
      generateExpression(state, subExpression);

    } else {
      ASSERT(false);
    }

    if (node->dereference) {
      state->out << ")";
    }

    firstNode = false;

    index += 1;
  }

#if 0
  if (expression->symbol) {
    generateSymbol(expression->symbol, lastType, state);
#if 0
    astSymbol* symbol = expression->symbol;
    switch (symbol->type) {
      case astSymbolType::Variable: {
        if (symbol->variable2) {
          bool writeMemberCode =
              (lastType == nullptr);  // Only generate 'this->' if this is the
                                      // beginning of the expression
          generateVariableRef2(symbol->variable2, writeMemberCode, state);
        } else {
          ASSERT(false);
        }
      } break;

      case astSymbolType::FunctionCall: {
        if (symbol->functionCall2) {
          generateFunctionCall2(symbol->functionCall2, state);
        } else {
          ASSERT(false);
        }
      } break;

      case astSymbolType::Literal: {
        astLiteral* constant = symbol->constant;
        ASSERT(constant);
        if (constant->globalStringIndex != std::string::npos) {
          ASSERT(constant->type->name == astTypeName::Array);
          ASSERT(constant->type->getSubtypes().at(0)->name ==
                 astTypeName::Char);
          state->out << FORMAT("global_strings[@]",
                               constant->globalStringIndex);
        } else if (constant->type->name == astTypeName::Int32) {
          state->out << symbol->constant->valueNumeric;
        } else if (constant->type->name == astTypeName::Bool) {
          if (symbol->constant->valueNumeric == 1) {
            state->out << "true";
          } else if (symbol->constant->valueNumeric == 0) {
            state->out << "false";
          } else {
            ASSERT(false);
          }
        } else {
          ASSERT(false);
        }
      } break;
    }
#endif
  } else {
    ASSERT(expression->left && expression->right &&
           expression->operatorToken != astOperator::None);
    internalGenerateExpression(expression->left, lastType, state);
    state->out << operatorPaddedToString(expression->operatorToken);
    internalGenerateExpression(expression->right, expression->right->type,
                               state);
  }
#endif
}

void generateExpression(State* state, astExpression* expression) {
  ASSERT(expression->modified);
  internalGenerateExpression(expression, nullptr, state);
}

void generateVariableName(astVariable* variable, State* state) {
  state->out << variable->name;
}

void generateVariableDecl(astVariable* variable, State* state) {
  std::string typeString = generateTypeToString(variable->type);
  state->out << typeString << " ";
  generateVariableName(variable, state);
}

void generateClassConstructorFunctionCall(astClass* aclass, State* state);

void generateVariableInitialization(astVariable* variable, State* state) {
  state->out << FORMAT("memset(@@, @, sizeof(@))", C_ampersand, variable->name,
                       0, generateTypeToString(variable->type));
}

void generateVariableDeclWithInitialization(State* state,
                                            astVariable* variable) {
  generateVariableDecl(variable, state);
  state->out << "; ";
  generateVariableInitialization(variable, state);
}

void generateVariableAssignment(astType* variableType,
                                astExpression* expression,
                                State* state) {
  state->out << " = ";
  ASSERT(expression);
  if (variableType->name != astTypeName::Reference &&
      expression->flatType->name == astTypeName::Reference) {
    state->out << "*";
  }
  generateExpression(state, expression);
}

void generateBlock(State*, astScope*, astBlock*);

void generateIf(State* state, astIf* anIf) {
  state->out << "if (";
  generateExpression(state, anIf->expression);
  state->out << ") {" << std::endl;
  state->pushIndent();
  generateBlock(state, anIf->scope, anIf->block);
  state->popIndent();
  state->out << state->indent << "}";
  if (anIf->hasElseBlock) {
    ASSERT(anIf->elseScope && anIf->elseBlock);
    state->out << std::endl << state->indent << "else ";
    if (anIf->elseBlockIsJustIf) {
      ASSERT(anIf->elseBlock->statements.size() == 1);
      ASSERT(anIf->elseBlock->statements.at(0)->type == astStatementType::If);
      astIf* elseIf = static_cast<astIf*>(anIf->elseBlock->statements.at(0));
      generateIf(state, elseIf);
    } else {
      state->out << "{" << std::endl;
      state->pushIndent();
      generateBlock(state, anIf->elseScope, anIf->elseBlock);
      state->popIndent();
      state->out << state->indent << "}";
    }
  }
}

void generateWhile(State* state, astWhile* awhile) {
  // Expression should have been moved inside the loop body into an 'if break'
  ASSERT(!awhile->expression);

  state->out << "while (true) {" << std::endl;
  state->pushIndent();
  generateBlock(state, awhile->scope, awhile->block);
  state->popIndent();
  state->out << state->indent << "}";
}

class GenerateVisitor : public StatementVisitor {
 public:
  GenerateVisitor(State* state) : state(state) {}

  virtual ~GenerateVisitor() {}

  State* state;

  void visit(astVariableDecl* decl) override {
    if (decl->expression) {
      generateVariableDecl(decl->variable, state);
      generateVariableAssignment(decl->variable->type, decl->expression, state);
    } else {
      generateVariableDeclWithInitialization(state, decl->variable);
    }
  }
  void visit(astVariableAssign* assign) override {
    ASSERT(assign->target);
    if (assign->variable_decl) {
      generateVariableDecl(assign->variable_decl, state);
      state->out << ";" << std::endl << state->indent;
    }
    if (assign->target->flatType->name == astTypeName::Reference) {
      state->out << "*";
    }
    generateExpression(state, assign->target);
    generateVariableAssignment(assign->target->flatType, assign->expression,
                               state);
  }
  void visit(astIf* anIf) override { generateIf(state, anIf); }
  void visit(astWhile* awhile) override { generateWhile(state, awhile); }
  void visit(astReturn* ret) override {
    state->out << FORMAT("function_stack_pop();") << std::endl;
    state->out << state->indent << "return";
    if (ret->expression) {
      state->out << " ";
      generateExpression(state, ret->expression);
    } else {
      // Nothing for now??? Its a simple 'return;' statement
      // state->out << ";";
    }
  }
  void visit(astStatementExpression* statementExpression) override {
    generateExpression(state, statementExpression->expression);
  }
  void visit(astStatementBreak*) override { state->out << "break"; }
  void visit(astStatementContinue*) override { state->out << "continue"; }
  void visit(astStatementNative* native) override {
    if (native->name == astStatementNative::Name::ArrayNew) {
      astType* subtype = native->arrayNewType->getSubtypes().at(0);
      astClass* aclass = subtype->getClass();
      std::string typeString = generateTypeToString(native->arrayNewType);
      state->out << "this->typeInfo = " << C_ampersand
                 << getTypeInfoString(subtype);
      if (native->array_with_size) {
        state->out << ";" << std::endl
                   << state->indent << "arrayNew(this, size)";
        if (aclass) {
          ASSERT(aclass->default_constructor);
          state->out << ";" << std::endl
                     << state->indent
                     << "for (int iter = 0; iter < size; iter++) {"
                     << std::endl;
          state->pushIndent();
          state->out << state->indent
                     << getScopeDeclarationForFunction(
                            aclass->default_constructor);
          state->out << FORMAT("((@*)arrayGet(this, iter));",
                               generateTypeToString(subtype))
                     << std::endl;
          state->popIndent();
          state->out << state->indent << "}" << std::endl;
        }
      }
    } else if (native->name == astStatementNative::Name::ArrayDestroy) {
      astType* subtype = native->arrayDestructorType;
      astClass* aclass = subtype->getClass();
      if (aclass) {
        ASSERT(aclass->destructor);
        state->out << "for (int iter = 0; iter < this->size; iter++) {"
                   << std::endl;
        state->pushIndent();
        state->out << state->indent
                   << getScopeDeclarationForFunction(aclass->destructor);
        state->out << FORMAT("((@*)arrayGet(this, iter));",
                             generateTypeToString(subtype))
                   << std::endl;
        state->popIndent();
        state->out << state->indent << "}" << std::endl;
        state->out << state->indent;
      }
      state->out << "arrayFree(this)";
    } else if (native->name == astStatementNative::Name::ArrayCopyConstructor) {
      astType* subtype = native->arrayCopyConstructorType;
      astClass* aclass = subtype->getClass();
      state->out << "arrayReallocate(this)";
      if (aclass) {
        state->out << ";" << std::endl << state->indent;
        ASSERT(aclass->copyConstructor);
        state->out << "for (int iter = 0; iter < this->size; iter++) {"
                   << std::endl;
        state->pushIndent();
        state->out << state->indent
                   << getScopeDeclarationForFunction(aclass->copyConstructor);
        state->out << FORMAT("((@*)arrayGet(this, iter));",
                             generateTypeToString(subtype))
                   << std::endl;
        state->popIndent();
        state->out << state->indent << "}";
      }
    } else if (native->name == astStatementNative::Name::ArraySize) {
      state->out << "function_stack_pop();" << std::endl;
      state->out << state->indent << "return this->size";
    } else if (native->name == astStatementNative::Name::ArrayGet) {
      astType* subtype = native->arrayGetType;
      std::string typeString = generateTypeToString(subtype);
      state->out << FORMAT("@ element = *(@*)arrayGet(this, index);",
                           typeString, typeString)
                 << std::endl;
      astClass* aclass = subtype->getClass();
      if (aclass) {
        ASSERT(aclass->copyConstructor);
        state->out << state->indent
                   << getScopeDeclarationForFunction(aclass->copyConstructor)
                   << "(&element);" << std::endl;
      }
      state->out << state->indent << "function_stack_pop();" << std::endl;
      state->out << state->indent << "return element";
    } else if (native->name == astStatementNative::Name::ArraySet) {
      astType* subtype = native->arraySetType;
      std::string typeString = generateTypeToString(subtype);
      state->out << FORMAT("@ * element = (@*)arrayGet(this, index);",
                           typeString, typeString)
                 << std::endl;
      state->out << state->indent;
      astClass* aclass = subtype->getClass();
      if (aclass) {
        ASSERT(aclass->destructor);
        state->out << getScopeDeclarationForFunction(aclass->destructor)
                   << "(element);" << std::endl;
        state->out << state->indent;
      }
      state->out << "*element = value";
      if (aclass) {
        state->out << ";" << std::endl << state->indent;
        ASSERT(aclass->copyConstructor);
        state->out << getScopeDeclarationForFunction(aclass->copyConstructor)
                   << "(element)";
      }
      state->out << ";" << std::endl;
      state->out << state->indent << "function_stack_pop()";
    } else if (native->name == astStatementNative::Name::ClassConstructor) {
      std::string typeName = generateTypeToString(native->classConstructorType);
      state->out << FORMAT("@ dst; memset(&dst, 0, sizeof(@));", typeName,
                           typeName)
                 << std::endl;
      // generateFunctionCall2(native->classConstructorCall, state);
      state->out << state->indent
                 << getScopeDeclarationForFunction(
                        native->classConstructorCall->function);
      state->out << "(&dst";
      for (auto& param : native->classConstructorCall->function->parameters) {
        state->out << ", " << param->name;
      }
      state->out << ");" << std::endl;
      state->out << state->indent << "function_stack_pop();" << std::endl;
      state->out << state->indent << "return dst";
    } else {
      ASSERT(false);
    }
  }
};

void generateFunctionSignature(State* state, astFunction* function) {
  if (function->isExternFunction || function->isAPIFunction) {
    // EXTERN_C is our definition: it works on both C and C++ code
    state->out << "EXTERN_C ";
  }

  if (function->scope->parent && function->scope->parent->owner.module != "" &&
      function->name == "main") {
    state->out << state->indent << "void main_impl(";
  } else {
    state->out << state->indent << generateTypeToString(function->returnType)
               << " " << getScopeDeclarationForFunction(function) << "(";
  }

  bool coma = false;
  if (function->memberOf) {
    state->out << FORMAT("struct @ * @",
                         getScopeDeclarationForClass(function->memberOf),
                         C_this);
    coma = true;
  }

  for (auto& parameter : function->parameters) {
    if (coma) {
      state->out << ", ";
    } else {
      coma = true;
    }
    state->out << generateTypeToString(parameter->type);
    state->out << " " << parameter->name;
  }

  state->out << ")";
}

void generateClassDefinition(State* state, astClass* aclass) {
  OUT out = state->out;
  out << state->indent << "struct " << getScopeDeclarationForClass(aclass)
      << std::endl;
  out << state->indent << "{" << std::endl;
  state->pushIndent();
  if (aclass->scope->variables.empty()) {
    // In C, all structs must have at least 1 member variable
    out << state->indent << "char compiler_placeholder_dont_use_this_variable;"
        << std::endl;
  } else {
    for (auto& variable : aclass->scope->variables) {
      out << state->indent;
      generateVariableDecl(variable, state);
      out << ";" << std::endl;
    }
  }
  state->popIndent();
  out << state->indent << "};" << std::endl;
}

void generateBlock(State* state, astScope* scope, astBlock* block) {
  base::MaybeUnused(scope);

  state->blocks.push(block);
  for (auto& statement : block->statements) {
    state->out << state->indent;
    statement->acceptVisitor(*state->visitor);
    state->out << ";" << std::endl;
  }
  state->blocks.pop();
}

void generateFunction(astFunction* function, State* state) {
  if (function->isExternFunction) {
    // Don't generate function body for extern functions..
    return;
  }
  generateFunctionSignature(state, function);
  state->out << std::endl;
  state->out << state->indent << "{" << std::endl;
  state->pushIndent();
  std::string fileName =
      function->location.fileName ? *function->location.fileName : "";
  state->out << state->indent
             << FORMAT("function_stack_push(\"@()\", \"@\");",
                       getScopeDeclarationForFunction(function), fileName)
             << std::endl;

  if (function->constructorFunction) {
    state->out << state->indent
               << FORMAT("@ thisOwner; memset(&thisOwner, 0, sizeof(@));",
                         generateTypeToString(function->returnType),
                         generateTypeToString(function->returnType))
               << std::endl;
    state->out << state->indent
               << FORMAT("@* this = &thisOwner;",
                         generateTypeToString(function->returnType))
               << std::endl;

    generateBlock(state, function->scope, function->block);

    state->out << state->indent << "function_stack_pop();" << std::endl;
    state->out << state->indent << "return *this;" << std::endl;
  } else {
    generateBlock(state, function->scope, function->block);
  }

  state->popIndent();
  state->out << state->indent << "}" << std::endl;
}

void generateFunctionForwardDeclarations(astScope* scope, State* state) {
  for (auto& function : scope->functions) {
    generateFunctionSignature(state, function);
    state->out << ";" << std::endl;
  }
  for (auto& function : scope->instantiatedTemplatedFunctions) {
    generateFunctionSignature(state, function);
    state->out << ";" << std::endl;
  }
  for (auto& aclass : scope->classes) {
    generateFunctionForwardDeclarations(aclass->scope, state);
    state->out << std::endl;
  }
  for (auto& aclass : scope->instantiatedTemplatedClasses) {
    generateFunctionForwardDeclarations(aclass->scope, state);
    state->out << std::endl;
  }
}

void generateScope(State* state, astScope* scope) {
  OUT out = state->out;
  for (auto& aclass : scope->classes) {
    generateScope(state, aclass->scope);
    out << std::endl;
  }
  for (auto& aclass : scope->instantiatedTemplatedClasses) {
    generateScope(state, aclass->scope);
    out << std::endl;
  }
  for (auto& function : scope->functions) {
    generateFunction(function, state);
  }
  for (auto& function : scope->instantiatedTemplatedFunctions) {
    generateFunction(function, state);
  }
}

void generateTypeInfos(ASTCache& cache, State* state) {
  // TypeInfoEntry
  struct TPE {
    std::string name;
    std::string typeName;
  };
  std::vector<TPE> entries;

  entries.push_back({"BOOL", "BOOL"});
  entries.push_back({"int", "int"});
  entries.push_back({"char", "char"});

  for (auto& aclass : cache.arrayClasses) {
    entries.push_back({getScopeDeclarationForClass(aclass),
                       "struct " + getScopeDeclarationForClass(aclass)});
  }

  for (auto& ast : cache.trees) {
    for (auto& aclass : ast->scope->classes) {
      entries.push_back({getScopeDeclarationForClass(aclass),
                         "struct " + getScopeDeclarationForClass(aclass)});
    }
    for (auto& aclass : ast->scope->instantiatedTemplatedClasses) {
      entries.push_back({getScopeDeclarationForClass(aclass),
                         "struct " + getScopeDeclarationForClass(aclass)});
    }
  }

  for (auto& entry : entries) {
    state->out << "struct TypeInfo TypeInfo_" << entry.name << ";" << std::endl;
  }
  state->out << std::endl;

  state->out << "void initialize_type_infos()" << std::endl;
  state->out << "{" << std::endl;
  state->pushIndent();
  for (auto& entry : entries) {
    // state->out << state->indent << FORMAT("memset(&TypeInfo_@, 0,
    // sizeof(struct TypeInfo));", entry.name) << std::endl;
    state->out << state->indent;
    state->out << FORMAT("TypeInfo_@.stringName = \"@\"; ", entry.name,
                         entry.name);
    state->out << FORMAT("TypeInfo_@.size = sizeof(@);", entry.name,
                         entry.typeName)
               << std::endl;
  }
  state->popIndent();
  state->out << "}" << std::endl;
  state->out << std::endl;
}

void generateStrings(ASTCache& cache, State* state) {
  // Global strings can be empty, in which case code generates has empty static
  // array, and it not valid code in VC
  if (!cache.globalStrings.empty()) {
    state->out << FORMAT("struct Array global_strings[@];",
                         cache.globalStrings.size())
               << std::endl;
    state->out << std::endl;
  }

  state->out << "void initialize_strings()" << std::endl;
  state->out << "{" << std::endl;

  state->pushIndent();
  int index = 0;
  for (auto& string : cache.globalStrings) {
    std::string pointer = string.empty() ? "NULL" : string;
    if (string.empty()) {
      // VNS arrays require the pointer to be NULL if an array is empty.
      // Otherwise there are assertions triggered at runtime.
      state->out << state->indent
                 << FORMAT("initialize_string_array(@, NULL, 0);", index)
                 << std::endl;
    } else {
      state->out << state->indent
                 << FORMAT("initialize_string_array(@, \"@\", @);", index,
                           pointer, string.size())
                 << std::endl;
    }
    index += 1;
  }
  state->popIndent();

  state->out << "}" << std::endl;
  state->out << std::endl;
}

void generateGlobalsDefinitions(ASTCache& cache, State* state) {
  for (SyntaxTree* ast : cache.trees) {
    for (astVariable* variable : ast->scope->variables) {
      state->out << generateTypeToString(variable->type) << " "
                 << variable->name << ";" << std::endl;
    }
  }
  state->out << std::endl;
  state->out << "void initialize_globals()" << std::endl;
  state->out << "{" << std::endl;
  state->pushIndent();
  for (SyntaxTree* ast : cache.trees) {
    for (astVariable* variable : ast->scope->variables) {
      state->out << state->indent;
      generateVariableInitialization(variable, state);
      state->out << ";" << std::endl;
    }
  }
  for (SyntaxTree* ast : cache.trees) {
    for (astVariable* variable : ast->scope->variables) {
      state->out << state->indent;
      generateVariableName(variable, state);
      generateVariableAssignment(variable->type,
                                 variable->initializeWithExpression, state);
      state->out << ";" << std::endl;
    }
  }
  state->popIndent();
  state->out << "}" << std::endl;
  state->out << std::endl;
}

void generateHeaderFile(ASTCache& cache, std::string* hFile) {
  State state;
  state.out << inlineC::bootstrap() << std::endl;

  // Array classes go first, since user defined classes depend on them.
  for (auto& aclass : cache.arrayClasses) {
    state.out << "#define " << getScopeDeclarationForClass(aclass) << " Array"
              << std::endl;
  }
  for (auto& ast : cache.trees) {
    for (auto& aclass : ast->allClasses) {
      generateClassDefinition(&state, aclass);
    }
    state.out << std::endl;
    for (auto& function : ast->scope->functions) {
      if (function->isAPIFunction) {
        generateFunctionSignature(&state, function);
        state.out << ";" << std::endl;
      }
    }
  }

  state.out << std::endl;
  *hFile = state.out.str();
}

void generateC(ASTCache& cache, std::string* hFile, std::string* cFile) {
  generateHeaderFile(cache, hFile);

  State state;

  state.out << "#include \"binary.h\"" << std::endl << std::endl;
  state.out << "enum { false, true };" << std::endl << std::endl;

  generateTypeInfos(cache, &state);
  generateStrings(cache, &state);
  generateGlobalsDefinitions(cache, &state);

  state.visitor.reset(new GenerateVisitor(&state));
  for (auto& ast : cache.trees) {
    generateFunctionForwardDeclarations(ast->scope, &state);
  }
  // Array functions depend on the user forward function declarations.
  for (auto& aclass : cache.arrayClasses) {
    generateScope(&state, aclass->scope);
  }
  for (auto& function : cache.newFunctions) {
    generateFunction(function, &state);
  }
  for (auto& ast : cache.trees) {
    generateScope(&state, ast->scope);
  }

  state.out << std::endl;

  state.out << inlineC::main() << std::endl;

  state.out << inlineC::append_at_end() << std::endl;

  *cFile = state.out.str();
}
