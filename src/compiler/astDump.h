#pragma once

#include <compiler/AST.h>
#include <compiler/Format.h>
#include <sstream>

inline std::string astDumpOperatorString(astOperator op) {
  switch (op) {
    case astOperator::None:
      return "{none}";
    case astOperator::Dot:
      return ".";
    case astOperator::Plus:
      return " + ";
    case astOperator::Minus:
      return " - ";
    case astOperator::Multiply:
      return " * ";
    case astOperator::Divide:
      return " / ";
    case astOperator::Modulo:
      return " % ";
    case astOperator::CompareEquals:
      return " == ";
    case astOperator::CompareNotEquals:
      return " != ";
    case astOperator::CompareLess:
      return " < ";
    case astOperator::CompareMore:
      return " > ";
    case astOperator::CompareEQLess:
      return " <= ";
    case astOperator::CompareEQMore:
      return " >= ";
    case astOperator::LogicalAND:
      return " && ";
    case astOperator::LogicalOR:
      return " || ";
  }
  // If this assert is left there, it causes annoying troubles when trying to do
  // tostring(expression) in VS debugger. The debugger says there was an
  // 'exception', so you have to go and add this expression to code, so you can
  // see what is wrong. ASSERT(false);
  return " {?} ";
}

inline void astDumpExpression_(astExpression* expression,
                               std::stringstream& stream);

inline void astDumpSymbol(astSymbol* symbol, std::stringstream& stream) {
  if (symbol->variable2) {
    stream << symbol->variable2->stringVariable;
  } else if (symbol->functionCall2) {
    if (symbol->functionCall2->function) {
      stream << symbol->functionCall2->function->name;
    } else {
      stream << astTypeToString(symbol->functionCall2->type);
    }
    stream << "(";
    bool comma = false;
    for (auto& argument : symbol->functionCall2->arguments) {
      if (!comma) {
        comma = true;
      } else {
        stream << ", ";
      }
      astDumpExpression_(argument, stream);
    }
    stream << ")";
  } else if (symbol->constant) {
    if (isNumericType(symbol->constant->type)) {
      stream << symbol->constant->valueNumeric;
    } else if (symbol->constant->type->name == astTypeName::Bool) {
      stream << symbol->constant->valueString;
    } else {
      stream << "\"" << symbol->constant->valueString << "\"";
    }
  } else {
    ASSERT(false);
  }
}

inline void astDumpExpressionModifier(ExpressionModifier& modifier,
                                      std::stringstream& stream) {
  if (modifier.type == ExpressionModifierType::Negative) {
    stream << "-";
  } else {
    ASSERT(false);
  }
}

inline void astDumpExpression_(astExpression* expression,
                               std::stringstream& stream) {
  stream << "(";
  for (astExpressionNode* node : expression->nodes) {
    if (ExpressionModifier* modifier = node->getModifier()) {
      astDumpExpressionModifier(*modifier, stream);
    }

    if (astSymbol* symbol = node->getSymbol()) {
      astDumpSymbol(symbol, stream);
    } else if (astOperatorInstance* op = node->getOperator()) {
      stream << astDumpOperatorString(op->op);
    } else if (astExpression* subExpression = node->getSubExpression()) {
      astDumpExpression_(subExpression, stream);
    } else {
      ASSERT(false);
    }
  }
  stream << ")";

#if 0
	if (expression->symbol) {
		astSymbol * symbol = expression->symbol;
		switch (expression->symbol->type)
		{
			case astSymbolType::Variable:
			{
				if (symbol->variable2) {
					stream << symbol->variable2->stringVariable;
				}
				else { ASSERT(false); }
			} break;

			case astSymbolType::FunctionCall:
			{
				if (symbol->functionCall2) {
					stream << symbol->functionCall2->stringName;
					stream << "(";
					bool comma = false;
					for (auto & argument : symbol->functionCall2->arguments) {
						if (!comma) {
							comma = true;
						}
						else {
							stream << ", ";
						}
						astDumpExpression_(argument, stream);
					}
					stream << ")";
				}
				else { ASSERT(false); }
			} break;

			case astSymbolType::Literal:
			{
				stream << "\"" << symbol->constant->valueString << "\"";
			} break;
		}
	}
	else {
		astDumpExpression_(expression->left, stream);
		stream << astDumpOperatorString(expression->operatorToken);
		astDumpExpression_(expression->right, stream);
	}
#endif
}

inline std::string tostring(astExpression* expression) {
  std::stringstream stream;
  astDumpExpression_(expression, stream);
  return stream.str();
}

class astDump {
 public:
  static void tostring(std::stringstream& stream, parsed::Type& type) {
    stream << type.name;
    if (!type.subtypes.empty()) {
      stream << "<";
      std::size_t index = 0;
      for (auto& subtype : type.subtypes) {
        astDump::tostring(stream, subtype);
        index += 1;
        if (index < type.subtypes.size()) {
          stream << ", ";
        }
      }
      stream << ">";
    }
  }

  static void tostring(std::stringstream& stream, parsed::Symbol& symbol) {
    if (auto ref = symbol.getVariableRef()) {
      stream << ref->name;
    } else if (auto call = symbol.getFunctionCall()) {
      stream << call->name;
      if (!call->templateArguments.empty()) {
        stream << "<";
        std::size_t index = 0;
        for (auto& argument : call->templateArguments) {
          astDump::tostring(stream, argument);
          index += 1;
          if (index < call->arguments.size()) {
            stream << ", ";
          }
        }
        stream << ">";
      }
      stream << "(";
      std::size_t index = 0;
      for (auto& argument : call->arguments) {
        astDump::tostring(stream, argument);
        index += 1;
        if (index < call->arguments.size()) {
          stream << ", ";
        }
      }
      stream << ")";
    } else if (auto constant = symbol.getConstant()) {
      stream << "\"" << constant->valueString << "\"";
    } else {
      ASSERT(false);
    }
#if 0
	struct Visitor : public parsed::SymbolVisitor {
		std::stringstream * stream;
		void visit(parsed::VariableRef2& ref) override {
			*stream << ref.name;
		}
		void visit(parsed::FunctionCall2& call) override {
			*stream << call.name;
			if (!call.templateArguments.empty()) {
				*stream << "<";
				std::size_t index = 0;
				for (auto & argument : call.templateArguments) {
					astDump::tostring(*stream, argument);
					index += 1;
					if (index < call.arguments.size()) {
						*stream << ", ";
					}
				}
				*stream << ">";
			}
			*stream << "(";
			std::size_t index = 0;
			for (auto & argument : call.arguments) {
				astDump::tostring(*stream, argument);
				index += 1;
				if (index < call.arguments.size()) {
					*stream << ", ";
				}
			}
			*stream << ")";
		}
		void visit(parsed::Literal& constant) override {
			*stream << "\"" << constant.valueString << "\"";
		}
	};
	Visitor visitor;
	visitor.stream = &stream;
	std::visit(visitor, symbol);
#endif
  }

  static void tostring(std::stringstream& stream,
                       parsed::Expression& expression) {
    stream << "(";
    for (int i = 0; i < (int)expression.nodes.size(); i++) {
      parsed::ExpressionNode& node = expression.nodes.at(i);

      if (ExpressionModifier* modifier = node.getModifier()) {
        astDumpExpressionModifier(*modifier, stream);
      }

      if (node.isOperator()) {
        stream << astDumpOperatorString(node.getOperator().op);
      } else if (node.isSymbol()) {
        astDump::tostring(stream, node.getSymbol());
      } else if (node.isSubExpression()) {
        astDump::tostring(stream, node.getSubExpression());
      } else {
        ASSERT(false);
      }
    }
    stream << ")";
#if 0
	struct Visitor : parsed::ExpressionVisitor {
		std::stringstream * stream;
		void visit(parsed::ExpressionLeaf & leaf) override {
			astDump::tostring(*stream, leaf.symbol());
		}
		void visit(parsed::ExpressionNode & node) override {
			astDump::tostring(*stream, node.left());
			*stream << astDumpOperatorString(node.operatorToken);
			astDump::tostring(*stream, node.right());
		}
	};
	Visitor visitor;
	visitor.stream = &stream;
	std::visit(visitor, expression);
#endif
  }
};

inline std::string tostring(parsed::Expression& expression) {
  std::stringstream stream;
  astDump::tostring(stream, expression);
  return stream.str();
}

inline std::string tostring(astType* type) {
  return astTypeToString(type);
}

inline std::string tostring(astSymbol* symbol) {
  std::stringstream stream;
  astDumpSymbol(symbol, stream);
  return stream.str();
}
