#include <compiler/Lexer.h>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

bool isLetter(char ch) {
  return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') ||
         (ch >= '0' && ch <= '9') || ch == '_';
}

static const char LINE_ENDING_N = '\n';
static const char LINE_ENDING_LF = (char)10;
static const char LINE_ENDING_CR = (char)13;

bool isLineEnding(char ch) {
  // TODO: This is probably incorrect design; Line ending might be CRLF (2
  // chars)??? Maybe the file shoud not compile with such line endings?
  return ch == LINE_ENDING_N || ch == LINE_ENDING_LF || ch == LINE_ENDING_CR;
}

bool isWhiteSpace(char ch) {
  return ch == ' ' || ch == (char)9  // TAB
         || isLineEnding(ch);
}

bool isDigit(char ch) {
  return ch >= '0' && ch <= '9';
}

Token LexerNextTokenImpl(Lexer* lexer) {
  int startPosition = lexer->scanner->currentPosition();
  char character = lexer->scanner->nextChar();

  if (character == 0) {
    return Token(startPosition, TokenType::EndOfFile);
  }

  if (isWhiteSpace(character)) {
    while (isWhiteSpace(lexer->scanner->peek())) {
      lexer->scanner->nextChar();
    }
    int endPosition = lexer->scanner->currentPosition();
    return Token(startPosition, endPosition, TokenType::WhiteSpace);
  }

  // COMMENT-LINE
  if (character == '/' && lexer->scanner->peek() == '/') {
    while (!isLineEnding(lexer->scanner->peek())) {
      lexer->scanner->nextChar();
    }
    int endPosition = lexer->scanner->currentPosition();
    return Token(startPosition, endPosition, TokenType::Comment);
  }

  {
    std::unordered_map<char, TokenType> charMap = {
        {'{', TokenType::BodyOpen},    {'}', TokenType::BodyClose},
        {'(', TokenType::ParamsOpen},  {')', TokenType::ParamsClose},
        {'[', TokenType::BracketOpen}, {']', TokenType::BracketClose},
        {'#', TokenType::Hash},        {':', TokenType::Colon},
        {';', TokenType::Semicolon},   {'.', TokenType::Dot},
        {',', TokenType::Coma},        {'=', TokenType::Equals},
        {'+', TokenType::Plus},        {'-', TokenType::Minus},
        {'<', TokenType::Less},        {'>', TokenType::More},
        {'$', TokenType::Dollar},      {'*', TokenType::Star},
        {'/', TokenType::Slash},       {'%', TokenType::Modulo},
        {'&', TokenType::Ampersand},   {'|', TokenType::Pipe},
        {'!', TokenType::Exclamation},
    };
    auto iter = charMap.find(character);
    if (iter != charMap.end()) {
      return Token(startPosition, iter->second);
    }
  }

  if (isDigit(character)) {
    bool floating = false;
    int end_offset = 0;

    while (isDigit(lexer->scanner->peek())) {
      lexer->scanner->nextChar();
    }

    if (lexer->scanner->peek() == 'f') {
      floating = true;
      lexer->scanner->nextChar();
      end_offset = 1;
    } else if (lexer->scanner->peek() == '.' &&
               isDigit(lexer->scanner->peek(1))) {
      floating = true;
      lexer->scanner->nextChar();
      while (isDigit(lexer->scanner->peek())) {
        lexer->scanner->nextChar();
      }
    }

    int endPosition = lexer->scanner->currentPosition() - end_offset;
    return Token(startPosition, endPosition, TokenType::Number, floating);
  }

  if (character == '"') {
    while (lexer->scanner->peek() != '"') {
      if (lexer->scanner->peek() == 0) {
        break;  // break the loop at EOF when there is no enclosing ["]
      }
      if (lexer->scanner->peek() == '\\') {
        lexer->scanner->nextChar();
      }
      lexer->scanner->nextChar();
    }
    lexer->scanner->nextChar();  // Consume the terminating ["]
    int endPosition = lexer->scanner->currentPosition();
    return Token(startPosition + 1, endPosition - 1, TokenType::String);
  }

  if (isLetter(character)) {
    while (isLetter(lexer->scanner->peek())) {
      lexer->scanner->nextChar();
    }
    int endPosition = lexer->scanner->currentPosition();

    // Identifier only becomes a keyword when all the characters match. For
    // example: [struct] - keyword [structA] - identifier
    {
      std::vector<std::pair<std::string, TokenType> > keywords = {
          std::make_pair("struct", TokenType::KeywordClass),
          std::make_pair("func", TokenType::KeywordFunction),
          std::make_pair("extern", TokenType::KeywordExtern),
          std::make_pair("api", TokenType::KeywordAPI),
          std::make_pair("return", TokenType::KeywordReturn),
          std::make_pair("if", TokenType::KeywordIf),
          std::make_pair("while", TokenType::KeywordWhile),
          std::make_pair("else", TokenType::KeywordElse),
          std::make_pair("break", TokenType::KeywordBreak),
          std::make_pair("continue", TokenType::KeywordContinue),
          std::make_pair("true", TokenType::KeywordTrue),
          std::make_pair("false", TokenType::KeywordFalse),
          std::make_pair("typealias", TokenType::KeywordTypeAlias),
          std::make_pair("mut", TokenType::KeywordMut),
          std::make_pair("pure", TokenType::KeywordPure),
          std::make_pair("as", TokenType::KeywordAs),
      };
      for (auto& pair : keywords) {
        if (lexer->scanner->compareString(pair.first, startPosition,
                                          endPosition)) {
          return Token(startPosition, endPosition, pair.second);
        }
      }
    }

    return Token(startPosition, endPosition, TokenType::Identifier);
  }

  return Token(startPosition, TokenType::Unknown);
}

Token LexerNextToken(Lexer* lexer) {
  Token token = LexerNextTokenImpl(lexer);
  while (token.type == TokenType::WhiteSpace ||
         token.type == TokenType::Comment) {
    token = LexerNextTokenImpl(lexer);
  }
  return token;
}

Token LexerPeekToken(Lexer* lexer, int howMany) {
  int position = lexer->scanner->currentPosition();
  Token token;
  for (int i = 0; i < howMany; i++) {
    token = LexerNextToken(lexer);
  }
  lexer->scanner->RestorePosition(position);
  return token;
}

std::string LexerGetString(Lexer* lexer, const Token& token) {
  return lexer->scanner->getString(token.start, token.end);
}

const int LEXER_FIRST_LINE = 1;

void LexerGetLineNumber(Lexer* lexer,
                        const Token& token,
                        int& outLine,
                        int& outChar) {
  const int charStart = 0;

  int line = LEXER_FIRST_LINE;
  int charInLine = charStart;

  for (int i = 0; i < token.start; i++) {
    charInLine += 1;
    char left = lexer->scanner->data.at(i);
    if (isLineEnding(left)) {
      line += 1;
      charInLine = charStart;
    }
  }

  outLine = line;
  outChar = charInLine;
}

std::vector<std::string> LexerGetTextBeforeLine(Lexer* lexer,
                                                int targetLine,
                                                int numberOfLines) {
  std::vector<std::string> lines;

  std::stringstream stream;

  const int minLine = targetLine - (numberOfLines - 1);
  const int maxLine = targetLine;
  auto lineWithinBounds = [minLine, maxLine](int line) {
    return line >= minLine && line <= maxLine;
  };

  int line = LEXER_FIRST_LINE;
  for (char left : lexer->scanner->data) {
    if (isLineEnding(left)) {
      if (lineWithinBounds(line)) {
        lines.push_back(stream.str());
      }
      stream = std::stringstream();
      line += 1;
    } else {
      if (lineWithinBounds(line)) {
        stream << left;
      }
    }
  }

  return lines;
}
