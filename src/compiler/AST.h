#pragma once

#include <compiler/Error.h>
#include <compiler/Log.h>
#include <compiler/Optional.h>
#include <compiler/Variant.h>
#include <compiler/base.h>
#include <array>
#include <memory>
#include <string>
#include <unordered_set>
#include <vector>

struct astScope;
struct astExpression;
struct astClass;
struct astVariable;
struct astFunction;
struct astBlock;
struct astScope;
struct astType;

struct astLocation {
  static astLocation Array() {
    static std::string name = "Compiler_Generated_Array_Location";
    return {9999990, 9999991, &name};
  }

  int line = 0;
  int charNumber = -1;
  std::string* fileName = nullptr;

  astLocation() {}

  astLocation(int line, int charNumber, std::string* fileName)
      : line(line), charNumber(charNumber), fileName(fileName) {}

  void sanity() const {
    ASSERT(line > 0);
    ASSERT(charNumber >= 0);  // Char can be 0. That is to reduce of by one
                              // errors that would appear if it was 1 char based
    ASSERT(fileName);
  }
};

struct astError {
  astLocation location;
  std::vector<astLocation> stackOfLocations;
  std::string message;
  astError() {}
  astError(astLocation location, std::string message)
      : location(location), message(message) {
    location.sanity();
    ASSERT(message.length() > 0);
  }
};

enum class astOperator {
  None,
  Dot,
  Plus,
  Minus,
  Multiply,
  Divide,
  Modulo,
  CompareEquals,
  CompareNotEquals,
  CompareLess,
  CompareMore,
  CompareEQLess,
  CompareEQMore,
  LogicalAND,
  LogicalOR,
};

struct astOperatorInstance {
  astLocation location;
  astOperator op = astOperator::None;
  astOperatorInstance(astOperator op, astLocation location)
      : location(location), op(op) {}
};

inline bool operatorIsMath(astOperator op) {
  switch (op) {
    case astOperator::Plus:
    case astOperator::Minus:
    case astOperator::Multiply:
    case astOperator::Divide:
    case astOperator::Modulo:
      return true;
    case astOperator::None:
    case astOperator::Dot:
    case astOperator::CompareEquals:
    case astOperator::CompareNotEquals:
    case astOperator::CompareLess:
    case astOperator::CompareMore:
    case astOperator::CompareEQLess:
    case astOperator::CompareEQMore:
    case astOperator::LogicalAND:
    case astOperator::LogicalOR:
      return false;
  }
  ASSERT(false);
  return false;
}

inline bool isComparisonOperator(astOperator op) {
  switch (op) {
    case astOperator::CompareEquals:
    case astOperator::CompareNotEquals:
    case astOperator::CompareLess:
    case astOperator::CompareMore:
    case astOperator::CompareEQLess:
    case astOperator::CompareEQMore:
      return true;
    case astOperator::Dot:
    case astOperator::Plus:
    case astOperator::Minus:
    case astOperator::Multiply:
    case astOperator::Divide:
    case astOperator::Modulo:
    case astOperator::LogicalAND:
    case astOperator::LogicalOR:
      return false;
    case astOperator::None:
      ASSERT(false);
  }
  ASSERT(false);
  return false;
}

inline bool isOperatorLogical(astOperator op) {
  switch (op) {
    case astOperator::None:
      ASSERT(false);
    case astOperator::LogicalAND:
    case astOperator::LogicalOR:
      return true;
    case astOperator::Dot:
    case astOperator::Plus:
    case astOperator::Minus:
    case astOperator::Multiply:
    case astOperator::Divide:
    case astOperator::Modulo:
    case astOperator::CompareEquals:
    case astOperator::CompareNotEquals:
    case astOperator::CompareLess:
    case astOperator::CompareMore:
    case astOperator::CompareEQLess:
    case astOperator::CompareEQMore:
      return false;
  }
  ASSERT(false);
  return false;
}

enum class ExpressionModifierType {
  Negative,  // -number
             // Not, // !boolean
};

struct ExpressionModifier {
  astLocation location;
  ExpressionModifierType type;
  ExpressionModifier(astLocation location, ExpressionModifierType type)
      : location(location), type(type) {}
};

namespace parsed {
struct Class;
struct Expression;
struct Function;
struct Literal;

struct Type {
  Type() {}
  Type(const astLocation& location, const std::string& name)
      : location(location), name(name) {}
  astLocation location;
  std::string name;
  Optional<Literal> literal;
  bool mutableReference = false;
  std::vector<Type> subtypes;
};

struct TypeAlias {
  astLocation location;
  std::string name;
  parsed::Type type;
};

struct Variable {
  astLocation location;
  std::string name;
  Optional<parsed::Type> type;
  Optional<parsed::Expression> initializeWithExpression;
};

struct VariableRef2 {
  astLocation location;
  std::string name;
};

struct VariableDecl;
struct VariableAssign;
struct StatementIf;
struct StatementWhile;
struct StatementReturn;
struct StatementBreak;
struct StatementContinue;
struct StatementExpression;

typedef std::variant<VariableDecl,
                     VariableAssign,
                     StatementIf,
                     StatementWhile,
                     StatementReturn,
                     StatementBreak,
                     StatementContinue,
                     StatementExpression>
    Statement;

struct Block {
  std::vector<Statement> statements;
};

enum class LiteralType {
  None,
  Integer,
  Float,
  Bool,
  String,
  Char,
};

// an integer, boolean, String (char array?), etc..
// '3', '12', '"string"', '"etc"', 'boolean'
struct Literal {
  Literal() {}
  Literal(LiteralType type) : type(type) {}
  LiteralType type = LiteralType::None;
  astLocation location;
  int valueNumeric = 0;
  std::string valueString;
};

struct FunctionCall2;

struct Symbol {
  Symbol(const parsed::VariableRef2& v) : variable_(v) {}
  Symbol(const parsed::FunctionCall2& f) : function_(f) {}
  Symbol(const parsed::Literal& c) : constant_(c) {}

  parsed::VariableRef2* getVariableRef() { return variable_.getPointer(); }
  parsed::FunctionCall2* getFunctionCall() { return function_.getPointer(); }
  parsed::Literal* getConstant() { return constant_.getPointer(); }

  astLocation getLocation();

 private:
  Optional<parsed::VariableRef2> variable_;
  Optional<parsed::FunctionCall2> function_;
  Optional<parsed::Literal> constant_;
};

#if 0
	struct ExpressionLeaf;
	struct ExpressionNode;

	typedef std::variant<parsed::ExpressionLeaf, parsed::ExpressionNode>
        Expression;

    struct ExpressionVisitor {
        VARIANT_INTERFACE_DECL(parsed::ExpressionLeaf&);
        VARIANT_INTERFACE_DECL(parsed::ExpressionNode&);
    };

	struct ExpressionLeaf
	{
		ExpressionLeaf() : mSymbol(1) { }
		// ExpressionLeaf(const parsed::Symbol & in) : mSymbol(1) { symbol() = in; }
		Symbol & symbol() { return mSymbol.at(0); }
	private:
		std::vector<Symbol> mSymbol;
	};

	struct ExpressionNode
	{
		ExpressionNode() : nodes(2) { }
		Expression & left() { return nodes.at(0); }
		Expression & right() { return nodes.at(1); }
		astLocation location;
		astOperator operatorToken = astOperator::None;
	private:
		std::vector<Expression> nodes;
		// Since ExpressionNode is part of Expression, it has to be dynamically alloced (like in Vector);
		// Otherwise, compilation fails with "usage of incomplete type ExpressionNode", since it hasn't been fully defined yet here.
	};
#endif

struct Expression;

struct ExpressionNode {
  ExpressionNode(astOperatorInstance op);
  ExpressionNode(const parsed::Symbol& symbol);
  ExpressionNode(const parsed::Expression& expression);

  astOperatorInstance& getOperator() {
    ASSERT(isOperator());
    return mOperator.getValue();
  }
  parsed::Symbol& getSymbol() {
    ASSERT(isSymbol());
    return mSymbol.getValue();
  }
  parsed::Expression& getSubExpression() {
    ASSERT(isSubExpression());
    return mExpression.getValue();
  }

  bool isOperator() { return mOperator.isValid(); }
  bool isSymbol() { return mSymbol.isValid(); }
  bool isSubExpression() { return mExpression.isValid(); }

  astLocation getLocation();

  void setModifier(ExpressionModifier mod) {
    ASSERT(!mModifier);
    mModifier = mod;
  }
  ExpressionModifier* getModifier() { return mModifier.getPointer(); }

  void setTypeCast(const Optional<parsed::Type>& cast) {
    ASSERT(!mTypeCast);
    mTypeCast = cast;
  }
  parsed::Type* getTypeCast() { return mTypeCast.getPointer(); }

 private:
  Optional<ExpressionModifier> mModifier;
  Optional<astOperatorInstance> mOperator;
  Optional<parsed::Symbol> mSymbol;
  Optional<parsed::Expression> mExpression;
  Optional<parsed::Type> mTypeCast;
};

struct Expression {
  astLocation location;
  std::vector<ExpressionNode> nodes;
};

struct Scope {
  std::vector<parsed::Class> classes;
  std::vector<parsed::Variable> variables;
  std::vector<parsed::Function> functions;
  std::vector<parsed::TypeAlias> typeAliases;
};

struct VariableDecl {
  astLocation location;
  astLocation assignLocation;
  parsed::Variable variable;
  bool hasInitExpression = false;
  parsed::Expression expression;
};

struct VariableAssign {
  astLocation location;
  // parsed::VariableRef variable;
  parsed::Expression target;
  parsed::Expression expression;
};

struct FunctionCall2 {
  astLocation location;
  std::string name;
  parsed::Type type;
  std::vector<Expression> arguments;
  std::vector<parsed::Type> templateArguments;
};

struct StatementIf {
  astLocation location;
  parsed::Expression expression;
  parsed::Scope scope;
  parsed::Block block;
  bool hasElseBlock = false;
  bool elseBlockIsJustIf = false;
  parsed::Scope elseScope;
  parsed::Block elseBlock;
};

struct StatementWhile {
  astLocation location;
  parsed::Expression expression;
  parsed::Scope scope;
  parsed::Block block;
};

struct StatementReturn {
  astLocation location;
  bool noExpression = false;
  parsed::Expression expression;
};

struct StatementExpression {
  parsed::Expression expression;
};

struct StatementBreak {
  astLocation location;
};

struct StatementContinue {
  astLocation location;
};

class StatementVisitor {
  VARIANT_INTERFACE_DECL(VariableDecl&)
  VARIANT_INTERFACE_DECL(VariableAssign&)
  VARIANT_INTERFACE_DECL(StatementIf&)
  VARIANT_INTERFACE_DECL(StatementWhile&)
  VARIANT_INTERFACE_DECL(StatementReturn&)
  VARIANT_INTERFACE_DECL(StatementExpression&)
  VARIANT_INTERFACE_DECL(StatementBreak&)
  VARIANT_INTERFACE_DECL(StatementContinue&)
};

struct Function {
  astLocation location;
  std::string name;
  std::vector<parsed::Variable> parameters;
  bool isExternFunction = false;
  bool isAPIFunction = false;
  bool isPure = false;
  parsed::Scope scope;
  parsed::Block block;
  parsed::Type returnType;
  bool templatedFunction = false;
  std::vector<std::string> templateParameters;
};

struct Class {
  astLocation location;
  std::string name;
  parsed::Scope scope;
  std::vector<std::size_t> constructors;  // Indexes into scope.functions array
  // Optional<parsed::Function> copyConstructor;
  // Optional<parsed::Function> destructor;
  bool templatedClass = false;
  std::vector<std::string> templateParameters;
};

}  // namespace parsed

class SyntaxTree;
class ASTCache {
 public:
  std::vector<SyntaxTree*> trees;
  std::vector<astClass*> arrayClasses;
  std::vector<astFunction*> newFunctions;
  std::vector<std::string> globalStrings;

  // Sometimes a type needs to be resolved, but it should not generate an error
  // (error is part of normal program operation, should be handled gracefully
  // by the compiler itself)
  std::unordered_set<astType*> types_without_error;
};

class SyntaxTree {
 public:
  SyntaxTree(const std::string& module_name);

  ASTCache* cache =
      nullptr;  // hopefully temporary fix! so I don't have to meddle with
                // changing from AST to ASTCache in multiple places for now
  astScope* scope = nullptr;
  std::vector<astType*> allTypes;
  std::vector<astClass*> allClasses;
};

enum class astTypeName {
  Unresolved,  // The type was created, but TypeChecker hasn't resolved it yet.
  Void,
  Bool,
  Int32,
  Int8,
  Float32,
  Char,
  Array,
  Class,
  Literal,
  TypeAlias,
  Pointer,
  Reference,
};

inline const char* typeNameToString(astTypeName name) {
  switch (name) {
    case astTypeName::Unresolved:
      return "Unresolved";
    case astTypeName::Void:
      return "Void";
    case astTypeName::Bool:
      return "Bool";
    case astTypeName::Int32:
      return "Int32";
    case astTypeName::Int8:
      return "Int8";
    case astTypeName::Float32:
      return "Int8";
    case astTypeName::Char:
      return "Char";
    case astTypeName::Array:
      return "Array";
    case astTypeName::Class:
      return "Class";
    case astTypeName::Literal:
      return "Literal";
    case astTypeName::TypeAlias:
      return "TypeAlias";
    case astTypeName::Pointer:
      return "Pointer";
    case astTypeName::Reference:
      return "Reference";
  }
  ASSERT(false);
  return "type_name_to_string_not_implemented";
}

struct astLiteral;

struct astType {
  astType(bool willBeResolved) : willBeResolved(willBeResolved) {}

  astLocation location;  // Its not the location where the 'actual' type is
                         // defined. Its location of Type in use-site

  std::string string;  // Token from source file

  astTypeName name = astTypeName::Unresolved;

  Optional<astLiteral*> literal;

  // If this type is a class, this pointer should be set
  astClass* aclass = nullptr;

  // When type is array, this will be compiler generated class during
  // TypeChecking
  astClass* arrayClass = nullptr;

  astScope* scope = nullptr;

  bool type_error_supressed = false;

  void createMutableReference() { mutableReference = true; }

  bool isMutableReference() {
    ASSERT(name == astTypeName::Reference);
    return mutableReference;
  }

  void addSubtype(astType* type, bool allowResolvedType = false);

  const std::vector<astType*> getSubtypes() const { return mSubtypes; }

  astClass* getClass();

  astType* COPY() const;

 private:
  std::vector<astType*> mSubtypes;

  // TODO: consider for removal. It's only purpose is to assert, but its not
  // clear what the assert means
  bool willBeResolved = false;

  bool mutableReference = false;
};

inline bool isNumericType(astType* type) {
  switch (type->name) {
    case astTypeName::Int32:
    case astTypeName::Int8:
    case astTypeName::Float32:
    case astTypeName::Char:
      return true;
    case astTypeName::Void:
    case astTypeName::Bool:
    case astTypeName::Array:
    case astTypeName::Class:
    case astTypeName::TypeAlias:
    case astTypeName::Literal:
    case astTypeName::Pointer:
    case astTypeName::Reference:
      return false;
    case astTypeName::Unresolved:
      ASSERT(false);
  }
  ASSERT(false);
  return false;
}

// an integer, boolean, String (char array?), etc..
// '3', '12', '"string"', '"etc"', 'boolean'
struct astLiteral {
  astLocation location;
  astType* type = nullptr;
  int valueNumeric = 0;
  std::string valueString;
  std::size_t globalStringIndex = std::string::npos;
};

struct astArgument {
  astExpression* expression = nullptr;
  std::string what;  // Foook meit
};

enum class astStatementType {
  VariableDecl,
  VariableAssignment,
  FunctionCall,
  If,
  While,
  Return,
  Expression,
  Break,
  Continue,
  Native,  // For generating native code. Its not added to visitor, since nobody
           // except for code generators wants to use it.
};

struct astVariableDecl;
struct astFunctionCall;
struct astVariableAssign;
struct astIf;
struct astWhile;
struct astReturn;
struct astStatementExpression;
struct astStatementNative;
struct astStatementBreak;
struct astStatementContinue;

class StatementVisitor {
 public:
  virtual void visit(astVariableDecl* decl) = 0;
  virtual void visit(astVariableAssign* assign) = 0;
  virtual void visit(astIf* aif) = 0;
  virtual void visit(astWhile* awhile) = 0;
  virtual void visit(astReturn* ret) = 0;
  virtual void visit(astStatementExpression* ret) = 0;
  virtual void visit(astStatementNative* native) = 0;
  virtual void visit(astStatementBreak* native) = 0;
  virtual void visit(astStatementContinue* native) = 0;
};

struct astStatement {
  const astStatementType type;
  astStatement(astStatementType type) : type(type) {}
  virtual void acceptVisitor(StatementVisitor& visitor) = 0;
};

struct astVariableDecl : public astStatement {
  astLocation location;
  astScope* scope = nullptr;
  astLocation assignLocation;
  astVariable* variable = nullptr;
  astExpression* expression = nullptr;
  astVariableDecl(astScope* scope)
      : astStatement(astStatementType::VariableDecl), scope(scope) {}
  void acceptVisitor(StatementVisitor& visitor) { visitor.visit(this); }
};

struct astFunctionCall2 {
  astLocation location;
  astScope* scope = nullptr;
  astType* type = nullptr;
  astFunction* function = nullptr;
  bool passThisContext =
      false;  // When true, this function call should implicitly pass 'this'
              // context to the function being called.
  std::vector<astExpression*> arguments;
  std::vector<astType*> templateArguments;
};

struct astVariableAssign : public astStatement {
  astLocation location;
  astScope* scope = nullptr;
  astVariable* variable_decl = nullptr;
  astExpression* target = nullptr;
  astExpression* expression = nullptr;
  astVariableAssign(astScope* scope)
      : astStatement(astStatementType::VariableAssignment), scope(scope) {}
  void acceptVisitor(StatementVisitor& visitor) { visitor.visit(this); }
};

struct astIf : public astStatement {
  astLocation location;
  astExpression* expression = nullptr;
  astScope* scope = nullptr;
  astBlock* block = nullptr;
  bool hasElseBlock = false;
  bool elseBlockIsJustIf = false;
  astScope* elseScope = nullptr;
  astBlock* elseBlock = nullptr;
  astIf() : astStatement(astStatementType::If) {}
  void acceptVisitor(StatementVisitor& visitor) { visitor.visit(this); }
};

struct astWhile : public astStatement {
  astLocation location;
  astExpression* expression = nullptr;
  astScope* scope = nullptr;
  astBlock* block = nullptr;
  astWhile() : astStatement(astStatementType::While) {}
  void acceptVisitor(StatementVisitor& visitor) { visitor.visit(this); }
};

struct astReturn : public astStatement {
  astLocation location;
  astScope* scope = nullptr;  // Used in Modify stage
  astExpression* expression = nullptr;

  astReturn(astLocation location, astScope* scope)
      : astStatement(astStatementType::Return),
        location(location),
        scope(scope) {}

  void acceptVisitor(StatementVisitor& visitor) { visitor.visit(this); }
};

struct astStatementExpression : public astStatement {
  astLocation location;
  astExpression* expression = nullptr;
  astStatementExpression() : astStatement(astStatementType::Expression) {}
  void acceptVisitor(StatementVisitor& visitor) { visitor.visit(this); }
};

struct astStatementBreak : public astStatement {
  astLocation location;
  astStatementBreak(astLocation location)
      : astStatement(astStatementType::Break), location(location) {}
  void acceptVisitor(StatementVisitor& visitor) { visitor.visit(this); }
};

struct astStatementContinue : public astStatement {
  astLocation location;
  astStatementContinue() : astStatement(astStatementType::Continue) {}
  void acceptVisitor(StatementVisitor& visitor) { visitor.visit(this); }
};

struct astStatementNative : public astStatement {
  enum class Name {
    None,
    ArrayNew,
    ArrayDestroy,
    ArrayTypeInfo,
    ArraySize,
    ArrayGet,
    ArraySet,
    ArrayCopyConstructor,
    ClassConstructor,
  };

  astLocation location;
  Name name = Name::None;

  astType* arrayNewType = nullptr;
  bool array_with_size = false;
  void initArrayNew(astType* arrayType, bool with_size) {
    name = Name::ArrayNew;
    arrayNewType = arrayType;
    array_with_size = with_size;
  }

  astType* arrayDestructorType = nullptr;
  void initArrayDestructor(astType* arrayType) {
    name = Name::ArrayDestroy;
    arrayDestructorType = arrayType;
  }

  astType* arrayGetType = nullptr;
  void initArrayGet(astType* arrayType) {
    name = Name::ArrayGet;
    arrayGetType = arrayType;
  }

  astType* arraySetType = nullptr;
  void initArraySet(astType* arrayType) {
    name = Name::ArraySet;
    arraySetType = arrayType;
  }

  astType* arrayCopyConstructorType = nullptr;
  void initArrayCopyConstructor(astType* arrayType) {
    name = Name::ArrayCopyConstructor;
    arrayCopyConstructorType = arrayType;
  }

  void initArraySize() { name = Name::ArraySize; }

  astType* classConstructorType = nullptr;
  astFunctionCall2* classConstructorCall = nullptr;
  void initClassConstructor(astType* classType, astFunctionCall2* call) {
    name = Name::ClassConstructor;
    classConstructorType = classType;
    classConstructorCall = call;
  }

  astStatementNative() : astStatement(astStatementType::Native) {}
  void acceptVisitor(StatementVisitor& visitor) { visitor.visit(this); }
};

struct astVariable {
  astLocation location;
  std::string name;
  bool global = false;
  astClass* memberOf = nullptr;
  astExpression* initializeWithExpression = nullptr;
  astType* type = nullptr;

  bool thisContext();
};

struct astVariableRef2 {
  astLocation location;
  astScope* scope = nullptr;
  std::string stringVariable;  // name of target variable
  astVariable* variable = nullptr;
};

enum class astScopeType {
  Global,
  Class,
  Function,
};

struct astClass {
  astClass(const std::string& name, astScope* parentScope);

  astLocation location;
  std::string name;
  astScope* scope = nullptr;
  astFunction* instance_constructor;
  astFunction* default_constructor = nullptr;
  astFunction* copyConstructor = nullptr;
  astFunction* destructor = nullptr;
  bool templatedClass = false;
  std::vector<astType*> templateArguments;

  std::vector<astLocation> templateInstantiationStack;
};

enum class astNativeFunctionName {
  None,
  ArrayGet,
  ArraySet,
  ArraySize,
  ArrayNew,
};

typedef int astOverloadTag;

struct astFunction {
  astFunction(const std::string& name, astScope* parentScope);

  astLocation location;
  const std::string name;
  astOverloadTag overloadTag = 0;  // Output code generators need to be able to
                                   // tell apart overloaded functions
  std::vector<astVariable*> parameters;
  bool isExternFunction = false;
  bool isAPIFunction = false;
  bool arrayFunction =
      false;  // For array generated functions, some compilation checks need to
              // be skipped, like return path detection
  bool constructorFunction = false;
  bool defaultVoidReturn = false;  // When function returns void and user didn't
                                   // specify 'return', we need to know that.
  astNativeFunctionName nativeName = astNativeFunctionName::None;
  astClass* memberOf = nullptr;  // If this function is a member function, this
                                 // will be set to the containing class
  astScope* scope = nullptr;
  astBlock* block = nullptr;
  astType* returnType = nullptr;
  bool templatedFunction = false;
  std::vector<astType*> templateArguments;
};

struct astTypeAlias {
  astLocation location;
  std::string name;
  astType* type = nullptr;
};

struct ScopeOwner {
  static ScopeOwner None() { return ScopeOwner("", nullptr, nullptr); }

  static ScopeOwner Module(const std::string& module) {
    return ScopeOwner(module, nullptr, nullptr);
  }

  static ScopeOwner Class(astClass* aclass) {
    return ScopeOwner("", aclass, nullptr);
  }

  static ScopeOwner Function(astFunction* function) {
    return ScopeOwner("", nullptr, function);
  }

  const std::string module;

  const astClass* aclass;

  const astFunction* function;

 private:
  ScopeOwner(const std::string& module, astClass* aclass, astFunction* function)
      : module(module), aclass(aclass), function(function) {}
};

struct astScope {
  astScope(ScopeOwner owner, astScope* parent) : owner(owner), parent(parent) {
    flag_member = parent ? parent->flag_member : false;
  }

  ScopeOwner owner;
  astScope* parent = nullptr;

  std::vector<astClass*> classes;
  std::vector<parsed::Class> templatedClasses;
  std::vector<astClass*> instantiatedTemplatedClasses;
  std::vector<astVariable*> variables;
  std::vector<astFunction*> functions;
  std::vector<parsed::Function> templatedFunctions;
  std::vector<astFunction*> instantiatedTemplatedFunctions;
  std::vector<astTypeAlias*> typeAliases;

  // Indicates if the scope can access member in a parent scope
  bool flag_member = false;

  std::vector<astScope*> otherFileScopes;
  astVariable* thisVariable = nullptr;
};

enum class astSymbolType {
  Variable,
  FunctionCall,
  Literal,
};

struct astSymbol {
  astSymbol(astVariableRef2* variable)
      : location(variable->location), variable2(variable) {
    location.sanity();
  }

  astSymbol(astFunctionCall2* function_call)
      : location(function_call->location), functionCall2(function_call) {
    location.sanity();
  }

  astSymbol(astLiteral* literal)
      : location(literal->location), constant(literal) {
    location.sanity();
  }

  // The content of this string depends on symbol type
  std::string string;
  const astLocation location;

  astScope* scope = nullptr;

  // Only one of these should be set
  astVariableRef2* variable2 = nullptr;
  astFunctionCall2* functionCall2 = nullptr;
  astLiteral* constant = nullptr;

  astType* getSymbolType() {
    if (variable2) {
      ASSERT(variable2->variable);
      ASSERT(variable2->variable->type);
      return variable2->variable->type;
    } else if (functionCall2) {
      ASSERT(functionCall2->function);
      ASSERT(functionCall2->function->returnType);
      return functionCall2->function->returnType;
    } else if (constant) {
      ASSERT(constant->type);
      return constant->type;
    } else {
      ASSERT(false);
      return nullptr;
    }
  }

  // TODO: Comment
  bool arrayLoopIterator = false;
};

enum class astBlockType {
  Function,   // A block which belongs to "function", meaning there is its the
              // root
  Statement,  // A block which belongs to a statement (IF, WHILE, etc..)
  Root = Function,
  Child = Statement,
};

struct astBlock {
  astBlock(astBlockType type) : type(type) {}
  astBlockType type;
  std::vector<astStatement*> statements;
};

struct astExpressionNode {
  astExpressionNode(astSymbol* symbol) : mSymbol(symbol) { ASSERT(symbol); }
  astExpressionNode(astOperatorInstance op) : mOperator(op) {}
  astExpressionNode(astExpression* expression) : mSubExpression(expression) {
    ASSERT(expression);
  }

  astSymbol* getSymbol() { return mSymbol; }
  astOperatorInstance* getOperator() { return mOperator.getPointer(); }
  astExpression* getSubExpression() { return mSubExpression; }

  astType* getNodeType();
  astLocation getLocation();

  void setModifier(const ExpressionModifier& mod) {
    ASSERT(!mModifier);
    mModifier = mod;
  }
  ExpressionModifier* getModifier() { return mModifier.getPointer(); }

  void setTypeCast(astType* type) {
    ASSERT(!mTypeCast);
    mTypeCast = type;
  }
  astType* getTypeCast() { return mTypeCast; }

  // Indicates that the node is a reference, and that the node should be
  // dereferenced to get its actual value.
  bool dereference = false;

 private:
  Optional<ExpressionModifier> mModifier;
  astSymbol* mSymbol = nullptr;
  Optional<astOperatorInstance> mOperator;
  astExpression* mSubExpression = nullptr;
  astType* mTypeCast = nullptr;
};

struct astExpression {
  bool modified = false;
  void setModifiedFlag() {
    ASSERT(!modified);
    modified = true;
  }

  const astLocation location;

  std::vector<astExpressionNode*> nodes;
  astType* flatType = nullptr;

  // Indicates that code should take a reference of the expression. Expression
  // could be pointing to value, but expression will take a reference to that
  // value and the type of expression will be a reference.
  bool reference_of = false;

  // astExpression() { }
  astExpression(astLocation location, astType* type)
      : location(location), flatType(type) {
    location.sanity();
  }

  astExpression* shallowCopy() {
    return new astExpression(location, flatType->COPY());
  }

  astExpressionNode* getEndingSymbol() {
    astExpressionNode* node = nodes.back();
    if (astExpression* expression = node->getSubExpression()) {
      return expression->getEndingSymbol();
    } else if (astSymbol* symbol = node->getSymbol()) {
      return node;
    } else {
      ASSERT(false);
      return nullptr;
    }
  }
};

inline astIf* astNewIf(astScope* parent) {
  astIf* anIf = new astIf();
  anIf->scope = new astScope(ScopeOwner::None(), parent);
  anIf->block = new astBlock(astBlockType::Child);
  anIf->elseScope = new astScope(ScopeOwner::None(), parent);
  anIf->elseBlock = new astBlock(astBlockType::Child);
  return anIf;
}
inline astType* astNewType(std::vector<astType*>* resolvedTypes,
                           const astLocation& location,
                           const std::string& string,
                           astScope* scope) {
  astType* type = new astType(resolvedTypes != nullptr);
  type->location = location;
  type->string = string;
  type->scope = scope;
  if (resolvedTypes) {
    resolvedTypes->push_back(type);
  }
  return type;
}
inline astWhile* astNewWhile(astScope* parent) {
  astWhile* awhile = new astWhile();
  awhile->scope = new astScope(ScopeOwner::None(), parent);
  awhile->block = new astBlock(astBlockType::Child);
  return awhile;
}

inline astType* astNewBasicType(astTypeName name, astLocation location) {
  astType* type =
      astNewType(nullptr, location, typeNameToString(name), nullptr);
  type->name = name;
  return type;
}

std::string astTypeToString(astType* type);
astClass* astFindClassInScope(astScope* scope,
                              const std::string& name,
                              bool recurse = true);
parsed::Class* astFindTemplatedClassInScope(astScope* scope,
                                            const std::string& name,
                                            astScope** outputScope,
                                            bool recurse = true);
astVariable* astFindVariableInScope(astScope* scope,
                                    const std::string& name,
                                    bool recurse,
                                    bool flag_member);
std::vector<astFunction*> astFindAllFunctionsInScope(astScope* scope,
                                                     const std::string& name,
                                                     bool recurse);
parsed::Function* astFindTemplatedFunctionInScope(astScope* scope,
                                                  const std::string& name,
                                                  bool recurse,
                                                  astScope** outputScope);
bool astTypesMatch(astType* left, astType* right);
bool astTypesMatchSkipReferences(astType* left, astType* rigth);
astTypeAlias* astFindTypeAlias(astScope* scope,
                               const std::string& name,
                               bool recurse = true);
astType* astGetArrayConcreteType(astType* type);

#if 0
// Some basic checks to see if expression AST is some what correct
void expressionSanityCheck(astExpression * expression);

void inferExpressionTypeBasedOnSymbol(astExpression * expression);
void inferExpressionNodeTypes(astExpression * expression, std::vector<astError> & errors);
#endif

// return: a type-checked expression
astExpression* COPY_EXPRESSION(astExpression* expression);

astVariable* COPY_VARIABLE(astVariable* variable);

// return: a type-checked expression
// astExpression* astWrapVariableInExpressionDontInferTypes(astVariable *
// variable, astScope * scope); astExpression*
// astWrapVariableInExpression(astVariable * variable, astScope * scope);
astSymbol* astWrapVariableInSymbol(astVariable* variable,
                                   astScope* scope,
                                   bool resolved);
astExpression* astWrapSymbolInExpression(astSymbol* symbol,
                                         bool nullType = false);

astType* astWrapClassInType(astClass* aclass, astLocation location);
astType* astWrapTypeInArray(astType* subtype,
                            astClass* arrayClass,
                            astLocation location);
astType* astWrapTypeInMutableReference(astType* type, astLocation location);

inline astReturn* astNewDefaultReturn(astLocation location, astScope* scope) {
  return new astReturn(location, scope);
}

inline astType* removeReference(astType* type) {
  if (type->name == astTypeName::Reference) {
    return type->getSubtypes().at(0);
  } else {
    return type;
  }
}
