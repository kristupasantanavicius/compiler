#pragma once

#include <compiler/Error.h>
#include <map>
#include <memory>
#include <string>
#include <vector>

enum class TokenType {
  Identifier,
  String,
  KeywordClass,
  KeywordFunction,
  KeywordExtern,
  KeywordAPI,
  KeywordReturn,
  KeywordIf,
  KeywordElse,
  KeywordWhile,
  KeywordBreak,
  KeywordContinue,
  KeywordTrue,
  KeywordFalse,
  KeywordTypeAlias,
  KeywordMut,
  KeywordPure,
  KeywordAs,
  BodyOpen,
  BodyClose,
  ParamsOpen,
  ParamsClose,
  BracketOpen,
  BracketClose,
  Hash,
  Colon,
  Semicolon,
  Dot,
  Coma,
  Equals,
  Plus,
  Minus,
  Less,
  More,
  Dollar,
  Star,
  Slash,
  Modulo,
  Ampersand,    // &
  Pipe,         // |
  Exclamation,  // !
  Number,
  Comment,
  WhiteSpace,
  EndOfFile,
  Unknown,
  COUNT,
};
inline std::string TokenTypeString(TokenType type) {
  switch (type) {
    case TokenType::Identifier:
      return "Identifier";
    case TokenType::String:
      return "String";
    case TokenType::KeywordClass:
      return "KeywordClass";
    case TokenType::KeywordFunction:
      return "KeywordFunction";
    case TokenType::KeywordExtern:
      return "KeywordExtern";
    case TokenType::KeywordAPI:
      return "KeywordAPI";
    case TokenType::KeywordReturn:
      return "KeywordReturn";
    case TokenType::KeywordIf:
      return "KeywordIf";
    case TokenType::KeywordElse:
      return "KeywordElse";
    case TokenType::KeywordWhile:
      return "KeywordWhile";
    case TokenType::KeywordBreak:
      return "KeywordBreak";
    case TokenType::KeywordContinue:
      return "KeywordContinue";
    case TokenType::KeywordTrue:
      return "KeywordTrue";
    case TokenType::KeywordFalse:
      return "KeywordFalse";
    case TokenType::KeywordTypeAlias:
      return "KeywordTypeAlias";
    case TokenType::KeywordMut:
      return "KeywordMut";
    case TokenType::KeywordPure:
      return "KeywordPure";
    case TokenType::KeywordAs:
      return "KeywordAs";
    case TokenType::BodyOpen:
      return "BodyOpen";
    case TokenType::BodyClose:
      return "BodyClose";
    case TokenType::ParamsOpen:
      return "ParamsOpen";
    case TokenType::ParamsClose:
      return "ParamsClose";
    case TokenType::BracketOpen:
      return "BracketOpen";
    case TokenType::BracketClose:
      return "BracketClose";
    case TokenType::Hash:
      return "Hash";
    case TokenType::Colon:
      return "Colon";
    case TokenType::Semicolon:
      return "Semicolon";
    case TokenType::Dot:
      return "Dot";
    case TokenType::Coma:
      return "Coma";
    case TokenType::Equals:
      return "Equals";
    case TokenType::Plus:
      return "Plus";
    case TokenType::Minus:
      return "Minus";
    case TokenType::Less:
      return "Less";
    case TokenType::More:
      return "More";
    case TokenType::Dollar:
      return "Dollar";
    case TokenType::Star:
      return "Star";
    case TokenType::Slash:
      return "Slash";
    case TokenType::Modulo:
      return "Modulo";
    case TokenType::Ampersand:
      return "Ampersand";
    case TokenType::Pipe:
      return "Pipe";
    case TokenType::Exclamation:
      return "Exclamation";
    case TokenType::Number:
      return "Number";
    case TokenType::Comment:
      return "Comment";
    case TokenType::WhiteSpace:
      return "WhiteSpace";
    case TokenType::EndOfFile:
      return "EndOfFile";
    case TokenType::Unknown:
      return "Unknown";
    case TokenType::COUNT:
      return "COUNT";
  }
  return "unknown_token_name";
}

struct Token {
  int start = 0, end = 0;
  TokenType type = TokenType::Unknown;
  bool floating_number = false;
  Token() {}
  Token(int start, int end, TokenType type, bool floating = false)
      : start(start), end(end), type(type), floating_number(floating) {}
  Token(int start, TokenType type) : start(start), end(start + 1), type(type) {}
};

struct Scanner {
  int position = 0;
  std::string data;

  Scanner(const std::string& data) : data(data) {}

  int currentPosition() { return position; }

  char peek(std::size_t steps = 0) {
    if (position + steps >= data.size()) {
      return (char)0;
    }
    return data.at(position + steps);
  }

  char nextChar() {
    if (position >= (int)data.size()) {
      return (char)0;
    }
    char ch = data.at(position);
    position += 1;
    return ch;
  }

  bool compareString(const std::string& str, int start, int end) {
    return str.compare(0, str.size(), data.c_str() + start, end - start) == 0;
  }

  std::string getString(int start, int end) {
    return data.substr(start, end - start);
  }

  void RestorePosition(int previousPosition) {
    this->position = previousPosition;
  }
};

class Lexer {
 public:
  std::string fileName = nullptr;
  std::unique_ptr<Scanner> scanner;
  Lexer(const std::string& fileName, const std::string& data)
      : fileName(fileName), scanner(new Scanner(data)) {}
};

Token LexerNextToken(Lexer* lexer);
Token LexerPeekToken(Lexer* lexer, int howMany);
std::string LexerGetString(Lexer* lexer, const Token& token);
void LexerGetLineNumber(Lexer* lexer,
                        const Token& token,
                        int& outLine,
                        int& outChar);
std::vector<std::string> LexerGetTextBeforeLine(Lexer* lexer,
                                                int targetLine,
                                                int numberOfLines);
