#include "compiler/Cgenerator.h"
#include "compiler/Error.h"
#include "compiler/Format.h"
#include "compiler/InsertionTest.h"
#include "compiler/Lexer.h"
#include "compiler/Modify.h"
#include "compiler/Module.h"
#include "compiler/Parser.h"
#include "compiler/System.h"
#include "compiler/TypeCheck.h"
#include "compiler/Variant.h"
#include "compiler/astDump.h"
#include "compiler/astGenerator.h"

#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

const std::string INPUT_FILE = "C:/workspace/compiler/test/Testing.vns";
const std::string OUTPUT_FILE_PATH = "C:/workspace/compiler_2/src/binary";

void fixForDebugger() {
  // in debugger this function does not appear for some reason without calling
  // it from code
  tostring((astExpression*)nullptr);
  tostring((astType*)nullptr);
  parsed::Expression expression;
  tostring(expression);
}

void writeToFileIfChanged(const std::string& filePath,
                          const std::string& newData) {
  std::string fileContent;
  readFile(filePath, fileContent);

  if (newData != fileContent) {
    std::ofstream outFile;
    outFile.open(filePath);
    if (!outFile.is_open()) {
      ASSERT(false);
    }
    outFile << newData;
  }
}

std::string transformTabsToSpaces(const std::string& source) {
  std::stringstream stream;
  for (char c : source) {
    if (c == (char)9) {
      stream << "    ";
    } else {
      stream << c;
    }
  }
  return stream.str();
}

int getNumberOfTabs(const std::string& source) {
  int tabs = 0;
  for (char c : source) {
    if (c == (char)9) {
      tabs += 1;
    }
  }
  return tabs;
}

std::string getSpaces(int spaces) {
  std::stringstream stream;
  for (int i = 0; i < spaces; i++) {
    stream << ' ';
  }
  return stream.str();
}

std::string getIndent(int spaces) {
  return getSpaces(spaces) + "> ";
}

std::string extractFileNameFromPath(const std::string& path);

namespace {
struct ASTData {
  std::vector<std::string> loadedFiles;
};
struct CompilerState {
  std::map<std::string, Lexer*> lexers;

  std::map<SyntaxTree*, ASTData> astMapping;
  std::map<std::string, SyntaxTree*> fileToAST;

  void insertLexer(const std::string& fileName, Lexer* lexer) {
    auto iter = lexers.find(fileName);
    ASSERT(iter == lexers.end());
    lexers[fileName] = lexer;
  }
  Lexer* getLexer(const std::string& fileName) {
    auto iter = lexers.find(fileName);
    ASSERT(iter != lexers.end());
    return iter->second;
  }

  void insertFile(const std::string& fileName, SyntaxTree* ast) {
    auto iter = fileToAST.find(fileName);
    ASSERT(iter == fileToAST.end());
    fileToAST[fileName] = ast;
  }
  SyntaxTree* findAST(const std::string& fileName) {
    auto iter = fileToAST.find(fileName);
    ASSERT(iter != fileToAST.end());
    return iter->second;
  }
  ASTData& getASTData(SyntaxTree* ast) { return astMapping[ast]; }
};
}  // namespace

void writeSourceCodeLines(astLocation location,
                          int lineCount,
                          int indentSpaces,
                          CompilerState& state) {
  Lexer* lexer = state.getLexer(*location.fileName);

  std::vector<std::string> rawLines =
      LexerGetTextBeforeLine(lexer, location.line, lineCount);
  std::vector<std::string> lines;
  for (auto& line : rawLines) {
    lines.push_back(transformTabsToSpaces(line));
  }

  int baseLine = location.line - static_cast<int>(lines.size());
  for (std::size_t i = 1; i < lines.size(); i++) {
    std::string lineString = make_string(baseLine + i) + " ";
    std::cout << getSpaces(indentSpaces - static_cast<int>(lineString.size()));
    std::cout << lineString;
    std::cout << getIndent(0);
    std::cout << lines[i] << std::endl;
  }

  int charArrowLength = location.charNumber;
  if (!rawLines.empty()) {
    // Tabs are 4 spaces, so we need to add 3 spaces for each tab character to
    // compensate
    charArrowLength += getNumberOfTabs(rawLines.back()) * 3;
  }
  std::cout << getIndent(indentSpaces);
  for (int i = 0; i < charArrowLength; i++) {
    std::cout << '-';
  }
  std::cout << '^' << std::endl;
}

void printErrorTemplateStack(const astError& error, CompilerState& state) {
  std::cout << "      Template instantiation stack:" << std::endl;
  int i = 0;
  for (auto iter = error.stackOfLocations.rbegin();
       iter != error.stackOfLocations.rend(); ++iter) {
    auto& location = *iter;
    std::string fileName = extractFileNameFromPath(*location.fileName);
    std::string msg =
        FORMAT("[@]: @:(@)", i, fileName, location.line, location.charNumber);
    std::cout << getSpaces(6) << msg << std::endl;
    writeSourceCodeLines(location, 1, 14, state);
    std::cout << std::endl;
    i += 1;
  }
}

void printError(const astError& error, CompilerState& state) {
  std::cout << std::endl;
  std::string fileName = extractFileNameFromPath(*error.location.fileName);
  std::string fullErrorMsg =
      FORMAT("Error at @(@): @", fileName, error.location.line, error.message);
  std::cout << fullErrorMsg << std::endl;
  std::cout << std::endl;

  writeSourceCodeLines(error.location, 3, 6, state);

  std::cout << std::endl;

  if (!error.stackOfLocations.empty()) {
    printErrorTemplateStack(error, state);
  }
}

std::string extractFileNameFromPath(const std::string& path) {
  std::size_t lastDirectory = path.find_last_of("/\\");
  ASSERT(lastDirectory != std::string::npos);
  return path.substr(lastDirectory + 1);
}

std::string extractDirectoryFromPath(const std::string& path) {
  std::size_t lastDirectory = path.find_last_of("/\\");
  ASSERT(lastDirectory != std::string::npos);
  return path.substr(0, lastDirectory + 1);
}

std::string getFileNameWithoutExtension(const std::string& path) {
  std::string name = extractFileNameFromPath(path);
  while (true) {
    std::size_t dotPosition = name.find_last_of(".");
    if (dotPosition == std::string::npos) {
      break;
    }
    name = name.substr(0, dotPosition);
  }
  return name;
}

double getCurrentTime() {
  return (double)std::chrono::duration_cast<std::chrono::milliseconds>(
             std::chrono::system_clock::now().time_since_epoch())
             .count() /
         1000;
}

struct MainGuard {
#if defined(_WIN32)
  ~MainGuard() {
    // system("pause");
  }
#endif
  void fixUnusedWarning() {}
};

int main(int argc, const char** argv) {
  MainGuard guard;
  guard.fixUnusedWarning();

  // OP unit testing
  // Incredible unit testing :D
  ASSERT(extractFileNameFromPath("main/File.vns") == "File.vns");
  ASSERT(extractFileNameFromPath("main\\File.vns") == "File.vns");
  ASSERT(extractFileNameFromPath("main/sub/File.vns") == "File.vns");

  ASSERT(extractDirectoryFromPath("main/File.vns") == "main/");
  ASSERT(extractDirectoryFromPath("main\\File.vns") == "main\\");
  ASSERT(extractDirectoryFromPath("main/sub/File.vns") == "main/sub/");

  ASSERT(getFileNameWithoutExtension("main/File.vns") == "File");
  ASSERT(getFileNameWithoutExtension("main/File.vns.2") == "File");
  ASSERT(getFileNameWithoutExtension("wtf.main/File.vns.2.3") == "File");

#if defined(VECTOR_INSERTION_TEST)
  testVectorInsertion();
  return 0;
#endif

  std::string inputFile = INPUT_FILE;
  if (argc >= 2) {
    inputFile = argv[1];
  }

  std::string outputPath = OUTPUT_FILE_PATH;
  if (argc >= 3) {
    outputPath = argv[2];
  }

  bool completedWithErrors = false;

  ASTCache cache;

  std::string workingDirectory = extractDirectoryFromPath(inputFile);

  std::vector<std::string> filesToCompile = {
      inputFile,
      workingDirectory + "VNS.vns",
  };

  CompilerState state;

  double timeBegin = getCurrentTime();

  for (int i = 0; i < (int)filesToCompile.size(); i++) {
    // NO-REF HERE. Files to compile is modified later in this loop!
    const std::string file = filesToCompile.at(i);

    LOG(INFO) << "Compiling file: " << file;
    std::string sourceData;
    if (!readFile(file, sourceData)) {
      ASSERT(false);
    }

    Lexer* lexer = new Lexer(file, sourceData);
    state.insertLexer(file, lexer);

    Parser parser(lexer);

    parsed::Scope scope;
    astError error;
    if (parser.parse(scope, &error)) {
      ASTData data;
      for (auto& loadFile : parser.getCommandsLoadFile()) {
        std::string fullPath = workingDirectory + loadFile;
        data.loadedFiles.push_back(fullPath);
        if (std::find(filesToCompile.begin(), filesToCompile.end(), fullPath) ==
            filesToCompile.end()) {
          filesToCompile.push_back(fullPath);
        }
      }
      SyntaxTree* ast = generateAst(module::GetModuleName(file), cache, scope);
      cache.trees.push_back(ast);
      state.insertFile(file, ast);
      state.getASTData(ast) = data;
    } else {
      printError(error, state);
      completedWithErrors = true;
      break;
    }
  }

  if (completedWithErrors) {
    return 1;
  }

  double timeParsing = getCurrentTime();

  for (auto& ast : cache.trees) {
    ASTData& data = state.getASTData(ast);
    for (auto& file : data.loadedFiles) {
      ast->scope->otherFileScopes.push_back(state.findAST(file)->scope);
    }
  }

  for (auto& ast : cache.trees) {
    std::vector<astError> errors;
    typeCheckTypes(ast, cache, errors);
    if (!errors.empty()) {
      completedWithErrors = true;
      for (auto& error : errors) {
        printError(error, state);
      }
    }
  }

  if (completedWithErrors) {
    return 1;
  }

  for (auto& ast : cache.trees) {
    instantiateArrayClasses(ast, cache);
  }

  if (completedWithErrors) {
    return 1;
  }

  for (auto& ast : cache.trees) {
    std::vector<astError> errors;
    resolveClassConstructors(ast, cache, errors);
    if (!errors.empty()) {
      completedWithErrors = true;
      for (auto& error : errors) {
        printError(error, state);
      }
    }
  }

  if (completedWithErrors) {
    return 1;
  }

#if 0
	for (auto & ast : cache.trees) {
		collapseTypeAliases(ast);
	}
#endif

  for (auto& ast : cache.trees) {
    astError error;
    if (runTypeCheck(ast, cache, &error)) {
    } else {
      printError(error, state);
      completedWithErrors = true;
      break;
    }
  }

  if (completedWithErrors) {
    LOG(INFO);
    return 1;
  }

  astModify(cache);

  double timeBeforeGeneratingCode = getCurrentTime();

  {
    std::string hFile;
    std::string cFile;
    generateC(cache, &hFile, &cFile);

    writeToFileIfChanged(outputPath + ".h", hFile);
    writeToFileIfChanged(outputPath + ".c", cFile);
  }

  double timeDone = getCurrentTime();

  LOG(INFO) << "Done";

  double timeTakenParsing = timeParsing - timeBegin;
  double timeTakenSemantic = timeBeforeGeneratingCode - timeParsing;
  double timeTakenCgeneration = timeDone - timeBeforeGeneratingCode;
  double timeTakenTotal = timeDone - timeBegin;

  auto formatTime = [timeTakenTotal](double value) {
    std::stringstream stream;
    stream << std::fixed;
    stream << std::setprecision(3);
    stream << value;
    stream << " (" << (int)(value / timeTakenTotal * 100) << "%)";
    return stream.str();
  };

  LOG(INFO) << "Time taken (Parsing)       : " << formatTime(timeTakenParsing);
  LOG(INFO) << "Time taken (Semantic)      : " << formatTime(timeTakenSemantic);
  LOG(INFO) << "Time taken (C generation)  : "
            << formatTime(timeTakenCgeneration);
  LOG(INFO) << "Time taken (Total)         : " << formatTime(timeTakenTotal);
}
