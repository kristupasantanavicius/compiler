#pragma once

#include <memory>

template <typename T>
class GrowList;

class Handle {
  Handle(int index) : index(index) {}
  const int index = 0;
};

template <typename T>
class GrowList {
 public:
  ~GrowList() {}

  Handle pushBack(const T& object) {
    int index = mSize;

    if (mBlocks.empty()) {
      mCapacity = 1;
      mBlocks.push_back(allocateBlock(mCapacity));
    }
    if (mSize >= mCapacity) {
      mBlocks.push_back(allocateBlock(mCapacity));
      mCapacity *= 2;
    }
    mSize += 1;
  }

  int getSize() const { return mSlots.size(); }

 private:
  struct Block {
    int size = 0;
    T* pointer = nullptr;
  };
  typedef std::unique_ptr<Block> UPBlock;

#if 0
	Block * queryBlock(int index)
	{
		if (index < 0 || index >= mSize) {
			return nullptr;
		}
		if (mRoot == nullptr) {
			return nullptr;
		}
		Block * block = mRoot;
		int i = 0;
		while (true) {

		}
		return nullptr;
	}
#endif

  static UPBlock allocateBlock(int size) {
    Block* block = new Block();
    block->size = size;
    block->pointer = new T[size];
    return UPBlock(block);
  }

 private:
  int mCapacity = 0;
  std::vector<UPBlock> mBlocks;
  std::vector<Handle> mSlots;
};
