#pragma once

#include <compiler/Error.h>
#include <stack>

template <typename T>
class Stack {
 public:
  void push(T t) { stack.push(t); }
  void pop(T t) {
    ASSERT(!stack.empty());
    ASSERT(getCurrent() == t);
    stack.pop();
  }
  T getCurrent() {
    ASSERT(!stack.empty());
    return stack.top();
  }

 private:
  std::stack<T> stack;
};
