#include <compiler/Error.h>
#include <compiler/Format.h>

std::string stringFormatPrivateImpl(const std::string& format,
                                    const std::string* args,
                                    std::size_t argCount) {
  std::string output;
  std::size_t argIndex = 0;
  bool escape = false;

  for (int i = 0; i < (int)format.size(); i++) {
    if (escape) {
      escape = false;
      output += format[i];
    } else {
      if (format[i] == '\\') {
        escape = true;
      } else if (format[i] == '@') {
        if (argIndex >= argCount) {
          ASSERT(false); // Not enough parameters
          output += "[ERROR]";
        } else {
          output += args[argIndex];
          argIndex++;
        }
      } else {
        output += format[i];
      }
    }
  }
  return output;
}
