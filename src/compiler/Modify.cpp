#include <compiler/Error.h>
#include <compiler/ExpressionVariable.h>
#include <compiler/Format.h>
#include <compiler/Modify.h>
#include <compiler/Stack.h>
#include <compiler/TypeCheck.h>
#include <compiler/astDump.h>
#include <algorithm>
#include <unordered_set>

namespace {

enum class StatementQueueLevel : uint8_t {
  // TODO: Give these better names?
  Before1,           // Temp variable definitions??
  DestructorBefore,  // For 'return' statement destructors, they need to be
                     // Before, but after first level of before
  After1,            // Copy constructors?
  DestructorAfter,
};

struct BlockInfo {
  BlockInfo(astBlock* block) : block(block) {}

  astBlock* block = nullptr;

  bool hasReturned = false;

  std::vector<astVariable*> declaredVariables;

  struct QueueItem {
    StatementQueueLevel level;
    astStatement* statement;
  };

  void queueStatement(StatementQueueLevel level, astStatement* statement) {
    statementQueue.push_back({level, statement});
  }

  std::vector<astStatement*> getStatementQueue(StatementQueueLevel level) {
    std::vector<astStatement*> vec;
    for (auto& item : statementQueue) {
      if (item.level == level) {
        vec.push_back(item.statement);
      }
    }
    return vec;
  }

  void clearStatementQueue() { statementQueue.clear(); }

 private:
  // These have to be scope for each block, otherwise we get code like this:
  // temp variable is generated in the if(), but its inserted into the block
  // of the if statement, because of the order in which queues are processed.
  // The fix is to separate queues for each block.
  // if (temp10) {
  //     var temp10 = 123;
  // }

  std::vector<QueueItem> statementQueue;
};

struct FunctionInfo {
  FunctionInfo(astFunction* function) : function(function) {}
  astFunction* function = nullptr;
  Stack<BlockInfo> blocks;

  std::vector<astVariable*> concatenateCurrentVariables() {
    std::vector<astVariable*> vec;
    for (auto& blockInfo : blocks.iterate()) {
      vec.insert(vec.end(), blockInfo.declaredVariables.begin(),
                 blockInfo.declaredVariables.end());
    }
    return vec;
  }
};

class Visitor;
typedef int TempVariableId;

enum class DestructorPlacementForTempValues {
  After,   // Default case; destructors are placed after the statemnet
  Return,  // Special case for 'return' statement. Destructors need to be placed
           // before the return statement.
};

struct State {
  SyntaxTree* ast = nullptr;

  std::unique_ptr<Visitor> visitor;

  TempVariableId nextTempVariableId = 0;

  Stack<FunctionInfo> functions;

  Stack<astScope*> scopeStack;

  BlockInfo& blockInfo() { return functions.getCurrent().blocks.getCurrent(); }

  DestructorPlacementForTempValues destructorPlacement =
      DestructorPlacementForTempValues::After;

  // TODO: This should be based on some kind of serial ID, not memory location.
  // It could be that statements are deallocated and new statements are created
  // on top.
  std::unordered_set<astStatement*> skipStatementMod;
};

#if 0
astExpression* recurseFindFirstMemberFunctionCall(astExpression* expression,
                                                  bool allowFunctionCall,
                                                  astExpression** beginning) {
  if (expression->symbol) {
    if (allowFunctionCall &&
        expression->symbol->type == astSymbolType::FunctionCall) {
      return expression;
    }
  } else {
    ASSERT(expression->left);
    ASSERT(expression->right);
    if (expression->operatorToken != astOperator::Dot) {
      *beginning = expression->right;
    }
    const bool leftSideIsMemberFunctionCall = false;
    if (auto left = recurseFindFirstMemberFunctionCall(
            expression->left, leftSideIsMemberFunctionCall, beginning)) {
      return left;
    }
    const bool rightSideIsMemberFunctionCall = true;
    if (auto right = recurseFindFirstMemberFunctionCall(
            expression->right, rightSideIsMemberFunctionCall, beginning)) {
      return right;
    }
  }
  return nullptr;
}

astExpression* findFirstMemberFunctionCall(astExpression* expression,
                                           astExpression** beginning) {
  const bool firstExpressionNodeAsFunctionCallIsMember = false;
  astExpression* found = recurseFindFirstMemberFunctionCall(
      expression, firstExpressionNodeAsFunctionCallIsMember, beginning);
  if (found != nullptr && *beginning == nullptr) {
    *beginning = expression;
  }
  return found;
}

astExpression* findParentExpression(astExpression* expression,
                                    astExpression* target) {
  if (expression->symbol) {
    return nullptr;
  } else {
    if (expression->left == target) {
      return expression;
    }
    if (expression->right == target) {
      return expression;
    }
    if (auto left = findParentExpression(expression->left, target)) {
      return left;
    }
    if (auto right = findParentExpression(expression->right, target)) {
      return right;
    }
    return nullptr;
  }
}

void expressionExtractMemberFunctionCallArgument(astExpression* expression,
                                                 State* state) {
  astExpression* beginning = nullptr;
  if (astExpression* memberExpression =
          findFirstMemberFunctionCall(expression, &beginning)) {
    ASSERT(memberExpression->symbol);
    ASSERT(memberExpression->symbol->functionCall2);
    ASSERT(memberExpression->symbol->functionCall2->function);
    ASSERT(memberExpression->symbol->functionCall2->function->memberOf);
    // std::cout << FORMAT("found member function call: '@'",
    // memberExpression->symbol->functionCall2->stringName) << std::endl;

    ASSERT(beginning);
    astExpression* callParentExpression =
        findParentExpression(expression, memberExpression);
    astExpression* beginningParentExpression =
        findParentExpression(expression, beginning);

    if (expression !=
        beginning) {  // If the beginning of the expression is the root itself,
                      // that means there can't be a parent for it
      ASSERT(beginningParentExpression);
    }

    // Extract argument expression for the member function call 1st argument.
    ASSERT(callParentExpression->right == memberExpression);
    *callParentExpression =
        *callParentExpression
             ->left;  // Disconnect before the member function call

    astExpression* shallowCopy = new astExpression();
    *shallowCopy = *beginning;
    auto iter = memberExpression->symbol->functionCall2->arguments.begin();
    memberExpression->symbol->functionCall2->arguments.insert(iter,
                                                              shallowCopy);

    // The old beginning of the expression is now the member function call
    *beginning = *memberExpression;
  }
}

astExpression* recursiveFindFirstLeftSideTempValue(astExpression* expression,
                                                   bool isLeft,
                                                   astExpression** parent) {
  if (expression->symbol) {
    if (isLeft && expression->symbol->type == astSymbolType::FunctionCall) {
      return expression;
    }
  } else {
    ASSERT(expression->left);
    ASSERT(expression->right);
    *parent = expression;
    if (auto left = recursiveFindFirstLeftSideTempValue(expression->left, true,
                                                        parent)) {
      return left;
    }
    bool rightSideIsTempValue =
        false;  // buildingId: = building.getId(); getId() is not a temp value,
                // since nothing is accessed from it
    if (auto right = recursiveFindFirstLeftSideTempValue(
            expression->right, rightSideIsTempValue, parent)) {
      return right;
    }
  }
  return nullptr;
}

astExpression* findFirstLeftSideTempValue(astExpression* expression,
                                          astExpression** parent) {
  // If the first expression node is a symbol, we don't consider it a temp value
  // ??
  const bool firstSymbolIsTempValue = false;
  return recursiveFindFirstLeftSideTempValue(expression, firstSymbolIsTempValue,
                                             parent);
}
#endif

std::string generateTempVariableName(State* state) {
  // TODO: Maybe this can be done at a function level?/
  // The reason is so that in generated code it would only have small ids,
  // because like it is now globally there will eventually be very high temp
  // variable ids.
  std::string name = "tempVar" + std::to_string(state->nextTempVariableId);
  state->nextTempVariableId += 1;
  return name;
}

astStatementExpression* generateCopyConstructorStatement(
    ExpressionVariable target,
    astScope* scope,
    State* state);
astStatementExpression* generateDestructorStatement(ExpressionVariable target,
                                                    astScope* scope,
                                                    State* state);

astFunctionCall2* expressionEndsWithFunctionCall(astExpression* expression) {
  ASSERT(!expression->nodes.empty());
  astExpressionNode* lastNode = expression->nodes.back();
  if (astSymbol* symbol = lastNode->getSymbol()) {
    if (symbol->functionCall2) {
      return symbol->functionCall2;
    }
  } else if (astExpression* subExpression = lastNode->getSubExpression()) {
    return expressionEndsWithFunctionCall(subExpression);
  }

  return nullptr;
}

bool needCopyConstructor(astExpression* expression) {
#if 0
  if (expression->symbol) {
    switch (expression->symbol->type) {
      case astSymbolType::Variable: {
        return true;
      }
      case astSymbolType::Literal: {
        return true;
      }
      // FUNCTION CALL ALREADY RETURNS A COPIED OBJECT, NO NEED TO COPY IT
      // AGAIN!
      case astSymbolType::FunctionCall: {
        return false;
      }
    }
    ASSERT(false);
    return false;
  } else {
    ASSERT(expression->left);
    ASSERT(expression->right);
    return needCopyConstructor(expression->right);
  }
#endif

  // FUNCTION CALL ALREADY RETURNS A COPIED OBJECT, NO NEED TO COPY IT AGAIN!
  return !expressionEndsWithFunctionCall(expression);
}

astVariable* generateTempVariable(astExpression* assignExpression,
                                  astType* tempType,
                                  bool generateDestructor,
                                  astLocation location,
                                  astScope* scope,
                                  State* state) {
  location.sanity();

  astVariable* var = new astVariable();
  var->location = location;
  var->name = generateTempVariableName(state);
  var->memberOf = nullptr;
  var->type = tempType->COPY();

  astVariableDecl* decl = new astVariableDecl(scope);
  decl->variable = var;
  decl->expression = assignExpression;

  state->blockInfo().queueStatement(StatementQueueLevel::Before1, decl);
  if (needCopyConstructor(assignExpression)) {
    if (auto copyConstructor =
            generateCopyConstructorStatement(var, scope, state)) {
      state->blockInfo().queueStatement(StatementQueueLevel::Before1,
                                        copyConstructor);
    }
  }
  if (generateDestructor) {
    if (auto destructor = generateDestructorStatement(var, scope, state)) {
      if (state->destructorPlacement ==
          DestructorPlacementForTempValues::After) {
        state->blockInfo().queueStatement(StatementQueueLevel::DestructorAfter,
                                          destructor);
      } else if (state->destructorPlacement ==
                 DestructorPlacementForTempValues::Return) {
        state->blockInfo().queueStatement(StatementQueueLevel::DestructorBefore,
                                          destructor);
      } else {
        ASSERT(false);
      }
    }
  }

  return var;
}

void modExpression(astExpression* expression,
                   State* state,
                   bool withTempValues = true);

void modFunctionCallArguments(astFunctionCall2* call, State* state) {
  for (int i = 0; i < (int)call->arguments.size(); i++) {
    astExpression* argument = call->arguments.at(i);

    modExpression(argument, state);

    int offset = 0;
    if (call->function->memberOf) {
      if (i == 0) {
        continue;
      }
      offset = -1;
    }

    if (astExpressionNode* node = argument->getEndingSymbol()) {
      astSymbol* symbol = node->getSymbol();
      if (call->function->parameters.at(i + offset)->type->name ==
          astTypeName::Reference) {
        bool createTemp = false;
        createTemp |= symbol->constant && isNumericType(symbol->constant->type);
        createTemp |= symbol->functionCall2 != nullptr;
        if (createTemp) {
          astVariable* tempVariable =
              generateTempVariable(argument, argument->flatType, true,
                                   argument->location, call->scope, state);
          astExpression* expression = astWrapSymbolInExpression(
              astWrapVariableInSymbol(tempVariable, call->scope, true));
          modExpression(expression, state);
          call->arguments[i] = expression;
        }
      }
    }
  }
}

void modExpressionFunctionCallArguments(astExpression* expression,
                                        State* state) {
#if 0
	if (expression->symbol) {
		if (expression->symbol->functionCall2) {
			for (auto & argument : expression->symbol->functionCall2->arguments) {
				modExpression(argument, state);
			}
		}
	}
	else {
		ASSERT(expression->left);
		ASSERT(expression->right);
		modExpressionFunctionCallArguments(expression->left, state);
		modExpressionFunctionCallArguments(expression->right, state);
	}
#endif

  for (astExpressionNode* node : expression->nodes) {
    if (astSymbol* symbol = node->getSymbol()) {
      if (symbol->functionCall2) {
        modFunctionCallArguments(symbol->functionCall2, state);
      }
    }
  }
}

int findMemberFunctionCall(astExpression* expression) {
  for (std::size_t i = 0; i < expression->nodes.size(); i++) {
    astExpressionNode* node = expression->nodes.at(i);
    if (astOperatorInstance* op = node->getOperator()) {
      if (op->op == astOperator::Dot) {
        astExpressionNode* memberNode = expression->nodes.at(i + 1);
        if (astSymbol* symbol = memberNode->getSymbol()) {
          if (symbol->functionCall2) {
            return static_cast<int>(i);
          }
        }
      }
    }
  }
  return -1;
}

void removeMemberFunctionCalls(astExpression* expression, State* state) {
  base::MaybeUnused(state);

  while (true) {
    int index = findMemberFunctionCall(expression);
    if (index < 0) {
      break;
    }

    astExpressionNode* left = expression->nodes.at(index - 1);
    astExpressionNode* nodeOperator = expression->nodes.at(index);
    astExpressionNode* right = expression->nodes.at(index + 1);

    ASSERT(left->getSymbol() || left->getSubExpression());
    ASSERT(nodeOperator->getOperator());
    ASSERT(right->getSymbol());

    astFunctionCall2* call = right->getSymbol()->functionCall2;
    ASSERT(call);

    TODO() << "Check if constructor needs to be considered";
    // if (!call->function->constructorFunction) {
    astExpression* memberExpression =
        new astExpression(left->getLocation(), left->getNodeType());
    memberExpression->nodes.push_back(left);
    call->arguments.insert(call->arguments.begin(), memberExpression);
    // } else {
    //   // TODO(leak) left
    // }

    auto iter = expression->nodes.begin() + index - 1;
    iter = expression->nodes.erase(iter);  // Erase 'left' node
    iter = expression->nodes.erase(iter);  // Erase 'nodeOperator' node

    delete nodeOperator;
  }
}

void moveExpressionNodes(astExpression* from, astExpression* to) {
  for (auto& node : from->nodes) {
    to->nodes.push_back(node);
  }
  from->nodes.clear();
}

astClass* typeIsConstructable(astType* type) {
  return type->getClass();
}

astExpressionNode* storeNodeIntoTemp(astExpressionNode* node, State* state) {
  astExpression* expression =
      new astExpression(node->getLocation(), node->getNodeType());

  // 1) This expression is already fully modified at this point, but since this
  // is the last modify step the flag hasn't been set
  // 2) Trying to modify this expression, which has already been modified,
  // creates stack overflow, so just set the modified flag.
  expression->setModifiedFlag();

  expression->nodes.push_back(node);

  astScope* scope = state->scopeStack.getCurrent();

  astVariable* tempVariable =
      generateTempVariable(expression, expression->flatType, true,
                           expression->location, scope, state);

  astSymbol* tempSymbol = astWrapVariableInSymbol(tempVariable, scope, true);
  ASSERT(tempSymbol->variable2->variable);
  return new astExpressionNode(tempSymbol);
}

bool maybeConvertExpressionNodeToReference(astExpressionNode* node) {
  astExpression* expression = node->getSubExpression();
  if (!expression) {
    return false;
  }

  if (expression->nodes.size() < 3) {
    return false;
  }

  astOperatorInstance* op = expression->nodes[1]->getOperator();
  if (!op) {
    return false;
  }

  if (op->op != astOperator::Dot) {
    return false;
  }

  expression->flatType =
      astWrapTypeInMutableReference(expression->flatType, expression->location);

  ASSERT(!expression->reference_of);
  expression->reference_of = true;

  return true;
}

void boilDownExpression(astExpression* expression,
                        bool withTempValues,
                        State* state) {
  if (expression->nodes.size() <= 1 && !withTempValues) {
    return;
  }

  for (std::size_t i = 0; i < expression->nodes.size(); i++) {
    astExpressionNode* node = expression->nodes.at(i);

    if (node->getOperator()) {
      continue;
    }

    if (astSymbol* symbol = node->getSymbol()) {
      if (symbol->variable2) {
        continue;
      }
    }

    bool reference = maybeConvertExpressionNodeToReference(node);

    expression->nodes[i] = storeNodeIntoTemp(node, state);
    expression->nodes[i]->dereference = reference;
  }
}

void setDereferenceFlag(astExpression* expression) {
  if (expression->nodes.size() < 2) {
    return;
  }

  astExpressionNode* left = expression->nodes[0];

  astOperatorInstance* operatorInstance = expression->nodes[1]->getOperator();
  if (!operatorInstance) {
    return;
  }

  if (left->getNodeType()->name == astTypeName::Reference &&
      operatorInstance->op == astOperator::Dot) {
    left->dereference = true;
  }
}

void modExpression(astExpression* expression,
                   State* state,
                   bool withTempValues) {
  ASSERT(!expression->modified);

  removeMemberFunctionCalls(expression, state);

  for (astExpressionNode* node : expression->nodes) {
    if (astExpression* subExpression = node->getSubExpression()) {
      modExpression(subExpression, state, false);
    }
  }

  modExpressionFunctionCallArguments(expression, state);

  boilDownExpression(expression, withTempValues, state);

  setDereferenceFlag(expression);

  expression->setModifiedFlag();
}

astFunctionCall2* generateFunctionCall(astFunction* function,
                                       astLocation location,
                                       astScope* scope) {
  astFunctionCall2* call = new astFunctionCall2();
  call->location = location;
  call->scope = scope;
  call->function = function;
  return call;
}

void inlineExpression(astExpression* dst, astExpression* src) {
  for (auto& node : src->nodes) {
    dst->nodes.push_back(node);
  }
  delete src;
}

astExpression* generateFunctionCallExpression(ExpressionVariable target,
                                              astFunction* function,
                                              astScope* scope,
                                              State* state) {
  astClass* aclass = target.getType()->getClass();
  ASSERT(aclass);
  astExpression* expression =
      new astExpression(target.getLocation(), target.getType()->COPY());

#if 0
  expression->operatorToken = astOperator::Dot;

  expression->left = target.getUnresolvedExpression(scope);

  expression->right = new astExpression();
  expression->right->symbol = new astSymbol(astSymbolType::FunctionCall);
  expression->right->symbol->functionCall2 =
      generateFunctionCall(function, target.getLocation(), scope);
  // expression->right->symbol->functionCall2 = new astFunctionCall2();
  // expression->right->symbol->functionCall2->location = target.getLocation();
  // expression->right->symbol->functionCall2->scope = scope;
  // expression->right->symbol->functionCall2->stringName = function->name;
  // expression->right->symbol->functionCall2->function = function;
  inferExpressionTypeBasedOnSymbol(expression->right);
#else
  inlineExpression(expression, target.getUnresolvedExpression(scope));
  expression->nodes.push_back(new astExpressionNode(
      astOperatorInstance(astOperator::Dot, expression->location)));

  astExpression* callExpression = astWrapSymbolInExpression(new astSymbol(
      generateFunctionCall(function, target.getLocation(), scope)));
  // expression->nodes.push_back(new astExpressionNode(callExpression));
  inlineExpression(expression, callExpression);
#endif

  modExpression(expression, state, false);

  return expression;
}

astStatementExpression* generateStatementExpression(astExpression* expression,
                                                    State* state) {
  astStatementExpression* statement = new astStatementExpression();
  statement->location = expression->location;
  statement->expression = expression;

  // Sanity check.. since we use in-memory location and not unique ID
  ASSERT(state->skipStatementMod.find(statement) ==
         state->skipStatementMod.end());
  state->skipStatementMod.insert(statement);

  return statement;
}

astStatementExpression* generateCopyConstructorStatement(
    ExpressionVariable target,
    astScope* scope,
    State* state) {
  astClass* aclass = typeIsConstructable(target.getType());
  if (aclass == nullptr) {
    return nullptr;
  }
  ASSERT(aclass->copyConstructor);

  astExpression* call = generateFunctionCallExpression(
      target, aclass->copyConstructor, scope, state);
  astStatementExpression* statement = generateStatementExpression(call, state);
  return statement;
}

void insertCopyConstructor(ExpressionVariable target,
                           astScope* scope,
                           State* state) {
  astStatementExpression* statement =
      generateCopyConstructorStatement(target, scope, state);
  if (statement) {
    state->blockInfo().queueStatement(StatementQueueLevel::After1, statement);
  }
}

astStatementExpression* generateDestructorStatement(ExpressionVariable target,
                                                    astScope* scope,
                                                    State* state) {
  astClass* aclass = typeIsConstructable(target.getType());
  if (aclass == nullptr) {
    return nullptr;
  }
  ASSERT(aclass->destructor);

  astExpression* call =
      generateFunctionCallExpression(target, aclass->destructor, scope, state);
  astStatementExpression* statement = generateStatementExpression(call, state);
  return statement;
}

void insertDestructor(ExpressionVariable target,
                      astScope* scope,
                      bool before,
                      State* state) {
  astStatementExpression* statement =
      generateDestructorStatement(target, scope, state);
  if (statement) {
    if (before) {
      state->blockInfo().queueStatement(StatementQueueLevel::DestructorBefore,
                                        statement);
    } else {
      state->blockInfo().queueStatement(StatementQueueLevel::DestructorAfter,
                                        statement);
    }
  }
}

void modReturn(astReturn* ret, State* state) {
  state->functions.getCurrent().blocks.getCurrent().hasReturned = true;

  if (ret->expression) {
    state->destructorPlacement = DestructorPlacementForTempValues::Return;
    const bool withTempValues = false;  // Avoid creating a useless temp value
    modExpression(ret->expression, state, withTempValues);
    state->destructorPlacement = DestructorPlacementForTempValues::After;
    const bool generateDestructor =
        false;  // Don't generate destructor for this temp value, since a
                // function returns a ready-to-use value!
    astType* tempVariableType = removeReference(
        ret->expression
            ->flatType);  // Convert possible reference type of return statement
                          // to match function's by value return type
    astVariable* tempVariable = generateTempVariable(
        ret->expression, tempVariableType, generateDestructor, ret->location,
        ret->scope, state);
    bool resolvedSymbol = true;  // It must be already resolved at this stage.
    astExpression* newRoot = astWrapSymbolInExpression(
        astWrapVariableInSymbol(tempVariable, ret->scope, resolvedSymbol));
    modExpression(newRoot, state);
    ret->expression = newRoot;
  }

  std::vector<astVariable*> declaredVariables =
      state->functions.getCurrent().concatenateCurrentVariables();
  for (auto iter = declaredVariables.rbegin(); iter != declaredVariables.rend();
       ++iter) {
    astVariable* variable = *iter;
    astStatementExpression* statement =
        generateDestructorStatement(variable, ret->scope, state);
    if (statement) {
      state->blockInfo().queueStatement(StatementQueueLevel::Before1,
                                        statement);
    }
  }
}

void modBlock(astBlock* block, astScope* scope, State* state);
void modBlockWithVariables(astBlock* block,
                           astScope* scope,
                           const std::vector<astVariable*>* variables,
                           State* state);
void modScope(astScope* scope, State* state);

void modFunction(astFunction* function, State* state) {
  if (function->arrayFunction) {
    // primarily for Array_set function. It has a parameter, but we should not
    // insert any code for it, since code generator will provide all the code
    // needed.
    return;
  }

  state->functions.push(FunctionInfo(function));
  for (auto& parameter : function->parameters) {
    astStatementExpression* statement =
        generateCopyConstructorStatement(parameter, function->scope, state);
    if (!statement) {
      continue;
    }
    function->block->statements.insert(function->block->statements.begin(),
                                       statement);
  }
  modScope(function->scope, state);
  modBlockWithVariables(function->block, function->scope, &function->parameters,
                        state);

  if (function->defaultVoidReturn) {
    function->block->statements.push_back(
        astNewDefaultReturn(function->location, function->scope));
  }

  state->functions.pop();
}

void generateClassConstructor(astClass* aclass, State* state) {
  base::MaybeUnused(state);

  astFunction* constructor = aclass->instance_constructor;
  int position = 0;

  for (auto& variable : aclass->scope->variables) {
    astVariableAssign* assign = new astVariableAssign(constructor->scope);
    assign->location = constructor->location;

    assign->target = astWrapSymbolInExpression(
        astWrapVariableInSymbol(variable, constructor->scope, true));

    assign->expression = astWrapSymbolInExpression(astWrapVariableInSymbol(
        constructor->parameters.at(position), constructor->scope, true));

    constructor->block->statements.insert(
        constructor->block->statements.begin() + position, assign);

    position += 1;
  }
}

void generateClassCopyConstructor(astClass* aclass, State* state) {
  if (!aclass->copyConstructor) {
    return;
  }

  astFunction* copyConstructor = aclass->copyConstructor;

  int position = 0;
  for (auto& variable : aclass->scope->variables) {
    astStatementExpression* statement = generateCopyConstructorStatement(
        variable, copyConstructor->scope, state);
    if (!statement) {
      continue;
    }
    copyConstructor->block->statements.insert(
        copyConstructor->block->statements.begin() + position, statement);
    position += 1;
  }
}

void generateClassDestructor(astClass* aclass, State* state) {
  if (!aclass->destructor) {
    return;
  }
  astFunction* destructor = aclass->destructor;

  for (auto iter = aclass->scope->variables.rbegin();
       iter != aclass->scope->variables.rend(); ++iter) {
    astVariable* variable = *iter;
    astStatementExpression* statement =
        generateDestructorStatement(variable, destructor->scope, state);
    if (!statement) {
      continue;
    }
    destructor->block->statements.push_back(statement);
  }
}

void insertCompareWithFalse(astExpression* expression) {
  ASSERT(expression->flatType->name == astTypeName::Bool);
  astExpression* wrapped =
      new astExpression(expression->location, expression->flatType);
  moveExpressionNodes(expression, wrapped);

  expression->nodes.push_back(new astExpressionNode(wrapped));

  expression->nodes.push_back(new astExpressionNode(
      astOperatorInstance(astOperator::CompareEquals, expression->location)));

  astLiteral* literal = new astLiteral();
  literal->location = expression->location;
  literal->type = astNewBasicType(astTypeName::Bool, expression->location);
  literal->valueNumeric = 0;
  literal->valueString = "false";
  expression->nodes.push_back(new astExpressionNode(new astSymbol(literal)));
}

void modClass(astClass* aclass, State* state) {
  generateClassConstructor(aclass, state);
  generateClassCopyConstructor(aclass, state);
  generateClassDestructor(aclass, state);
  modScope(aclass->scope, state);
}

void modGlobalVariable(astVariable* variable, State* state) {
  if (!variable->global) {
    return;
  }
  if (variable->initializeWithExpression) {
    modExpression(variable->initializeWithExpression, state);
  }
}

void modScope(astScope* scope, State* state) {
  state->scopeStack.push(scope);
  for (auto& aclass : scope->classes) {
    modClass(aclass, state);
  }
  for (auto& aclass : scope->instantiatedTemplatedClasses) {
    modClass(aclass, state);
  }
  for (auto& variable : scope->variables) {
    modGlobalVariable(variable, state);
  }
  for (auto& function : scope->functions) {
    modFunction(function, state);
  }
  for (auto& function : scope->instantiatedTemplatedFunctions) {
    modFunction(function, state);
  }
  state->scopeStack.pop();
}

class Visitor : public StatementVisitor {
 public:
  State* state = nullptr;
  Visitor(State* state) : state(state) {}

  virtual ~Visitor() {}

  void visit(astVariableDecl* decl) override {
    state->functions.getCurrent()
        .blocks.getCurrent()
        .declaredVariables.push_back(decl->variable);
    // This avoids unnecessary copies eg: building:= Building();
    bool withTempValues = false;

    if (decl->expression) {
      modExpression(decl->expression, state, withTempValues);
      if (needCopyConstructor(decl->expression)) {
        insertCopyConstructor(decl->variable, decl->scope, state);
      }
    } else if (astClass* aclass = typeIsConstructable(decl->variable->type)) {
      decl->expression = astWrapSymbolInExpression(new astSymbol(
          generateFunctionCall(aclass->default_constructor,
                               decl->variable->location, decl->scope)));
      modExpression(decl->expression, state, withTempValues);
    }
  }
  void visit(astVariableAssign* assign) override {
    bool withTempValues = true;
    if (assign->variable_decl) {
      state->functions.getCurrent()
          .blocks.getCurrent()
          .declaredVariables.push_back(assign->variable_decl);
      // This avoids unnecessary copies eg: building = Building();
      withTempValues = false;
    }
    modExpression(assign->target, state);
    modExpression(assign->expression, state, withTempValues);
    if (removeReference(assign->expression->flatType)->getClass()) {
      if (!assign->variable_decl &&
          !state->functions.getCurrent().function->constructorFunction) {
        // Don't destroy the value inside a constructor; thats because inside
        // a constructor, it is the first time that the value is being created.
        insertDestructor(assign->target, assign->scope, true, state);
      }
      if (needCopyConstructor(assign->expression)) {
        insertCopyConstructor(assign->target, assign->scope, state);
      }
    }
  }

  void visit(astIf* anIf) override {
    modExpression(anIf->expression, state);
    modScope(anIf->scope, state);
    modBlock(anIf->block, anIf->scope, state);
    modScope(anIf->elseScope, state);
    modBlock(anIf->elseBlock, anIf->elseScope, state);
  }
  void visit(astWhile* awhile) override {
    astIf* statementIf = astNewIf(awhile->scope);

    insertCompareWithFalse(awhile->expression);
    statementIf->expression = awhile->expression;
    awhile->expression = nullptr;

    statementIf->block->statements.push_back(
        new astStatementBreak(awhile->location));
    awhile->block->statements.insert(awhile->block->statements.begin(),
                                     statementIf);

    modScope(awhile->scope, state);
    modBlock(awhile->block, awhile->scope, state);
  }
  void visit(astReturn* ret) override { modReturn(ret, state); }
  void visit(astStatementExpression* statementExpression) override {
    modExpression(statementExpression->expression, state, false);
  }
  void visit(astStatementBreak*) override {
    // Nothing..
  }
  void visit(astStatementContinue*) override {
    // Nothing..
  }
  void visit(astStatementNative*) override {
    // Nothing..
  }
};

void modBlockWithVariables(astBlock* block,
                           astScope* scope,
                           const std::vector<astVariable*>* variables,
                           State* state) {
  state->functions.getCurrent().blocks.push(BlockInfo(block));

  if (variables) {
    std::vector<astVariable*>& dst =
        state->functions.getCurrent().blocks.getCurrent().declaredVariables;
    dst.insert(dst.end(), variables->begin(), variables->end());
  }

  ASSERT(state->visitor);
  for (auto iter = block->statements.begin(); iter != block->statements.end();
       ++iter) {
    astStatement* statement = *iter;

    auto skipIter = state->skipStatementMod.find(statement);
    bool shouldSkip = skipIter != state->skipStatementMod.end();
    if (!shouldSkip) {
      statement->acceptVisitor(*state->visitor);
    }

    auto putBefore = [&iter,
                      block](const std::vector<astStatement*>& statements) {
      for (astStatement* statement : statements) {
        iter = block->statements.insert(iter, statement);
        iter += 1;
      }
    };
    auto putAfter = [&iter,
                     block](const std::vector<astStatement*>& statements) {
      for (astStatement* statement : statements) {
        iter += 1;
        iter = block->statements.insert(iter, statement);
      }
    };
    // Destructor order needs to be reversed
    auto reverse = [](const std::vector<astStatement*>& input) {
      std::vector<astStatement*> statements = input;
      std::reverse(statements.begin(), statements.end());
      return statements;
    };

    putBefore(
        state->blockInfo().getStatementQueue(StatementQueueLevel::Before1));
    putBefore(reverse(state->blockInfo().getStatementQueue(
        StatementQueueLevel::DestructorBefore)));
    putAfter(state->blockInfo().getStatementQueue(StatementQueueLevel::After1));
    putAfter(reverse(state->blockInfo().getStatementQueue(
        StatementQueueLevel::DestructorAfter)));
    state->blockInfo().clearStatementQueue();
  }

  if (!state->functions.getCurrent().blocks.getCurrent().hasReturned) {
    const std::vector<astVariable*>& declaredVariables =
        state->functions.getCurrent().blocks.getCurrent().declaredVariables;
    for (auto iter = declaredVariables.rbegin();
         iter != declaredVariables.rend(); ++iter) {
      astVariable* variable = *iter;
      astStatementExpression* statement =
          generateDestructorStatement(variable, scope, state);
      if (statement) {
        block->statements.push_back(statement);
      }
    }
  }

  state->functions.getCurrent().blocks.pop();
}
void modBlock(astBlock* block, astScope* scope, State* state) {
  modBlockWithVariables(block, scope, nullptr, state);
}

}  // namespace

void astModify(ASTCache& cache) {
  State state;
  state.visitor.reset(new Visitor(&state));
  for (auto& aclass : cache.arrayClasses) {
    modClass(aclass, &state);
  }
  for (auto& ast : cache.trees) {
    modScope(ast->scope, &state);
  }
}
