#pragma once

// C Shared pointer!!! Thread safe!!!!
// https://codereview.stackexchange.com/questions/168997/thread-safe-shared-pointer-implementation-in-c
// C Shared pointer!!! Thread safe!!!!

// This is a hack from stack overflow
// https://stackoverflow.com/questions/1135841/c-multiline-string-literal
//
// V0G0N seems to be a special character which allows to write any text without
// having to worry about escaping the string.
//
// It allows adding any text as part of binary file, instead of reading it from
// disk.
//

struct inlineC {
  static const char* main() {
    return R"V0G0N(
void main_entry()
{
	initialize_type_infos();
	initialize_strings();
	initialize_globals();
	system_init();
	main_impl();
}

int main(int argc, const char** argv)
{
	main_entry();
#if defined(_WIN32)
	system("pause");
#endif
	return 0;
}
)V0G0N";
  }

  static const char* bootstrap() {
    return R"V0G0N(
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#define runtime_crash() (*(volatile int*)NULL) = 0

// C doesn't have a native bool type, invent our own!
typedef unsigned char BOOL;

#if __cplusplus
	// extern "C" doesn't compile inside C file, but without it c++ and c code doesn't link
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

EXTERN_C void main_entry();

// Generated code needs to see these functions, but the implementation of these functions depends on the generated code.
void system_init();
void pointer_track(void * pointer);
void pointer_done(void * pointer);

struct FunctionStackEntry
{
	const char * name;
	const char * fileName;
	int lineNumber;
};
EXTERN_C struct FunctionStackEntry * function_stack;
EXTERN_C int function_stack_size;
void function_stack_push(const char * name, const char * fileName);
void function_stack_pop();
void function_stack_setLineNumber(int lineNumber);

// A nice wrapper for setting the current VNS source code line being executed before calling a function.
// For now, this simplifies how the generated code looks. Without this, each function call would need to be
// separated into a new line, before which function_stack_setLineNumber() would be used.
// Its best explain by the following example:
// 7 > value:= getCost() +
// 8 >			getOtherCost();
// Before each function call we should set a line number to that function stack always nice information.
#define VNS_CALL(function, line) (function_stack_setLineNumber(line), function)

struct TypeInfo
{
	const char * stringName;
	int size;
};

struct Array
{
	struct TypeInfo * typeInfo;
	char * pointer;
	int size;
};

static int ArrayTypeInfoSize(struct Array * array)
{
	return array->typeInfo->size;
}

static char* arrayGet(struct Array * array, int index)
{
	if (index >= array->size) {
		runtime_crash();
		return NULL;
	}
	else {
		return array->pointer + (index * ArrayTypeInfoSize(array));
	}
}

static void arraySet(struct Array * array, int index, void * value)
{
	if (index >= array->size) {
		runtime_crash();
	}
	else {
		memcpy(array->pointer + (index * ArrayTypeInfoSize(array)), value, ArrayTypeInfoSize(array));
	}
}

static int arrayGetSize(struct Array * array)
{
	return array->size;
}

static void arrayNew(struct Array * array, int size)
{
	if (size == 0) {
		runtime_crash();
	}
	if (array->pointer) {
		free(array->pointer);
	}
	array->pointer = (char*)malloc(ArrayTypeInfoSize(array) * size);
	memset(array->pointer, 0, ArrayTypeInfoSize(array) * size);
	array->size = size;
}

static void arrayReallocate(struct Array * array)
{
	if (array->pointer == NULL) {
		return;
	}
	int memSize = ArrayTypeInfoSize(array) * array->size;
	char * newPointer = (char*)malloc(memSize);
	memcpy(newPointer, array->pointer, memSize);
	array->pointer = newPointer;
}

static void arrayFree(struct Array * array)
{
	if (array->size == 0) {
		// When array's size is 0, the pointer should be null!
		if (array->pointer != NULL) {
			runtime_crash();
		}
	}
	else {
		if (array->pointer == NULL) {
			runtime_crash();
		}
		free(array->pointer);
		array->pointer = NULL;
		array->size = 0;
	}
}

#define initialize_string_array(index, string, string_size) \
	global_strings[index].typeInfo = &TypeInfo_char; \
	global_strings[index].pointer = string; \
	global_strings[index].size = string_size
)V0G0N"; /* End of multi-line string */
  }

  // We use some of vns compiled code as library code, sort of
  static const char* append_at_end() {
    return R"V0G0N(
// static struct Map pointer_map;
struct FunctionStackEntry * function_stack = NULL;
int function_stack_size = 0;
static void system_init()
{
	// Map_Constructor(&map);
	const int stackSize = sizeof(struct FunctionStackEntry) * 1024 * 1024;
	function_stack = malloc(stackSize);
	memset(function_stack, 1, stackSize);
}
static void pointer_track(void * pointer)
{

}
static void pointer_done(void * pointer)
{

}
static void function_stack_push(const char * name, const char * fileName)
{
	function_stack[function_stack_size].name = name;
	function_stack[function_stack_size].fileName = fileName;
	function_stack_size += 1;
}
static void function_stack_pop()
{
	function_stack_size -= 1;
}
static void function_stack_setLineNumber(int lineNumber)
{
	if (function_stack_size > 0) {
		function_stack[function_stack_size - 1].lineNumber = lineNumber;
	}
}

static struct Array VNS_getStacktrace()
{
	struct Array stacktrace = {0};
	stacktrace.typeInfo = &TypeInfo_VNS_StacktraceFrame;
	arrayNew(&stacktrace, function_stack_size);
	for (int i = 0; i < function_stack_size; i++) {
		struct FunctionStackEntry * entry = &function_stack[i];

		struct VNS_StacktraceFrame frame = {0};

		int nameSize = (int)strlen(entry->name);
		if (nameSize > 0) {
			struct Array name = {0};
			name.typeInfo = &TypeInfo_char;
			arrayNew(&name, nameSize);
			memcpy(name.pointer, entry->name, nameSize);
			frame.name = name;
		}

		int fileNameSize = (int)strlen(entry->fileName);
		if (fileNameSize > 0) {
			struct Array fileName = {0};
			fileName.typeInfo = &TypeInfo_char;
			arrayNew(&fileName, fileNameSize);
			memcpy(fileName.pointer, entry->fileName, fileNameSize);
			frame.fileName = fileName;
		}

		frame.lineNumber = entry->lineNumber;

		arraySet(&stacktrace, i, &frame);
	}
	return stacktrace;
}

)V0G0N";
  }
};
