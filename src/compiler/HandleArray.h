#pragma once

#include <compiler/Error.h>
#include <vector>

template <typename T>
class HandleArray;

template <typename T>
class GenericHandle {
  friend HandleArray<T>;
  GenericHandle(int index) : index(index) {}
  int index = 0;
};

template <typename T>
class HandleArray {
 public:
  GenericHandle<T> insertObject(const T& object) {
    int index = mIndex;
    mIndex += 1;
    mObjects.push_back(object);
    return GenericHandle<T>{index};
  }

  template <typename FUNCTION, typename ERROR>
  auto forObject(const GenericHandle<T>& handle,
                 const FUNCTION& function,
                 const ERROR& error) {
    int index = handle.index - 1;
    if (index < 0 || index >= mObjects.size()) {
      return error();
    }
    return function(mObjects.at(index));
  }

 private:
  int mIndex = 1;
  std::vector<T> mObjects;
};
