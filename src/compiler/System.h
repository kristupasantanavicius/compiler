#pragma once

#include <fstream>
#include <string>

inline bool readFile(const std::string& path, std::string& out) {
  std::fstream file;
  file.open(path);
  if (!file.is_open()) {
    return false;
  }

  std::string sourceData((std::istreambuf_iterator<char>(file)),
                         (std::istreambuf_iterator<char>()));
  out = sourceData;
  return true;
}
