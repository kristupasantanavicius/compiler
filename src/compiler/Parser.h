#pragma once

#include <compiler/AST.h>
#include <compiler/Lexer.h>
#include <compiler/Optional.h>

class Lexer;

struct ParserException : public std::runtime_error {
  astError compilerError;
  explicit ParserException(const astError& error)
      : std::runtime_error(error.message), compilerError(error) {}
};

class Parser {
 public:
  Parser(Lexer* lexer) : mLexer(lexer) {}

  bool parse(parsed::Scope& scope, astError* error);
  const std::vector<std::string> getCommandsLoadFile() {
    return mCommandsLoadFile;
  }

  enum class Directive {
    None,
    LoadFile,
    Constructor,
    CopyConstructor,
    Destructor,
    Char,
  };

 private:
  struct ParserState {
    int scanner_position;
    Token token;
  };

  void consume(TokenType type, std::string msg = "");
  void consumeUnchecked();
  bool tryConsume(TokenType type);
  Token peekToken(int howMany = 1) { return LexerPeekToken(mLexer, howMany); }

  void makeError(const std::string& expected,
                 astLocation* actualLocation = nullptr);

  void doParsing(parsed::Scope& parsedScope);

  bool parseScope(parsed::Scope& parsedScope);
  bool parseScopeImpl(parsed::Scope& parsedScope);

  bool parseCommand();

  std::string getDirectiveString(Directive directive);
  Directive parseDirective();

  bool parseClassDirective(parsed::Class& aclass);
  bool parseClass(parsed::Scope& parsedScope,
                  std::string* identifier = nullptr,
                  astLocation location = astLocation());
  void parseFunctionWithoutIdentifier(parsed::Function& function);
  bool parseFunction(parsed::Scope& parsedScope,
                     std::string* identifier = nullptr,
                     astLocation location = astLocation());
  bool parseScopeVariable(parsed::Scope& parsedScope);

  bool parseFunctionParameter(parsed::Function& parsedFunction);
  void parseStatements(parsed::Scope& scope, parsed::Block& block);

  Optional<parsed::Statement> parseStatement(parsed::Block& block);

  Optional<parsed::StatementIf> parseIfStatement();
  Optional<parsed::StatementWhile> parseWhileStatement();
  Optional<parsed::StatementReturn> parseReturnStatement();
  Optional<parsed::StatementBreak> parseBreakStatement();
  Optional<parsed::StatementContinue> parseContinueStatement();

  Optional<parsed::VariableDecl> parseVariableDecl();
  Optional<parsed::VariableAssign> parseVariableAssign();
  Optional<parsed::Symbol> parseVariableOrFunctionCall();

  Optional<parsed::Variable> parseVariable(bool typeOptional = false);

  Optional<parsed::Type> parseTypeCast();
  void parseTypeCast(parsed::ExpressionNode& node);
  Optional<parsed::Expression> parseExpression();
  Optional<parsed::Literal> parseStringConstant();
  Optional<parsed::Literal> parseIntegerLiteral();
  Optional<parsed::Symbol> parseSymbol();

  Optional<parsed::Type> parseType(bool with_errors = true);

  bool parseOperator(astOperator& op, astLocation& opLocation);

  bool parseTypeAlias(parsed::Scope& parsedScope);

  bool testTokenType(TokenType type) { return mToken.type == type; }
  std::string getTokenString();
  int getTokenLineNumber();
  astLocation getTokenLocation();

  ParserState SaveState();
  void RestoreState(const ParserState& state);

 private:
  Lexer* mLexer;
  Token mToken;
  std::vector<std::string> mCommandsLoadFile;
};
