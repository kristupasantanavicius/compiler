#include <iostream>

// https://stackoverflow.com/questions/8487986/file-macro-shows-full-path
#define __FILENAME__ \
  (strrchr(__FILE__, '\\') ? strrchr(__FILE__, '\\') + 1 : __FILE__)

#define ERROR 0
#define INFO 1
#define DEBUG 2
#define TODO_LEVEL 3

#define LOG(level) (LogWriter(level) << __FILENAME__ << "(" << __LINE__ << "): ")

#define TODO() LOG(TODO_LEVEL) << "TODO - "

#define NOTREACHED() LOG(ERROR) << "Code is not supposed to be reached"

class LogWriter {
  int level_;

 public:
  LogWriter(int level) : level_(level) {}

  ~LogWriter() { std::cout << std::endl; }

  constexpr bool IsEnabled() const { return level_ <= INFO; }
};

inline LogWriter&& operator<<(LogWriter&& writer, const char* value) {
  std::cout << value;
  return std::move(writer);
}

inline LogWriter&& operator<<(LogWriter&& writer, const std::string& value) {
  std::cout << value;
  return std::move(writer);
}

inline LogWriter&& operator<<(LogWriter&& writer, int value) {
  std::cout << value;
  return std::move(writer);
}

inline LogWriter&& operator<<(LogWriter&& writer, std::size_t value) {
  std::cout << value;
  return std::move(writer);
}
