#include <compiler/AST.h>
#include <compiler/Error.h>
#include <compiler/ExpressionVariable.h>
#include <compiler/Format.h>
#include <compiler/Stack.h>
#include <compiler/TypeCheck.h>
#include <compiler/astDump.h>
#include <compiler/astGenerator.h>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <memory>
#include <unordered_map>
#include <unordered_set>

std::string make_string(const astLocation& location) {
  return make_string(location.line) + ":" + make_string(location.charNumber);
}

// Need a local namespace, otherwise we might get global namespace collisions,
// like State (Also Cgenerator::State), etc.. That causes weird runtime crashes
// (memory corruptions) when allocating State when 2 symbols of same name are in
// global namespace
namespace {

struct TypeCheckException : public std::runtime_error {
  astError compilerError;
  explicit TypeCheckException(const astError& error)
      : std::runtime_error(error.message), compilerError(error) {}
};

struct State;

void resolveExpression(astExpression* expression, State* state);

void resolveFunctionCallArguments(std::vector<astExpression*>& arguments,
                                  State* state) {
  for (auto& argument : arguments) {
    resolveExpression(argument, state);
  }
}

class TypeCheckVisitor;

struct FunctionScope {
  astFunction* function = nullptr;
  std::unordered_set<astVariable*> declaredVariables;
};

struct BlockInfo {
  int numberOfChildBlocks = 0;
  int childBlocksWhichReturned = 0;
  bool blockHasReturn = false;
  bool allPathsHaveReturnStatements() {
    return childBlocksWhichReturned > 0 &&
           childBlocksWhichReturned >= numberOfChildBlocks;
  }
};

struct State {
  // Moved way down the file, because c++ (Visitor can only be instantiated
  // later on, after its defined)
  State(SyntaxTree* ast, ASTCache& cache);

  SyntaxTree* ast = nullptr;
  ASTCache& cache;
  FunctionScope* fscope = nullptr;
  BlockInfo* blockInfo = nullptr;
  std::unique_ptr<TypeCheckVisitor> visitor;
  Stack<astClass*> classStack;

  Stack<astLocation> templateInstantiationStack;

  Stack<std::vector<astLocation>*> templateLocationStack;

  std::vector<astType*> typeQueue;

  void pushFunctionScope(astFunction* function) {
    FunctionScope scope;
    scope.function = function;
    scopeStack.push_back(scope);
    refreshScope();
  }
  void popFunctionScope() {
    ASSERT(!scopeStack.empty());
    scopeStack.pop_back();
    refreshScope();
  }

  void pushBlockInfo() {
    blocks.push_back(BlockInfo());
    refreshBlockInfo();
  }
  void popBlockInfo() {
    ASSERT(!blocks.empty());
    blocks.pop_back();
    refreshBlockInfo();
  }

  void reportError(astLocation location, const std::string& msg) {
    astError error(location, msg);
    if (!templateLocationStack.empty()) {
      error.stackOfLocations = *templateLocationStack.getCurrent();
    }
    throw TypeCheckException(error);
  }

 private:
  void refreshScope() {
    if (scopeStack.empty()) {
      fscope = nullptr;
    } else {
      fscope = &scopeStack.back();
    }
  }
  void refreshBlockInfo() {
    if (blocks.empty()) {
      blockInfo = nullptr;
    } else {
      blockInfo = &blocks.back();
    }
  }

  std::vector<FunctionScope> scopeStack;
  std::vector<BlockInfo> blocks;
};

std::string placifyNumber(int number) {
  std::string base = std::to_string(number);
  if (number == 1)
    base += "st";
  else if (number == 2)
    base += "nd";
  else if (number == 3)
    base += "rd";
  else
    base += "th";
  return base;
}

void checkFunctionCall2ArgumentsMatchWithFunctionSignature(
    astFunctionCall2* call,
    State* state) {
  astFunction* function = call->function;
  if (call->arguments.size() != function->parameters.size()) {
    state->reportError(
        call->location,
        FORMAT("function '@' wants @ arguments, but it was given @",
               function->name, function->parameters.size(),
               call->arguments.size()));
  }

  for (int i = 0; i < (int)call->arguments.size(); i++) {
    astType* argType = call->arguments.at(i)->flatType;
    astType* paramType = call->function->parameters.at(i)->type;
    const std::string& paramName = call->function->parameters.at(i)->name;
    if (!astTypesMatchSkipReferences(argType, paramType)) {
      state->reportError(
          call->arguments.at(i)->location,
          FORMAT("@ argument ('@') is supposed to be '@', but was given '@'",
                 placifyNumber(i + 1), paramName, astTypeToString(paramType),
                 astTypeToString(argType)));
    }
  }
}

void resolveType(astType* type, State* state);

astVariable* createVariable(astType* type, const std::string& name) {
  astVariable* variable = new astVariable();
  variable->name = name;
  variable->type = type;
  return variable;
}

void resolveClass(astClass* aclass, State* state);
void resolveClassConstructor(astClass* aclass, State* state);
void resolveClassCopyConstructor(astClass* aclass, State* state);

void addParameterToFunction(astFunction* function, astVariable* variable) {
  function->parameters.push_back(variable);
  function->scope->variables.push_back(variable);
}

astFunction* generateConstructor(astClass* memberOf,
                                 astType* returnType,
                                 astLocation location);
astFunction* generateClassCopyConstructor(astClass* aclass);
astFunction* generateClassDestructor(astClass* aclass);

// Return type as a C identifier (Array_Array_int instead of []_[]_int)
std::string typeToIdentifierString(astType* type) {
  std::string name = type->string;
  if (astClass* aclass = type->getClass()) {
    name = aclass->name;
    for (auto& subtype : type->getSubtypes()) {
      name += "_";
      name += typeToIdentifierString(subtype);
    }
  }
  return name;
}

void arrayInsertDefaultReturn(astFunction* function) {
  function->block->statements.push_back(
      astNewDefaultReturn(astLocation(), function->scope));
}

astClass* getCachedArrayClass(astType* subtype,
                              std::vector<astClass*>& arrayCache) {
  ASSERT(subtype->name != astTypeName::TypeAlias);

  std::string arrayClassName = "Array_" + typeToIdentifierString(subtype);

  for (auto& aclass : arrayCache) {
    if (aclass->name == arrayClassName) {
      return aclass;
    }
  }

  astClass* arrayClass = new astClass(arrayClassName, nullptr);

  {
    astFunction* constructor = generateConstructor(
        arrayClass,
        astWrapTypeInArray(subtype->COPY(), arrayClass, astLocation::Array()),
        astLocation::Array());
    astStatementNative* array_new = new astStatementNative();
    array_new->initArrayNew(constructor->returnType, false);
    constructor->block->statements.push_back(array_new);
    arrayClass->default_constructor = constructor;
  }

  {
    astFunction* constructor = generateConstructor(
        arrayClass,
        astWrapTypeInArray(subtype->COPY(), arrayClass, astLocation::Array()),
        astLocation::Array());
    addParameterToFunction(
        constructor,
        createVariable(
            astNewBasicType(astTypeName::Int32, astLocation::Array()), "size"));
    constructor->overloadTag = 1;
    astStatementNative* array_new = new astStatementNative();
    array_new->initArrayNew(constructor->returnType, true);
    constructor->block->statements.push_back(array_new);
  }

  {
    astFunction* copyConstructor = generateClassCopyConstructor(arrayClass);
    astStatementNative* statement = new astStatementNative();
    statement->initArrayCopyConstructor(subtype);
    copyConstructor->block->statements.push_back(statement);
    arrayInsertDefaultReturn(copyConstructor);
  }

  {
    astFunction* destructor = generateClassDestructor(arrayClass);
    astStatementNative* arrayNew = new astStatementNative();
    arrayNew->initArrayDestructor(subtype);
    destructor->block->statements.push_back(arrayNew);
    arrayInsertDefaultReturn(destructor);
  }

  {
    astFunction* function = new astFunction("get", arrayClass->scope);
    addParameterToFunction(
        function, createVariable(
                      astNewBasicType(astTypeName::Int32, astLocation::Array()),
                      "index"));
    // function->isExternFunction = true;
    function->arrayFunction = true;
    function->nativeName = astNativeFunctionName::ArrayGet;
    function->memberOf = arrayClass;
    function->returnType = subtype;

    astStatementNative* statement = new astStatementNative();
    statement->initArrayGet(subtype);
    function->block->statements.push_back(statement);

    arrayClass->scope->functions.push_back(function);
  }

  {
    astFunction* function = new astFunction("set", arrayClass->scope);
    addParameterToFunction(
        function, createVariable(
                      astNewBasicType(astTypeName::Int32, astLocation::Array()),
                      "index"));
    addParameterToFunction(function, createVariable(subtype, "value"));
    // function->isExternFunction = true;
    function->arrayFunction = true;
    function->nativeName = astNativeFunctionName::ArraySet;
    function->memberOf = arrayClass;
    function->returnType =
        astNewBasicType(astTypeName::Void, astLocation::Array());

    astStatementNative* statement = new astStatementNative();
    statement->initArraySet(subtype);
    function->block->statements.push_back(statement);

    arrayClass->scope->functions.push_back(function);
  }

  {
    astFunction* function = new astFunction("length", arrayClass->scope);
    // function->isExternFunction = true;
    function->arrayFunction = true;
    function->nativeName = astNativeFunctionName::ArraySize;
    function->memberOf = arrayClass;
    function->returnType =
        astNewBasicType(astTypeName::Int32, astLocation::Array());

    astStatementNative* statement = new astStatementNative();
    statement->initArraySize();
    function->block->statements.push_back(statement);

    arrayClass->scope->functions.push_back(function);
  }

  arrayCache.push_back(arrayClass);

  return arrayClass;
}

void checkFunction(astScope* parentScope, astFunction* function, State* state);

#if 0
void collapseTypeAlias(astType * type);
#endif
void instantiateArrayClass(astType* type, std::vector<astClass*>& arrayCache);

astClass* instantiateClassTemplate(parsed::Class& templateClass,
                                   astType* argumentType,
                                   astScope* scope,
                                   State* state) {
  if (templateClass.templateParameters.size() !=
      argumentType->getSubtypes().size()) {
    state->reportError(argumentType->location,
                       "TEMPLATE ARGUMENTS COUNT DOES NOT MATCH");
  }

  {
    auto iter = std::find_if(
        scope->instantiatedTemplatedClasses.begin(),
        scope->instantiatedTemplatedClasses.end(),
        [templateClass, argumentType](astClass* aclass) {
          // @TODO Store instantiated classes in a table based on their name.
          // Right now this look-up could be very slow.
          if (templateClass.name != aclass->name) {
            return false;
          }

          ASSERT(!aclass->templateArguments.empty());
          ASSERT(aclass->templateArguments.size() ==
                 argumentType->getSubtypes().size());
          for (int i = 0; i < (int)aclass->templateArguments.size(); i++) {
            if (!astTypesMatch(aclass->templateArguments.at(i),
                               argumentType->getSubtypes().at(i))) {
              return false;
            }
          }
          return true;
        });
    if (iter != scope->instantiatedTemplatedClasses.end()) {
      return *iter;
    }
  }

  std::vector<astType*> typesToResolve;
  astClass* aclass = astGenerateClass(
      templateClass, scope, state->cache.globalStrings, typesToResolve,
      state->ast->allClasses, state->cache.types_without_error);
  for (int i = 0; i < (int)templateClass.templateParameters.size(); i++) {
    astTypeAlias* alias = new astTypeAlias();
    alias->location = argumentType->location;
    alias->name = templateClass.templateParameters.at(i);
    alias->type = argumentType->getSubtypes().at(i);
    aclass->scope->typeAliases.push_back(alias);
    aclass->templateArguments.push_back(argumentType->getSubtypes().at(i));
  }

  // @TODO do we need to insert here?
  // state->typeQueue.insert(state->typeQueue.end(), typesToResolve.begin(),
  // typesToResolve.end());

  state->templateInstantiationStack.push(argumentType->location);

  // This has to be inserted before resolving the types. The class might be
  // referencing itself, which would create infinite cycle of class creation:
  // List = struct $T {
  //   Foo = func() { bar = List<T>(); };
  // }
  scope->instantiatedTemplatedClasses.push_back(aclass);

  for (auto& type : typesToResolve) {
    resolveType(type, state);
  }
#if 0
	for (auto & type : typesToResolve) {
		collapseTypeAlias(type);
	}
#endif
  for (auto& type : typesToResolve) {
    instantiateArrayClass(type, state->cache.arrayClasses);
  }

  aclass->templateInstantiationStack =
      state->templateInstantiationStack.asVector();
  state->templateInstantiationStack.pop();

  return aclass;
}

astType* findTemplateArgumentInType(const parsed::Type& source,
                                    astType* type,
                                    const std::string& templateParameter) {
  // Ow fk,.. Not very pretty. We need to skip references in the 'source', but
  // the parsed::Type doesn't yet have a concept of what a reference is, it just
  // has a string representation of it.
  if (source.name == "&") {
    return findTemplateArgumentInType(source.subtypes.at(0), type,
                                      templateParameter);
  }

  if (source.name == templateParameter) {
    return type;
  } else {
    int index = 0;
    for (auto& subSource : source.subtypes) {
      astType* subType = type->getSubtypes().at(index);
      if (astType* result = findTemplateArgumentInType(subSource, subType,
                                                       templateParameter)) {
        return result;
      }
      index += 1;
    }
    return nullptr;
  }
}

astType* findFirstFunctionParameterWhichUsesTemplateParameter(
    parsed::Function& templateFunction,
    const std::vector<astExpression*> callArguments,
    const std::string& templateParameter) {
  ASSERT(templateFunction.parameters.size() == callArguments.size());
  for (int i = 0; i < (int)callArguments.size(); i++) {
    astType* found = findTemplateArgumentInType(
        templateFunction.parameters.at(i).type.getValue(),
        callArguments.at(i)->flatType, templateParameter);
    if (found) {
      return found;
    }
  }
  return nullptr;
}

astFunction* instantiateFunctionTemplate(
    parsed::Function& templateFunction,
    const std::vector<astExpression*> callArguments,
    std::vector<astType*> givenArguments,
    astLocation callerLocation,
    astScope* scope,
    State* state) {
  if (templateFunction.parameters.size() != callArguments.size()) {
    state->reportError(
        callerLocation,
        "internal error: number of function gparameters does not match number "
        "of call arguments when instantiating function template");
  }

  std::vector<astType*> templateArguments = givenArguments;
  for (int i = (int)givenArguments.size();
       i < (int)templateFunction.templateParameters.size(); i++) {
#if 0
    const std::string& templateParameter =
        templateFunction.templateParameters.at(i);
    int firstIndex = findFirstFunctionParameterWhichUsesTemplateParameter(
        templateFunction.parameters, templateParameter);
    if (firstIndex >= 0) {
      templateArguments.push_back(callArguments.at(firstIndex)->flatType);
    }
#endif
    const std::string& templateParameter =
        templateFunction.templateParameters.at(i);
    astType* foundType = findFirstFunctionParameterWhichUsesTemplateParameter(
        templateFunction, callArguments, templateParameter);
    if (foundType) {
      templateArguments.push_back(foundType);
    }
  }

  if (templateFunction.templateParameters.size() != templateArguments.size()) {
    state->reportError(
        callerLocation,
        FORMAT("function '@' wants @ template arguments, but was given @",
               templateFunction.name,
               templateFunction.templateParameters.size(),
               templateArguments.size()));
  }

  {
    auto iter = std::find_if(
        scope->instantiatedTemplatedFunctions.begin(),
        scope->instantiatedTemplatedFunctions.end(),
        [templateFunction, templateArguments](astFunction* function) {
          ASSERT(!function->templateArguments.empty());
          if (templateFunction.name != function->name) {
            return false;
          }
          if (function->templateArguments.size() != templateArguments.size()) {
            return false;
          }
          for (int i = 0; i < (int)function->templateArguments.size(); i++) {
            if (!astTypesMatch(function->templateArguments.at(i),
                               templateArguments.at(i))) {
              return false;
            }
          }
          return true;
        });
    if (iter != scope->instantiatedTemplatedFunctions.end()) {
      return *iter;
    }
  }

  TODO();
  astClass* memberOf = nullptr;

  std::vector<astType*> typesToResolve;
  astFunction* function = astGenerateFunction(
      templateFunction, scope, memberOf, state->cache.globalStrings,
      typesToResolve, state->ast->allClasses, state->cache.types_without_error);
  for (int i = 0; i < (int)templateFunction.templateParameters.size(); i++) {
    astTypeAlias* alias = new astTypeAlias();
    alias->location = templateArguments.at(i)->location;
    alias->name = templateFunction.templateParameters.at(i);
    alias->type = templateArguments.at(i);
    function->scope->typeAliases.push_back(alias);
    function->templateArguments.push_back(templateArguments.at(i));
  }
  // @TODO do we need to insert here?
  // state->typeQueue.insert(state->typeQueue.end(), typesToResolve.begin(),
  // typesToResolve.end());
  for (auto& type : typesToResolve) {
    resolveType(type, state);
  }
#if 0
	for (auto & type : typesToResolve) {
		collapseTypeAlias(type);
	}
#endif
  for (auto& type : typesToResolve) {
    instantiateArrayClass(type, state->cache.arrayClasses);
  }
  checkFunction(scope, function, state);
  scope->instantiatedTemplatedFunctions.push_back(function);
  return function;
}

void resolveType(astType* type, State* state) {
  if (type->name != astTypeName::Unresolved) {
    // Already resolved, skip
    return;
  }

  bool with_error = state->cache.types_without_error.find(type) ==
                    state->cache.types_without_error.end();

  if (type->string == "[]") {
    type->name = astTypeName::Array;
    ASSERT(type->getSubtypes().size() == 1);
    astType* subtype = type->getSubtypes().at(0);
    resolveType(subtype, state);
    return;
  } else if (type->string == "bool") {
    type->name = astTypeName::Bool;
  } else if (type->string == "i32") {
    type->name = astTypeName::Int32;
  } else if (type->string == "i8") {
    type->name = astTypeName::Int8;
  } else if (type->string == "f32") {
    type->name = astTypeName::Float32;
  } else if (type->string == "char") {
    type->name = astTypeName::Char;
  } else if (type->string == "void") {
    type->name = astTypeName::Void;
  } else if (type->string == "*") {
    type->name = astTypeName::Pointer;
    ASSERT(type->getSubtypes().size() == 1);
    resolveType(type->getSubtypes().at(0), state);
  } else if (type->string == "&") {
    type->name = astTypeName::Reference;
    ASSERT(type->getSubtypes().size() == 1);
    resolveType(type->getSubtypes().at(0), state);
    if (type->getSubtypes().at(0)->name == astTypeName::Reference) {
      state->reportError(type->getSubtypes().at(0)->location,
                         "&& (reference to reference) is not allowed");
    }
  } else if (type->literal) {
    type->name = astTypeName::Literal;
  } else {
    bool typeResolved = false;
    astTypeAlias* alias = astFindTypeAlias(type->scope, type->string);
    if (alias) {
      // We want to keep the old location, because otherwise error messages
      // which include type aliases don't make any sense.
      // The location of the type is the location in which the type aliases is
      // created, not the location of the type being references by type alias.
      astLocation locationOfAlias = type->location;

      type->name = astTypeName::TypeAlias;
      type->addSubtype(alias->type, true);
      if (alias->type->name == astTypeName::Unresolved) {
        resolveType(alias->type, state);
      }
      *type = *type->getSubtypes().at(0);
      type->location = locationOfAlias;
      typeResolved = true;
    } else {
      astClass* aclass = astFindClassInScope(type->scope, type->string);
      if (aclass) {
        type->aclass = aclass;
        type->name = astTypeName::Class;
        typeResolved = true;
      } else {
        astScope* scopeOfTemplatedClass = nullptr;
        parsed::Class* templateClass = astFindTemplatedClassInScope(
            type->scope, type->string, &scopeOfTemplatedClass);
        if (templateClass) {
          // SUBTYPES HAVE TO BE RESOLVED BEFORE INSTANTIATING THE TEMPLATED
          // CLASS!
          for (auto& templateArgument : type->getSubtypes()) {
            resolveType(templateArgument, state);
          }
          astClass* newClass = instantiateClassTemplate(
              *templateClass, type, scopeOfTemplatedClass, state);
          if (newClass->name != templateClass->name) {
            LOG(ERROR) << "Cached instantiated template class has returned a "
                          "class instance which doesn't match by name";
            ASSERT(false);
          }
          type->aclass = newClass;
          type->name = astTypeName::Class;
          typeResolved = true;
        }
      }
    }

    if (!typeResolved) {
      if (with_error) {
        state->reportError(type->location, FORMAT("unrecognized type '@'",
                                                  astTypeToString(type)));
      } else {
        type->type_error_supressed = true;
        return;
      }
    }
  }

  ASSERT(type->name != astTypeName::Unresolved);
}

void resolveTypes(const std::vector<astType*>& types, State* state) {
  for (auto& type : types) {
    resolveType(type, state);
  }
}

void checkDuplicateClasses(astScope* scope, State* state) {
  std::unordered_map<std::string, astClass*> existingClasses;
  for (auto& classNode : scope->classes) {
    auto iter = existingClasses.find(classNode->name);
    if (iter != existingClasses.end()) {
      state->reportError(classNode->location,
                         FORMAT("class '@' already defined on line @ ",
                                classNode->name, iter->second->location));
    }
    existingClasses.emplace(classNode->name, classNode);
  }
}

bool functionsCanBeOverloaded(astFunction* function,
                              astFunction* otherFunction) {
  // overloading is possible in 2 cases:
  // 1) number of parameters don't match
  // 2) type of one of the parameters doesn't match
  if (function->parameters.size() != otherFunction->parameters.size()) {
    return true;
  } else {
    for (int i = 0; i < (int)function->parameters.size(); i++) {
      if (!astTypesMatchSkipReferences(function->parameters.at(i)->type,
                                       otherFunction->parameters.at(i)->type)) {
        return true;
      }
    }
  }
  return false;
}

void checkDuplicateFunctionsInScope(astScope* inScope, State* state) {
  astOverloadTag nextOverloadTag = 1;

  std::unordered_map<std::string, astFunction*> existingFunctions;
  for (auto& function : inScope->functions) {
    auto iter = existingFunctions.find(function->name);
    if (iter != existingFunctions.end()) {
      astFunction* otherFunction = iter->second;
      if (functionsCanBeOverloaded(function, otherFunction)) {
        ASSERT(function->overloadTag == 0);
        function->overloadTag = nextOverloadTag;
        nextOverloadTag += 1;
      } else {
        state->reportError(
            function->location,
            FORMAT("function '@(TODO:Params)' already defined on line @",
                   function->name, iter->second->location));
      }
    }
    existingFunctions.emplace(function->name, function);
  }
}

void checkExternFunctionIsInGlobalScope(astScope* scope,
                                        astFunction* inFunction,
                                        State* state) {
  if (inFunction->isExternFunction && scope->owner.module == "") {
    state->reportError(inFunction->location,
                       FORMAT("function '@' cannot be extern, because it is "
                              "not inside global scope",
                              inFunction->name));
  }
}

void checkAPIFunctionIsInGlobalScope(astScope* scope,
                                     astFunction* inFunction,
                                     State* state) {
  if (inFunction->isAPIFunction && scope->owner.module == "") {
    state->reportError(
        inFunction->location,
        FORMAT(
            "function '@' cannot be API, because it is not inside global scope",
            inFunction->name));
  }
}

void checkDuplicateVariablesInScope(astScope* inScope, State* state) {
  std::unordered_map<std::string, astVariable*> existingVariables;
  for (auto& var : inScope->variables) {
    auto iter = existingVariables.find(var->name);
    if (iter != existingVariables.end()) {
      state->reportError(var->location,
                         FORMAT("variable '@' already defined on line @",
                                var->name, iter->second->location));
    }
    existingVariables.emplace(var->name, var);
  }
}

void checkDuplicateTypeAliasesInScope(astScope* inScope, State* state) {
  std::unordered_map<std::string, astTypeAlias*> existingAliases;
  for (auto& alias : inScope->typeAliases) {
    auto iter = existingAliases.find(alias->name);
    if (iter != existingAliases.end()) {
      state->reportError(alias->location,
                         FORMAT("typealias '@' already defined on line @",
                                alias->name, iter->second->location));
    }
    existingAliases.emplace(alias->name, alias);
  }
}

void checkClassDoesNotContainItselfAsMemberVariable(astClass* aclass,
                                                    State* state) {
  for (auto& variable : aclass->scope->variables) {
    if (variable->type->name == astTypeName::Class) {
      ASSERT(variable->type->aclass);
      if (variable->type->aclass == aclass) {
        state->reportError(
            variable->location,
            FORMAT("member variable '@' causes recursion of class '@'",
                   variable->name, aclass->name));
      }
    }
  }
}

bool typeDoesNotHaveDefaultConstructor(astType* type) {
  if (type->getClass()) {
    if (!type->getClass()->default_constructor) {
      return true;
    }
  }
  return false;
}

astFunction* generateConstructor(astClass* aclass,
                                 astType* returnType,
                                 astLocation location) {
  astFunction* dst = new astFunction("constructor", aclass->scope);
  dst->location = location;
  dst->returnType = returnType;

  dst->constructorFunction = true;
  aclass->scope->functions.push_back(dst);
  aclass->instance_constructor = dst;

  return dst;
}

void checkConstructorReturnType(astFunction* constructor, State* state) {
  if (!constructor->constructorFunction &&
      constructor->returnType->name != astTypeName::Void) {
    state->reportError(constructor->returnType->location,
                       FORMAT("return type of constructor must be void"));
  }
}

void generateConstructorWithParameters(astClass* aclass) {
  astFunction* constructor = generateConstructor(
      aclass, astWrapClassInType(aclass, aclass->location), aclass->location);
  for (auto& member : aclass->scope->variables) {
    astVariable* parameter = new astVariable();
    parameter->location = member->location;
    parameter->name = member->name;
    parameter->type =
        astNewType(nullptr, member->location, "", member->type->scope);
    parameter->type->name = astTypeName::Reference;
    parameter->type->addSubtype(member->type->COPY(), true);
    addParameterToFunction(constructor, parameter);
  }
}

void resolveClassConstructor(astClass* aclass, State* state) {
  generateConstructorWithParameters(aclass);

  std::vector<astFunction*> constructors =
      astFindAllFunctionsInScope(aclass->scope, "constructor", false);

  for (auto& constructor : constructors) {
    checkConstructorReturnType(constructor, state);
    constructor->returnType = astWrapClassInType(aclass, aclass->location);
    if (constructor->parameters.empty()) {
      aclass->default_constructor = constructor;
    }
    constructor->scope->flag_member = false;
  }
}

astFunction* generateClassCopyConstructor(astClass* aclass) {
  astFunction* copyConstructor =
      new astFunction("copyConstructor", aclass->scope);
  copyConstructor->memberOf = aclass;
  copyConstructor->returnType =
      astNewBasicType(astTypeName::Void, aclass->location);
  aclass->scope->functions.push_back(copyConstructor);
  aclass->copyConstructor = copyConstructor;
  return copyConstructor;
}

void resolveClassCopyConstructor(astClass* aclass, State* state) {
  std::vector<astFunction*> copyConstructors =
      astFindAllFunctionsInScope(aclass->scope, "copyConstructor", false);

  if (copyConstructors.size() >= 2) {
    state->reportError(
        aclass->location,
        FORMAT("a class may have only 1 copyConstructor (class '@' has @)",
               aclass->name, (int)copyConstructors.size()));
    return;
  }

  astFunction* copyConstructor = nullptr;

  if (!copyConstructors.empty()) {
    copyConstructor = copyConstructors.at(0);
  }

  if (copyConstructor) {
    aclass->copyConstructor = copyConstructor;
  } else {
    copyConstructor = generateClassCopyConstructor(aclass);
  }
  if (!aclass->copyConstructor->parameters.empty()) {
    state->reportError(
        copyConstructor->location,
        FORMAT("a copyConstructor should not have any parameters (class '@')",
               aclass->name));
  }
  if (copyConstructor->returnType->name != astTypeName::Void) {
    state->reportError(
        copyConstructor->returnType->location,
        FORMAT("return type of copyConstructor must be void for class '@'",
               aclass->name));
  }
}

astFunction* generateClassDestructor(astClass* aclass) {
  ASSERT(aclass);
  ASSERT(aclass->destructor == nullptr);
  astFunction* destructor = new astFunction("destructor", aclass->scope);
  destructor->memberOf = aclass;
  destructor->returnType = astNewBasicType(astTypeName::Void, aclass->location);
  aclass->scope->functions.push_back(destructor);
  aclass->destructor = destructor;
  return destructor;
}

void resolveClassDestructor(astClass* aclass, State* state) {
  // TODO: Simply calling classes function "destructor" is error prone:
  // if there is a typo, the destructor function will simply be ignored
  // as if it was an unused function. The fix should be to come up with a
  // good syntax that would prevent such madness, like in C++ ~Class();
  // Maybe something like compiler directives, like #constructor ?
  // Since compiler directives have a limited vocabulary, typos would be easily
  // caught via incorrect compiler directive error.
  // ??? Already done?

  std::vector<astFunction*> destructors =
      astFindAllFunctionsInScope(aclass->scope, "destructor", false);

  if (destructors.size() >= 2) {
    state->reportError(
        aclass->location,
        FORMAT("a class may have only 1 destructor (class '@' has @)",
               aclass->name, (int)destructors.size()));
    return;
  }

  astFunction* destructor = nullptr;

  if (!destructors.empty()) {
    destructor = destructors.at(0);
  }

  if (destructor) {
    aclass->destructor = destructor;
  } else {
    destructor = generateClassDestructor(aclass);
  }
  if (!aclass->destructor->parameters.empty()) {
    state->reportError(
        destructor->location,
        FORMAT("a destructor should not have any parameters (class '@')",
               aclass->name));
  }
  if (destructor->returnType->name != astTypeName::Void) {
    state->reportError(
        destructor->returnType->location,
        FORMAT("return type of destructor must be void for class '@'",
               aclass->name));
  }
}

void checkScope(astScope* scope, State* state);
void checkBlock(astBlock* block, State* state);

void checkFunction(astScope* parentScope, astFunction* function, State* state) {
  state->pushFunctionScope(function);
  checkExternFunctionIsInGlobalScope(parentScope, function, state);
  checkAPIFunctionIsInGlobalScope(parentScope, function, state);
  for (auto& parameter : function->parameters) {
    state->fscope->declaredVariables.insert(parameter);
  }
  checkScope(function->scope, state);

  state->pushBlockInfo();
  checkBlock(function->block, state);
  if (function->arrayFunction || function->constructorFunction) {
    // Exceptions for compiler generated functions which generate
    // false positives 100% of the time, because of the way they are
    // implemented.
  } else if (function->isExternFunction) {
    // Extern function doesn't need to return anything,v since its
    // implementation is somewhere else.
  } else {
    if (!state->blockInfo->blockHasReturn &&
        !state->blockInfo->allPathsHaveReturnStatements()) {
      if (function->returnType->name == astTypeName::Void) {
        function->defaultVoidReturn = true;
      } else {
        state->reportError(
            function->location,
            FORMAT("function '@' does not return in some control paths",
                   function->name));
      }
    }
    if (function->returnType->name == astTypeName::Reference) {
      state->reportError(
          function->location,
          FORMAT("functions can't return reference (&) types (function '@')",
                 function->name));
    }
  }
  state->popBlockInfo();

  state->popFunctionScope();
}

void resolveClass(astClass* aclass, State* state) {
  state->classStack.push(aclass);
  checkClassDoesNotContainItselfAsMemberVariable(aclass, state);
  checkScope(aclass->scope, state);

#if 0
  for (auto& variable : aclass->scope->variables) {
    if (variable->type->getClass() &&
        !variable->type->getClass()->default_constructor) {
      state->reportError(variable->location,
                         FORMAT("'@' does not have a default constructor",
                                astTypeToString(variable->type)));
    }
  }
#endif

  state->classStack.pop();
}

bool expressionIsValidGlobalVariableInitializer(astExpression* expression) {
  for (astExpressionNode* node : expression->nodes) {
    if (astSymbol* symbol = node->getSymbol()) {
      if (!symbol->constant) {
        return false;
      }
    } else if (astExpression* subExpression = node->getSubExpression()) {
      if (!expressionIsValidGlobalVariableInitializer(subExpression)) {
        return false;
      }
    } else if (astOperatorInstance* op = node->getOperator()) {
      // Nothing..
    } else {
      ASSERT(false);
    }
  }
  return true;
}

void resolveGlobalVariable(astVariable* variable, State* state) {
  if (!variable->global) {
    return;
  }

  if (variable->initializeWithExpression) {
    resolveExpression(variable->initializeWithExpression, state);
    if (!expressionIsValidGlobalVariableInitializer(
            variable->initializeWithExpression)) {
      state->reportError(variable->initializeWithExpression->location,
                         "For now, only constant values (eg: 123, \"hello\") "
                         "are allowed for global variable initializers");
    }
  } else {
    state->reportError(
        variable->location,
        "global variables must be initialized with an expression");
  }
}

void checkScope(astScope* scope, State* state) {
  checkDuplicateClasses(scope, state);
  checkDuplicateFunctionsInScope(scope, state);
  checkDuplicateVariablesInScope(scope, state);
  checkDuplicateTypeAliasesInScope(scope, state);
  for (auto& aclass : scope->classes) {
    resolveClass(aclass, state);
  }
  for (auto& aclass : scope->instantiatedTemplatedClasses) {
    state->templateLocationStack.push(&aclass->templateInstantiationStack);
    resolveClass(aclass, state);
    state->templateLocationStack.pop();
  }
  for (auto& variable : scope->variables) {
    resolveGlobalVariable(variable, state);
  }
  for (auto& function : scope->functions) {
    checkFunction(scope, function, state);
  }
}

void resolveFunctionCall2(astFunctionCall2* call,
                          astType* lastType,
                          State* state);
void resolveVariableRef2(astVariableRef2* ref,
                         astType* targetType,
                         State* state);

#if 0
astType*
dotExpressionType(astExpression * expression)
{
	// Given expression: (123 < building.id)
	// 123<building would consider it like 123.building, which is incorrect.
	// Compiler should terminate dot access pattern when operator is not dot.
	if (expression->operatorToken != astOperator::Dot) {
		return nullptr;
	}
	return expression->left->type;
}
#endif

void resolveSymbol(astSymbol* symbol, astType* lastType, State* state) {
  if (symbol->variable2) {
    resolveVariableRef2(symbol->variable2, lastType, state);
  } else if (symbol->functionCall2) {
    resolveFunctionCall2(symbol->functionCall2, lastType, state);
  } else if (symbol->constant) {
    // nothing to do here ??
    ASSERT(symbol->constant);
    ASSERT(symbol->constant->type->name != astTypeName::Unresolved);
  } else {
    ASSERT(false);
  }
}

#if 0
void
internalResolveExpression(astExpression * expression, astType * lastType, State * state)
{
	ASSERT(expression);
	if (expression->symbol) {
		ASSERT(expression->left == nullptr);
		ASSERT(expression->right == nullptr);
		ASSERT(expression->type == nullptr);

		astSymbol * symbol = expression->symbol;
		resolveSymbol(symbol, lastType, state);
        inferExpressionTypeBasedOnSymbol(expression);
	}
	else {
		ASSERT(expression->left);
		ASSERT(expression->right);
		internalResolveExpression(expression->left, lastType, state);
		internalResolveExpression(expression->right, dotExpressionType(expression), state);
	}
}
#endif

void insertDeclaredVariable(astVariable* variable, State* state) {
  ASSERT(state->fscope);
  ASSERT(state->fscope->declaredVariables.find(variable) ==
         state->fscope->declaredVariables.end());
  state->fscope->declaredVariables.insert(variable);
}

astType* getPreviousDotType(astExpression* expression, int currentNodeIndex) {
  currentNodeIndex -= 1;       // Goto previous node
  if (currentNodeIndex < 0) {  // This just means there was no previous opeator.
    return nullptr;
  }

  // Try to look back from the current node and see if there is a dot operator.
  // If there is, try to extract the type of symbol from the dot operator.
  astExpressionNode* operatorNode = expression->nodes.at(currentNodeIndex);
  astOperatorInstance* op = operatorNode->getOperator();
  ASSERT(op);
  if (op->op != astOperator::Dot) {
    return nullptr;
  }

  currentNodeIndex -= 1;  // Goto previous node after the operator

  // If this triggers, that probably means expression is malformed somewhere.
  // Right now the structure should be [symbol op symbol]
  // This assert signals that the structure is rather [op symbol]
  ASSERT(currentNodeIndex >= 0);

  astExpressionNode* previousNode = expression->nodes.at(currentNodeIndex);
  return previousNode->getNodeType();
}

void checkOperatorTypes(astExpression* expression, int index, State* state) {
  astExpressionNode* right = expression->nodes.at(index);
  astType* rightType = right->getNodeType();
  // astSymbol * rightSymbol = right->getSymbol();
  // ASSERT(rightSymbol);

  index -= 1;  // Find previous operator
  if (index < 0) {
    return;
  }

  astExpressionNode* operatorNode = expression->nodes.at(index);
  astOperatorInstance* op = operatorNode->getOperator();
  ASSERT(op);

  index -= 1;  // Find symbol before the operator
  if (index < 0) {
    // Malformed expression? There should always be a symbol before an operator
    ASSERT(false);
    return;
  }

  astExpressionNode* left = expression->nodes.at(index);
  astType* leftType = left->getNodeType();
  // astSymbol * leftSymbol = left->getSymbol();
  // ASSERT(leftSymbol);

  if (operatorIsMath(op->op)) {
    if (!isNumericType(leftType)) {
      state->reportError(
          left->getLocation(),
          FORMAT("math operators can't be used on symbol of type '@'",
                 astTypeToString(leftType)));
    }
    if (!isNumericType(rightType)) {
      state->reportError(
          right->getLocation(),
          FORMAT("math operators can't be used on symbol of type '@'",
                 astTypeToString(rightType)));
    }
    if (!astTypesMatch(leftType, rightType)) {
      state->reportError(
          op->location,
          FORMAT(
              "symbol types '@' and '@' don't match, so operator can't be used",
              astTypeToString(leftType), astTypeToString(rightType)));
    }
  } else if (isComparisonOperator(op->op)) {
    if (!(isNumericType(leftType) || leftType->name == astTypeName::Bool)) {
      state->reportError(
          left->getLocation(),
          FORMAT("comparison operators can't be used on symbol of type '@'",
                 astTypeToString(leftType)));
    }
    if (!(isNumericType(rightType) || rightType->name == astTypeName::Bool)) {
      state->reportError(
          right->getLocation(),
          FORMAT("comparison operators can't be used on symbol of type '@'",
                 astTypeToString(rightType)));
    }
    if (!astTypesMatch(leftType, rightType)) {
      state->reportError(
          op->location,
          FORMAT(
              "symbol types '@' and '@' don't match, so operator can't be used",
              astTypeToString(leftType), astTypeToString(rightType)));
    }
  } else if (isOperatorLogical(op->op)) {
    if (leftType->name != astTypeName::Bool) {
      state->reportError(
          left->getLocation(),
          FORMAT("symbol is type '@', but logical operators require Bool",
                 astTypeToString(leftType)));
    }
    if (rightType->name != astTypeName::Bool) {
      state->reportError(
          right->getLocation(),
          FORMAT("symbol is type '@', but logical operators require Bool",
                 astTypeToString(rightType)));
    }
  } else if (op->op == astOperator::Dot) {
    // Nothing ..
  } else {
    ASSERT(false);
  }
}

void checkTypeCast(astType* original, astType* casted, State* state) {
  if (!isNumericType(original)) {
    state->reportError(original->location,
                       FORMAT("'@' can't be casted, because only numeric types "
                              "are allowed for casting (because WIP)",
                              astTypeToString(original)));
  }
  if (!isNumericType(casted)) {
    state->reportError(casted->location,
                       FORMAT("can't cast to '@', because casting is allowed "
                              "only to numeric types (because WIP)",
                              astTypeToString(casted)));
  }
}
void resolveExpression(astExpression* expression, State* state) {
  astOperatorInstance* lastOperator = nullptr;

  ASSERT(!expression->nodes.empty());
  for (int i = 0; i < (int)expression->nodes.size(); i++) {
    astExpressionNode* node = expression->nodes.at(i);
    if (astSymbol* symbol = node->getSymbol()) {
      astType* previousDotType = getPreviousDotType(expression, i);
      resolveSymbol(symbol, previousDotType, state);
      if (astType* typeCast = node->getTypeCast()) {
        checkTypeCast(symbol->getSymbolType(), typeCast, state);
      }
      checkOperatorTypes(expression, i, state);
    } else if (astOperatorInstance* op = node->getOperator()) {
      lastOperator = op;
    } else if (astExpression* subExpression = node->getSubExpression()) {
      // Not done yet, need to test properly.
      // ASSERT(false);
      resolveExpression(subExpression, state);
      if (astType* typeCast = node->getTypeCast()) {
        checkTypeCast(subExpression->flatType, typeCast, state);
      }
      checkOperatorTypes(expression, i, state);
    }

    if (ExpressionModifier* modifier = node->getModifier()) {
      if (modifier->type == ExpressionModifierType::Negative) {
        if (!isNumericType(node->getNodeType())) {
          state->reportError(
              modifier->location,
              FORMAT("'-' not usable for '@', only numeric types allowed",
                     astTypeToString(node->getNodeType())));
        }
      } else {
        ASSERT(false);
      }
    }
  }
  ASSERT(expression->flatType == nullptr);
  if (lastOperator != nullptr && isComparisonOperator(lastOperator->op)) {
    expression->flatType =
        astNewBasicType(astTypeName::Bool, lastOperator->location);
  } else {
    expression->flatType = expression->nodes.back()->getNodeType();
  }
  ASSERT(expression->flatType);
}

bool variableIsDeclared(astVariable* variable, State* state) {
  ASSERT(state->fscope);
  auto iter = state->fscope->declaredVariables.find(variable);
  return iter != state->fscope->declaredVariables.end();
}

astVariable* findDeclaredVariable(astScope* scope,
                                  const std::string& name,
                                  const astLocation& location,
                                  bool checkForDeclaration,
                                  State* state) {
  astVariable* variable =
      astFindVariableInScope(scope, name, true, scope->flag_member);
  if (!variable) {
    return nullptr;
  }

  if (variable->memberOf) {
    // Member Variables don't need to be checked,
    // because they are already declared when a function is executed.
    checkForDeclaration = false;
  }

  if (variable->global) {
    // Globals variables are always "declared"
    checkForDeclaration = false;
  }

  if (checkForDeclaration && !variableIsDeclared(variable, state)) {
    state->reportError(
        location,
        FORMAT("variable '@' is found, but it has not yet been declared",
               variable->name));
  }

  return variable;
}
void resolveVariableRef2(astVariableRef2* ref,
                         astType* targetType,
                         State* state) {
  if (ref->variable) {
    ASSERT(false);  // Currently this logic is not supposed to work! Remove at
                    // your own conscience.
    // See AST.cpp: astWrapVariableInExpression()
    // There are compiler generated expressions which don't need to resolve
    // variable reference. Compiler already knows which variable is being
    // referenced when the expression is created.
    ASSERT(
        ref->stringVariable.empty());  // When .stringVariable is empty, we know
                                       // that variable ref is auto generated
    return;
  }

  if (targetType) {
    astClass* aclass = removeReference(targetType)->aclass;
    if (aclass == nullptr) {
      std::string msg = FORMAT(
          "trying to get variable '@' from non-class type '@' (variables can "
          "only be gotten from class types)",
          ref->stringVariable, astTypeToString(targetType));
      state->reportError(ref->location, msg);
    }

    astVariable* variable =
        astFindVariableInScope(aclass->scope, ref->stringVariable, false, true);
    ref->variable = variable;
    if (!variable) {
      std::string msg =
          FORMAT("member variable '@' not found in class '@'",
                 ref->stringVariable, astTypeToString(targetType));
      state->reportError(ref->location, msg);
    }

    if (variable->thisContext()) {
      state->reportError(
          ref->location,
          FORMAT("'@' may only be used as first term of an expression",
                 ref->variable->name));
    }
  } else {
    ref->variable = findDeclaredVariable(ref->scope, ref->stringVariable,
                                         ref->location, true, state);
    if (ref->variable) {
      if (ref->variable->memberOf && ref->variable->name != "this") {
        state->reportError(ref->location,
                           FORMAT("'@' may only be accessed with 'this.'",
                                  ref->variable->name));
      }
    } else {
      state->reportError(ref->location,
                         FORMAT("variable '@' not found", ref->stringVariable));
    }
  }
}

bool functionCallArgumentTypesMatchParameters(
    const std::vector<astExpression*>& arguments,
    astFunction* function) {
  if (arguments.size() != function->parameters.size()) {
    return false;
  } else {
    for (int i = 0; i < (int)arguments.size(); i++) {
      astType* argType = arguments.at(i)->flatType;
      astType* paramType = function->parameters.at(i)->type;
      if (!astTypesMatchSkipReferences(argType, paramType)) {
        return false;
      }
    }
    return true;
  }
}

astFunction* findOverloadFunction(astScope* scope,
                                  const std::string& name,
                                  const std::vector<astExpression*>& arguments,
                                  bool recurse,
                                  bool* outFoundCandidates) {
  std::vector<astFunction*> overloadCandidates =
      astFindAllFunctionsInScope(scope, name, recurse);
  if (!overloadCandidates.empty()) {
    *outFoundCandidates = true;
    astFunction* overloadMatch = nullptr;
    for (auto& candidate : overloadCandidates) {
      if (functionCallArgumentTypesMatchParameters(arguments, candidate)) {
        overloadMatch = candidate;
        break;
      }
    }
    if (overloadMatch) {
      return overloadMatch;
    }
  }
  return nullptr;
}

void validateFunctionCallArguments(astFunctionCall2* call, State* state) {
  for (int i = 0; i < (int)call->arguments.size(); i++) {
    astType* parameterType = call->function->parameters.at(i)->type;
    astExpression* argument = call->arguments.at(i);
    if (argument->flatType->name != astTypeName::Reference &&
        parameterType->name == astTypeName::Reference &&
        parameterType->isMutableReference()) {
      astSymbol* symbol = argument->getEndingSymbol()->getSymbol();
      ASSERT(symbol);
      if (symbol->functionCall2) {
        state->reportError(argument->location,
                           FORMAT("creating a mutable reference to temporary "
                                  "value '@' is not allowed",
                                  tostring(symbol)));
      } else if (symbol->constant) {
        state->reportError(argument->location,
                           FORMAT("creating a mutable reference to constant "
                                  "value '@' is not allowed",
                                  tostring(symbol)));
      } else if (symbol->variable2) {
        // taking ref of a variable is allowed
      } else {
        ASSERT(false);
      }
    }
  }
}

void resolveFunctionOverloads(astFunctionCall2* call,
                              const std::string& function_name,
                              astClass* aclass,
                              State* state) {
  bool foundCandidates = false;
  astFunction* overload = findOverloadFunction(
      aclass->scope, function_name, call->arguments, false, &foundCandidates);
  if (!overload) {
    if (foundCandidates) {
      // TODO: nicer message
      state->reportError(
          call->location,
          FORMAT("function '@' did not match any overloads in class '@'",
                 function_name, aclass->name));
    } else {
      state->reportError(call->location,
                         FORMAT("member function '@' not found in class '@'",
                                function_name, aclass->name));
    }
  }
  call->function = overload;
}

void resolveFunctionCall2(astFunctionCall2* call,
                          astType* targetType,
                          State* state) {
  resolveFunctionCallArguments(call->arguments, state);

  std::string function_name = call->type->string;

  if (!call->type->type_error_supressed) {
    astClass* aclass = call->type->getClass();
    if (!aclass) {
      state->reportError(
          call->location,
          FORMAT("attempt to construct an object for non class type '@'",
                 astTypeToString(call->type)));
    }
    call->passThisContext = false;
    resolveFunctionOverloads(call, "constructor", aclass, state);

  } else if (targetType) {
    astClass* aclass = removeReference(targetType)->aclass;

    if (removeReference(targetType)->name == astTypeName::Array) {
      ASSERT(removeReference(targetType)->arrayClass);
      aclass = removeReference(targetType)->arrayClass;
    }

    if (aclass == nullptr) {
      std::string msg = FORMAT(
          "trying to call function '@' from non-class type '@' (functions can "
          "only be called on class types)",
          function_name, astTypeToString(targetType));
      state->reportError(call->location, msg);
    }

    resolveFunctionOverloads(call, function_name, aclass, state);

  } else {
    ASSERT(call->function == nullptr);
    bool foundCandidates = false;
    astFunction* overload = findOverloadFunction(
        call->scope, function_name, call->arguments, true, &foundCandidates);
    call->function = overload;
    if (!overload) {
      if (foundCandidates) {
        // TODO: display nice error which would make it easy to understand
        // which arguments are not matching with which overload candidates
        state->reportError(
            call->location,
            FORMAT("function '@' did not match any overloads", function_name));
      } else {
        astScope* scopeOfTemplatedClass = nullptr;
        parsed::Function* templateFunction = astFindTemplatedFunctionInScope(
            call->scope, function_name, true, &scopeOfTemplatedClass);
        if (templateFunction) {
          call->function = instantiateFunctionTemplate(
              *templateFunction, call->arguments, call->templateArguments,
              call->location, scopeOfTemplatedClass, state);
          ASSERT(call->function);
        }
      }
    }

    if (call->function == nullptr) {
      state->reportError(call->location,
                         FORMAT("function '@' not found", function_name));
    }

    // Determine if 'this' context needs to be passed. EG:
    /*
            struct Foo {
                    fn bar() {
                            zet(); // Implicitly pass 'this', since we are
       calling a member function
                    }
                    fn zet() {
                    }
            }
    */
    if (call->function) {
      // TODO: This probably doesn't work right for lambdas???
      astClass* classCurrentlyBeingResolved =
          state->classStack.empty() ? nullptr : state->classStack.getCurrent();
      if (classCurrentlyBeingResolved &&
          call->function->memberOf == classCurrentlyBeingResolved) {
        // std::cout << "passing this context (" <<
        // classCurrentlyBeingResolved->name << "::" << call->function->name <<
        // ")" << std::endl;
        call->passThisContext = true;
      }
    }
  }

  if (call->function) {
    if (call->function->isAPIFunction) {
      state->reportError(
          call->location,
          FORMAT("API function '@' may only be used from outside",
                 call->function->name));
    }
    checkFunctionCall2ArgumentsMatchWithFunctionSignature(call, state);
    validateFunctionCallArguments(call, state);
  } else {
    // ???? This code is quite complex; we can either silently ignore this case,
    // assert (crash) or do compilation error. I think the best solution would
    // be to throw compilation error, because ignoring is bad and crashing is
    // even worse.
    state->reportError(
        call->location,
        FORMAT("internal compiler error: function call '@' failed to be found",
               function_name));
  }
}

void checkAssignmentTypesMatch(ExpressionVariable target,
                               astExpression* expression,
                               astLocation location,
                               State* state) {
  astType* targetType = target.getType();
  if (!astTypesMatchSkipReferences(targetType, expression->flatType)) {
    state->reportError(location, FORMAT("cannot assign '@' to '@'",
                                        astTypeToString(expression->flatType),
                                        astTypeToString(targetType)));
  }
}

bool isExpressionAssignable(astExpression* expression) {
  astExpressionNode* node = expression->getEndingSymbol();
  if (!node) {
    return false;
  }
  astSymbol* symbol = node->getSymbol();
  ASSERT(symbol);

  if (symbol->variable2) {
    if (symbol->variable2->variable->thisContext()) {
      // Assigning to 'this' is not allowed;
      // The state of the program after such an action is logically predictable,
      // but in practice it allows to accidentally write a lot of erroneous
      // code.
      return false;
    }
  }

  return true;
}

astVariable* TryDeclareVariable(astExpression* target,
                                astExpression* initializer,
                                State* state) {
  if (target->nodes.size() != 1) {
    return nullptr;
  }
  astExpressionNode* node = target->nodes.at(0);
  astSymbol* symbol = node->getSymbol();
  if (!symbol) {
    return nullptr;
  }
  if (!symbol->variable2) {
    return nullptr;
  }

  astVariableRef2* ref = symbol->variable2;

  bool check_for_declaration = false;
  if (findDeclaredVariable(ref->scope, ref->stringVariable, ref->location,
                           check_for_declaration, state)) {
    // If the variable already exists, there is nothing to declare
    return nullptr;
  }

  astVariable* variable = new astVariable();
  variable->location = ref->location;
  variable->name = ref->stringVariable;

  ASSERT(initializer->flatType);
  // Currently we don't allow creating references.
  variable->type = removeReference(initializer->flatType);

  ref->scope->variables.push_back(variable);
  insertDeclaredVariable(variable, state);
  return variable;
}

class TypeCheckVisitor : public StatementVisitor {
 public:
  State* state = nullptr;

  TypeCheckVisitor(State* state) : state(state) {}

  virtual ~TypeCheckVisitor() {}

  void visit(astVariableDecl* decl) override {
    insertDeclaredVariable(decl->variable, state);
    if (decl->expression) {
      resolveExpression(decl->expression, state);
      if (decl->variable->type) {
        checkAssignmentTypesMatch(decl->variable, decl->expression,
                                  decl->assignLocation, state);
      } else {
        ASSERT(decl->expression->flatType);
        astType* valueType = removeReference(
            decl->expression
                ->flatType);  // Currently we don't allow creating references.
        decl->variable->type = valueType;
      }
    } else {
      ASSERT(decl->variable->type);
      ASSERT(decl->variable->type->name != astTypeName::TypeAlias);
      if (typeDoesNotHaveDefaultConstructor(decl->variable->type)) {
        state->reportError(decl->variable->location,
                           FORMAT("class '@' does not have default constructor",
                                  decl->variable->type->getClass()->name));
      }
    }
  }
  void visit(astVariableAssign* assign) override {
    ASSERT(assign->expression);
    resolveExpression(assign->expression, state);

    if (assign->target) {
      assign->variable_decl =
          TryDeclareVariable(assign->target, assign->expression, state);
      resolveExpression(assign->target, state);
    } else {
      ASSERT(false);
    }

    if (assign->target) {
      if (!isExpressionAssignable(assign->target)) {
        state->reportError(assign->target->location,
                           "expression is not assignable");
      }

      checkAssignmentTypesMatch(assign->target, assign->expression,
                                assign->location, state);
      if (assign->target->flatType->name == astTypeName::Reference) {
        if (!assign->target->flatType->isMutableReference()) {
          state->reportError(
              assign->location,
              FORMAT("assignment of immutable reference '@' is not allowed",
                     tostring(assign->target)));
        }
      }
    } else {
      ASSERT(false);
    }
  }
  void visit(astIf* anIf) override {
    resolveExpression(anIf->expression, state);
    if (anIf->expression->flatType->name != astTypeName::Bool) {
      state->reportError(
          anIf->expression->location,
          FORMAT("'if' expression must be 'bool', but was given '@'",
                 astTypeToString(anIf->expression->flatType)));
    }
    checkScope(anIf->scope, state);
    checkBlock(anIf->block, state);
    checkScope(anIf->elseScope, state);
    checkBlock(anIf->elseBlock, state);
  }
  void visit(astWhile* awhile) override {
    resolveExpression(awhile->expression, state);
    checkScope(awhile->scope, state);
    checkBlock(awhile->block, state);
  }
  void visit(astReturn* ret) override {
    ASSERT(state->fscope);
    astFunction* function = state->fscope->function;
    if (ret->expression) {
      resolveExpression(ret->expression, state);
      if (!astTypesMatchSkipReferences(function->returnType,
                                       ret->expression->flatType)) {
        state->reportError(
            ret->expression->location,
            FORMAT("function return type is '@', but return statement gave '@'",
                   astTypeToString(function->returnType),
                   astTypeToString(ret->expression->flatType)));
      }
    } else {
      // Return statement has no expression, that means its 'void'
      if (function->returnType->name != astTypeName::Void) {
        state->reportError(
            ret->location,
            FORMAT(
                "function return type is '@', but return statement gave 'void'",
                astTypeToString(function->returnType)));
      }
    }
    state->blockInfo->blockHasReturn = true;
  }
  void visit(astStatementExpression* statementExpression) override {
    resolveExpression(statementExpression->expression, state);
  }
  void visit(astStatementBreak*) override {
    // Nothing??
  }
  void visit(astStatementContinue*) override {
    // Nothing??
  }
  void visit(astStatementNative*) override {
    // Nothing??
  }
};

void checkBlock(astBlock* block, State* state) {
  state->pushBlockInfo();

  ASSERT(state->visitor);
  for (auto iter = block->statements.begin(); iter != block->statements.end();
       ++iter) {
    astStatement* statement = *iter;
    statement->acceptVisitor(*state->visitor);
  }

  if (!state->blockInfo->blockHasReturn) {
    if (state->blockInfo->allPathsHaveReturnStatements()) {
      state->blockInfo->blockHasReturn = true;
    }
  }

  bool blockHasReturn = state->blockInfo->blockHasReturn;
  state->popBlockInfo();

  // Block checking starts at function block; function is supposed to catch
  // block info, so it must be already pushed here;
  ASSERT(state->blockInfo);
  state->blockInfo->numberOfChildBlocks += 1;
  if (blockHasReturn) {
    state->blockInfo->childBlocksWhichReturned += 1;
  }
}

State::State(SyntaxTree* ast, ASTCache& cache)
    : ast(ast), cache(cache), visitor(new TypeCheckVisitor(this)) {}

#if 0
void
collapseTypeAlias(astType * type)
{
	// Firstly we have to collapse sub-typealiases, because otherwise we would be
	// overwriting our type alias with another typealias (in case of recursive type
	// aliases)
	for (auto & subtype : type->getSubtypes()) {
		collapseTypeAlias(subtype);
	}
	if (type->name == astTypeName::TypeAlias) {
		ASSERT(type->getSubtypes().size() == 1);
		*type = *type->getSubtypes().at(0);
		ASSERT(type->name != astTypeName::Unresolved && type->name != astTypeName::TypeAlias);
	}
}
#endif

void instantiateArrayClass(astType* type, std::vector<astClass*>& arrayCache) {
  for (auto& subtype : type->getSubtypes()) {
    instantiateArrayClass(subtype, arrayCache);
  }

  if (type->name == astTypeName::Array) {
    ASSERT(type->getSubtypes().size() == 1);
    astType* subtype = type->getSubtypes().at(0);
    astClass* arrayClass = getCachedArrayClass(subtype, arrayCache);
    type->arrayClass = arrayClass;
  }
}

}  // namespace

bool runTypeCheck(SyntaxTree* ast, ASTCache& cache, astError* error) {
  try {
    State state(ast, cache);
    checkScope(ast->scope, &state);
  } catch (TypeCheckException& e) {
    *error = e.compilerError;
    return false;
  }
  return true;
}

void typeCheckTypes(SyntaxTree* ast,
                    ASTCache& cache,
                    std::vector<astError>& errors) {
  try {
    State state(ast, cache);
    resolveTypes(ast->allTypes, &state);
    while (!state.typeQueue.empty()) {
      std::vector<astType*> queue = state.typeQueue;
      state.typeQueue.clear();
      resolveTypes(queue, &state);
      ast->allTypes.insert(ast->allTypes.end(), queue.begin(), queue.end());
    }
  } catch (TypeCheckException& e) {
    errors.push_back(e.compilerError);
  }
}

void resolveClassConstructors(SyntaxTree* ast,
                              ASTCache& cache,
                              std::vector<astError>& errors) {
  try {
    State state(ast, cache);
    for (auto& aclass : ast->allClasses) {
      resolveClassConstructor(aclass, &state);
      resolveClassCopyConstructor(aclass, &state);
      resolveClassDestructor(aclass, &state);
    }
  } catch (TypeCheckException& e) {
    errors.push_back(e.compilerError);
  }
}

#if 0
void
collapseTypeAliases(AST * ast)
{
	// There are 2 ways to implement type aliases:
	// 1) Use dereferenceType(astType*) function EVERY time a type might contain a type alias.
	//     This is EXTREMELY error prone, since you might completely forget that you need to
	//		deref the type, and you will have broken code that works without type aliases,
	// 		but might completely break after a type alias is introduced.
	// 2) dereference type aliases inline, modifying the AST, before any work that requires
	//   	dereferenced type aliases is done. This prevents ALL the edge cases that 1) has.
	for (auto & type : ast->allTypes) {
		collapseTypeAlias(type);
	}
}
#endif

void instantiateArrayClasses(SyntaxTree* ast, ASTCache& cache) {
  // Array classes must be instantiated after collapsing type aliases.
  // Otherwise, the compiler will think that [](alias)->bool == [](alias)->i32
  // The the subtype of array is alias in both cases, compiler will think the
  // types of the arrays are the same!
  for (auto& type : ast->allTypes) {
    instantiateArrayClass(type, cache.arrayClasses);
  }
}
