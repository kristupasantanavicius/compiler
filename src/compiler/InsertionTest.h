
#if 0

#include <string>
#include <vector>

#define VECTOR_INSERTION_TEST

void testVectorInsertion() {
  std::vector<std::string> statements = {
      "first", "second", "third", "fourth", "fifth",
  };

  std::vector<std::string> queueBefore = {
      "before1",
      "before2",
      "before3",
  };

  std::vector<std::string> queueAfter = {
      "after1",
      "after2",
      "after3",
  };

  for (auto iter = statements.begin(); iter != statements.end(); iter++) {
    std::string statement = *iter;
    LOG(DEBUG) << "doing shit with statement: " << statement;
    if (statement == "second") {
      for (auto& str : queueBefore) {
        iter = statements.insert(iter, str);
        iter += 1;
      }
      for (auto& str : queueAfter) {
        iter += 1;
        iter = statements.insert(iter, str);
      }
#if 0
			while (!queueBefore.empty()) {
				iter = statements.insert(iter, queueBefore.front());
				queueBefore.erase(queueBefore.begin());
				iter++;
			}
			while (!queueAfter.empty()) {
				iter += 1;
				iter = statements.insert(iter, queueAfter.front());
				queueAfter.erase(queueAfter.begin());
			}
#endif
    }
  }

  LOG(DEBUG);
  LOG(DEBUG);
  LOG(DEBUG);

  for (auto& statement : statements) {
    LOG(DEBUG) << statement;
  }
}

#endif
