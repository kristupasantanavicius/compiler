#pragma once

#include <mpark/variant.hpp>

namespace std {
using namespace mpark;
}

#define VARIANT_INTERFACE_DECL(TYPE) \
 protected:                          \
  virtual void visit(TYPE) = 0;      \
                                     \
 public:                             \
  void operator()(TYPE variable) { visit(variable); }
