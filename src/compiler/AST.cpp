#include <compiler/AST.h>
#include <compiler/Error.h>
#include <compiler/Format.h>
#include <algorithm>
#include <sstream>

SyntaxTree::SyntaxTree(const std::string& module_name) {
  this->scope = new astScope(ScopeOwner::Module(module_name), nullptr);
}

astClass::astClass(const std::string& name, astScope* parentScope)
    : name(name) {
  scope = new astScope(ScopeOwner::Class(this), parentScope);
  scope->flag_member = true;
}

astFunction::astFunction(const std::string& name, astScope* parentScope)
    : name(name) {
  this->scope = new astScope(ScopeOwner::Function(this), parentScope);
  this->block = new astBlock(astBlockType::Root);
}

std::string astTypeToString(astType* type) {
  std::stringstream output;
  if (type->name == astTypeName::TypeAlias) {
    output << "alias(" << type->string << ")->(";
  } else if (type->name == astTypeName::Pointer) {
    output << "*";
    ASSERT(type->getSubtypes().size() == 1);
    output << astTypeToString(type->getSubtypes().at(0));
    return output.str();
  } else {
    output << type->string;
  }

  if (type->name == astTypeName::Array) {
    ASSERT(type->getSubtypes().size() == 1);
    output << astTypeToString(type->getSubtypes().at(0));
    return output.str();
  }

  if (type->name == astTypeName::Class) {
    if (!type->getSubtypes().empty()) {
      output << "<";
    }
    bool coma = false;
    for (auto& subtype : type->getSubtypes()) {
      if (coma) {
        output << ", ";
      } else {
        coma = true;
      }
      output << astTypeToString(subtype);
    }
    if (!type->getSubtypes().empty()) {
      output << ">";
    }
  } else {
    for (auto& subtype : type->getSubtypes()) {
      output << "_" << astTypeToString(subtype);
    }
  }

  if (type->name == astTypeName::TypeAlias) {
    output << ")";
  }

  return output.str();
}

astClass* astFindClassInScope(astScope* scope,
                              const std::string& name,
                              bool recurse) {
  auto iter = std::find_if(
      scope->classes.begin(), scope->classes.end(),
      [name](const astClass* aclass) { return aclass->name == name; });
  if (iter != scope->classes.end()) {
    return *iter;
  } else {
    if (!recurse) {
      return nullptr;
    }
    if (scope->parent) {
      return astFindClassInScope(scope->parent, name, recurse);
    } else {
      for (auto& otherFile : scope->otherFileScopes) {
        astClass* aclass = astFindClassInScope(otherFile, name, false);
        if (aclass) {
          return aclass;
        }
      }
      return nullptr;
    }
  }
}

parsed::Class* astFindTemplatedClassInScope(astScope* scope,
                                            const std::string& name,
                                            astScope** outputScope,
                                            bool recurse) {
  auto iter = std::find_if(
      scope->templatedClasses.begin(), scope->templatedClasses.end(),
      [name](const parsed::Class& aclass) { return aclass.name == name; });
  if (iter != scope->templatedClasses.end()) {
    *outputScope = scope;
    return &(*iter);
  } else {
    if (!recurse) {
      return nullptr;
    }
    if (scope->parent) {
      return astFindTemplatedClassInScope(scope->parent, name, outputScope);
    } else {
      for (auto& otherFile : scope->otherFileScopes) {
        parsed::Class* aclass =
            astFindTemplatedClassInScope(otherFile, name, outputScope, false);
        if (aclass) {
          return aclass;
        }
      }
      return nullptr;
    }
  }
}

astVariable* astFindVariableInScope(astScope* scope,
                                    const std::string& name,
                                    bool recurse,
                                    bool flag_member) {
  auto iter = std::find_if(scope->variables.begin(), scope->variables.end(),
                           [&](const astVariable* variable) {
                             return variable->name == name &&
                                    (flag_member || !variable->memberOf);
                           });
  if (iter != scope->variables.end()) {
    return *iter;
  } else if (flag_member && scope->thisVariable &&
             scope->thisVariable->name == name) {
    return scope->thisVariable;
  } else {
    if (!recurse) {
      return nullptr;
    }

    if (scope->parent) {
      if (scope->owner.aclass && scope->parent->owner.module == "") {
        // Variable scope can't penetrate through a class, unless the parent
        // scope is global. For example, if this is a local Class (defined
        // inside a function), the parent scope would be a function, and stuff
        // beyond that boundary should be off limits for a class.
        return nullptr;
      } else {
        return astFindVariableInScope(scope->parent, name, recurse,
                                      scope->flag_member);
      }
    } else {
      for (auto& otherFile : scope->otherFileScopes) {
        astVariable* variable =
            astFindVariableInScope(otherFile, name, false, false);
        if (variable) {
          return variable;
        }
      }
      return nullptr;
    }
  }
}

bool functionMayPenetrateScope(astScope* scope) {
  // Function scope can't penetrate through a class, unless the parent scope is
  // global. For example, if this is a local Class (defined inside a function),
  // the parent scope would be a function, and stuff beyond that boundary should
  // be off limits for a class.
  if (scope->owner.aclass) {
    return scope->parent->owner.module != "";
  }
  return true;
}

void internalFindAllFunctionsInScope(astScope* scope,
                                     const std::string& name,
                                     bool recurse,
                                     std::vector<astFunction*>& output) {
  for (auto& function : scope->functions) {
    if (function->name == name) {
      output.push_back(function);
    }
  }

  if (!recurse) {
    return;
  }

  if (scope->parent && functionMayPenetrateScope(scope)) {
    internalFindAllFunctionsInScope(scope->parent, name, recurse, output);
  }

  for (auto& otherFile : scope->otherFileScopes) {
    internalFindAllFunctionsInScope(otherFile, name, false, output);
  }
}

std::vector<astFunction*> astFindAllFunctionsInScope(astScope* scope,
                                                     const std::string& name,
                                                     bool recurse) {
  std::vector<astFunction*> functions;
  internalFindAllFunctionsInScope(scope, name, recurse, functions);
  return functions;
}

parsed::Function* astFindTemplatedFunctionInScope(astScope* scope,
                                                  const std::string& name,
                                                  bool recurse,
                                                  astScope** outputScope) {
  auto iter = std::find_if(scope->templatedFunctions.begin(),
                           scope->templatedFunctions.end(),
                           [name](const parsed::Function& function) {
                             return function.name == name;
                           });
  if (iter != scope->templatedFunctions.end()) {
    *outputScope = scope;
    return &(*iter);
  } else {
    if (!recurse) {
      return nullptr;
    }
    if (scope->parent && functionMayPenetrateScope(scope)) {
      return astFindTemplatedFunctionInScope(scope->parent, name, recurse,
                                             outputScope);
    } else {
      for (auto& otherFile : scope->otherFileScopes) {
        parsed::Function* function = astFindTemplatedFunctionInScope(
            otherFile, name, false, outputScope);
        if (function) {
          return function;
        }
      }
      return nullptr;
    }
  }
}

bool nameInvalid(astTypeName name) {
  return name == astTypeName::Unresolved || name == astTypeName::TypeAlias;
}

bool astTypesMatch(astType* left, astType* right) {
  if (nameInvalid(left->name) || nameInvalid(right->name)) {
    ASSERT(false);
    return false;
  }

  if (left->name != right->name) {
    return false;
  }
  if (left->name == astTypeName::Class && right->name == astTypeName::Class) {
    if (left->aclass->name != right->aclass->name) {
      return false;
    }

    // FOR TEMPLATES ONLY
    if (left->aclass->templateArguments.size() !=
        right->aclass->templateArguments.size()) {
      return false;
    }
    for (int i = 0; i < (int)left->aclass->templateArguments.size(); i++) {
      if (!astTypesMatch(left->aclass->templateArguments.at(0),
                         right->aclass->templateArguments.at(0))) {
        return false;
      }
    }

    return true;
  } else if (left->name == astTypeName::Array) {
    return astTypesMatch(left->getSubtypes().at(0), right->getSubtypes().at(0));
  }
  return true;
}

bool astTypesMatchSkipReferences(astType* left, astType* right) {
  return astTypesMatch(removeReference(left), removeReference(right));
}

astTypeAlias* astFindTypeAlias(astScope* scope,
                               const std::string& name,
                               bool recurse) {
  auto iter = std::find_if(
      scope->typeAliases.begin(), scope->typeAliases.end(),
      [name](const astTypeAlias* alias) { return alias->name == name; });
  if (iter != scope->typeAliases.end()) {
    return *iter;
  } else {
    if (!recurse) {
      return nullptr;
    }
    if (scope->parent) {
      return astFindTypeAlias(scope->parent, name);
    } else {
      for (auto& otherFile : scope->otherFileScopes) {
        astTypeAlias* alias = astFindTypeAlias(otherFile, name, false);
        if (alias) {
          return alias;
        }
      }
      return nullptr;
    }
  }
}

astType* astGetArrayConcreteType(astType* type) {
  ASSERT(type->name == astTypeName::Array);
  astType* subtype = type->getSubtypes().at(0);
  if (subtype->name == astTypeName::Array) {
    return astGetArrayConcreteType(subtype);
  } else {
    return subtype;
  }
}

void astType::addSubtype(astType* type, bool allowResolvedType) {
  if (allowResolvedType == false) {
    ASSERT(type->willBeResolved == false);
  }
  mSubtypes.push_back(type);
}

astClass* astType::getClass() {
  if (aclass) {
    ASSERT(arrayClass == nullptr);
    return aclass;
  }
  if (arrayClass) {
    ASSERT(aclass == nullptr);
    return arrayClass;
  }
  return nullptr;
}

astType* astType::COPY() const {
  ASSERT(!nameInvalid(name));
  astType* dst = astNewType(nullptr, location, string, scope);
  dst->name = name;
  dst->aclass = aclass;
  dst->arrayClass = arrayClass;
  dst->willBeResolved = willBeResolved;
  dst->type_error_supressed = type_error_supressed;
  for (auto& subtype : getSubtypes()) {
    // Since we are doing copies, the types may be resolved, so allow them!
    bool allowResolvedTypes = true;
    dst->addSubtype(subtype->COPY(), allowResolvedTypes);
  }
  return dst;
}

astVariableRef2* COPY_VARIABLE_REF(astVariableRef2* ref) {
  ASSERT(ref->variable);
  astVariableRef2* dst = new astVariableRef2();
  dst->location = ref->location;
  dst->scope = ref->scope;
  dst->stringVariable = ref->stringVariable;
  dst->variable = ref->variable;
  return dst;
}

astExpression* COPY_EXPRESSION_INTERNAL(astExpression* expression);

astFunctionCall2* COPY_FUNCTION_CALL(astFunctionCall2* call) {
  astFunctionCall2* dst = new astFunctionCall2();
  dst->location = call->location;
  dst->scope = call->scope;
  dst->function = call->function;
  dst->passThisContext = call->passThisContext;
  for (auto& argument : call->arguments) {
    dst->arguments.push_back(COPY_EXPRESSION_INTERNAL(argument));
  }
  for (auto& templateArgument : call->templateArguments) {
    dst->templateArguments.push_back(templateArgument->COPY());
  }
  return dst;
}

astLiteral* COPY_CONSTANT(astLiteral* constant) {
  astLiteral* dst = new astLiteral();
  dst->location = constant->location;
  dst->type = constant->type->COPY();
  dst->valueNumeric = constant->valueNumeric;
  dst->valueString = constant->valueString;
  dst->globalStringIndex = constant->globalStringIndex;
  return dst;
}

astSymbol* COPY_SYMBOL(astSymbol* symbol) {
  astSymbol* dst = nullptr;

  if (symbol->variable2) {
    dst = new astSymbol(COPY_VARIABLE_REF(symbol->variable2));
  } else if (symbol->functionCall2) {
    dst = new astSymbol(COPY_FUNCTION_CALL(symbol->functionCall2));
  } else if (symbol->constant) {
    dst = new astSymbol(COPY_CONSTANT(symbol->constant));
  } else {
    ASSERT(false);
  }

  dst->string = symbol->string;
  dst->scope = symbol->scope;

  return dst;
}

astExpressionNode* COPY_EXPRESSION_NODE(astExpressionNode* node) {
  if (astSymbol* symbol = node->getSymbol()) {
    return new astExpressionNode(COPY_SYMBOL(symbol));
  } else if (astOperatorInstance* op = node->getOperator()) {
    return new astExpressionNode(*op);
  } else if (astExpression* subExpression = node->getSubExpression()) {
    return new astExpressionNode(COPY_EXPRESSION_INTERNAL(subExpression));
  } else {
    ASSERT(false);
    return nullptr;
  }
}

astExpression* COPY_EXPRESSION_INTERNAL(astExpression* expression) {
  ASSERT(expression->flatType);  // ??
  astExpression* dst =
      new astExpression(expression->location, expression->flatType->COPY());

  for (astExpressionNode* node : expression->nodes) {
    dst->nodes.push_back(COPY_EXPRESSION_NODE(node));
  }

  return dst;
}

astExpression* COPY_EXPRESSION(astExpression* expression) {
  return COPY_EXPRESSION_INTERNAL(expression);
}

astVariable* COPY_VARIABLE(astVariable* var) {
  astVariable* dst = new astVariable();
  dst->location = var->location;
  dst->name = var->name;
  dst->global = var->global;
  dst->memberOf = var->memberOf;
  dst->type = var->type->COPY();

  // TODO: Do we need a copy??
  dst->initializeWithExpression = nullptr;

  return dst;
}

void sanityCheckTypes(astType* left, astType* right) {
  ASSERT(astTypesMatch(left, right));
  ASSERT(left == right);
}

astSymbol* astWrapVariableInSymbol(astVariable* variable,
                                   astScope* scope,
                                   bool resolved) {
  astVariableRef2* reference = new astVariableRef2();
  reference->location = variable->location;
  reference->scope = scope;
  reference->stringVariable = variable->name;
  if (resolved) {
    reference->variable = variable;
  }
  return new astSymbol(reference);
}

astExpression* astWrapSymbolInExpression(astSymbol* symbol, bool nullType) {
  astType* type = nullType ? nullptr : symbol->getSymbolType();
  astExpression* expression = new astExpression(symbol->location, type);
  expression->nodes.push_back(new astExpressionNode(symbol));
  return expression;
}

astType* astWrapClassInType(astClass* aclass, astLocation location) {
  astType* type = astNewType(nullptr, location, aclass->name, nullptr);
  type->name = astTypeName::Class;
  type->aclass = aclass;
  return type;
}

astType* astWrapTypeInArray(astType* subtype,
                            astClass* arrayClass,
                            astLocation location) {
  astType* type = astNewType(nullptr, location, "[]", nullptr);
  type->name = astTypeName::Array;
  type->arrayClass = arrayClass;
  type->addSubtype(subtype, true);
  return type;
}

astType* astWrapTypeInMutableReference(astType* type, astLocation location) {
  astType* dst = astNewType(nullptr, location, "&", nullptr);
  dst->name = astTypeName::Reference;
  dst->addSubtype(type, true);
  dst->createMutableReference();
  return dst;
}

astType* astExpressionNode::getNodeType() {
  if (getTypeCast()) {
    return getTypeCast();
  }
  if (astSymbol* symbol = getSymbol()) {
    ASSERT(symbol->getSymbolType());
    return symbol->getSymbolType();
  } else if (astExpression* subExpression = getSubExpression()) {
    ASSERT(subExpression->flatType);
    return subExpression->flatType;
  }
  ASSERT(false);
  return nullptr;
}

astLocation astExpressionNode::getLocation() {
  if (astSymbol* symbol = getSymbol()) {
    symbol->location.sanity();
    return symbol->location;
  } else if (astExpression* subExpression = getSubExpression()) {
    astLocation location = subExpression->location;
    location.sanity();
    return location;
  }
  ASSERT(false);
  return astLocation();
}

parsed::ExpressionNode::ExpressionNode(astOperatorInstance op) : mOperator(op) {
  op.location.sanity();
}
parsed::ExpressionNode::ExpressionNode(const parsed::Symbol& symbol)
    : mSymbol(symbol) {
  // TODO location sanity
}
parsed::ExpressionNode::ExpressionNode(const parsed::Expression& expression)
    : mExpression(expression) {
  expression.location.sanity();
}

astLocation parsed::Symbol::getLocation() {
  if (auto ref = getVariableRef()) {
    ref->location.sanity();
    return ref->location;
  } else if (auto call = getFunctionCall()) {
    call->location.sanity();
    return call->location;
  } else if (auto constant = getConstant()) {
    constant->location.sanity();
    return constant->location;
  } else {
    ASSERT(false);
  }
}

astLocation parsed::ExpressionNode::getLocation() {
  if (isOperator()) {
    getOperator().location.sanity();
    return getOperator().location;
  } else if (isSymbol()) {
    getSymbol().getLocation().sanity();
    return getSymbol().getLocation();
  } else if (isSubExpression()) {
    getSubExpression().location.sanity();
    return getSubExpression().location;
  } else {
    ASSERT(false);
  }
}

bool astVariable::thisContext() {
  if (memberOf) {
    return memberOf->scope->thisVariable == this;
  } else {
    return false;
  }
}
