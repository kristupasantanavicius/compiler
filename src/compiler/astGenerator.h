#pragma once

#include <compiler/AST.h>

// Template instantiation??
astClass* astGenerateClass(parsed::Class& aclass,
                           astScope* scope,
                           std::vector<std::string>& globalStrings,
                           std::vector<astType*>& outputTypes,
                           std::vector<astClass*>& newClasses,
                           std::unordered_set<astType*>& types_without_error);

astFunction* astGenerateFunction(
    parsed::Function& function,
    astScope* scope,
    astClass* memberOf,
    std::vector<std::string>& globalStrings,
    std::vector<astType*>& outputTypes,
    std::vector<astClass*>& newClasses,
    std::unordered_set<astType*>& types_without_error);

SyntaxTree* generateAst(const std::string& module_name,
                        ASTCache& cache,
                        parsed::Scope& scope);
