#!/bin/bash

if [ ! -d "build" ]; then
	mkdir build
fi

pushd build
	cmake ..
	make -j4
	# valgrind --leak-check=yes ./compiler "/home/kristupas/Desktop/compiler/shait/Input.vns" "/home/kristupas/Desktop/compiler/build/binary"
	catchsegv ./compiler "/home/kristupas/Desktop/compiler/shait/Input.vns" "/home/kristupas/Desktop/compiler/build/binary"
popd
