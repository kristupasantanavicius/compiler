BUILD (Windows):
- Install Visual Studio 2019, CMake
cd compiler
mkdir build
cd build
cmake -T ClangCL ..

vns (random code name) is supposed to be some kind of mix between Python and
C++. Python code has an elegant and simple syntax, yet manages to be extremely
powerful. But Python has a big weakness: the code is very difficult to scale due
to the nature of its semantics. vns aims to create a compiled language with
elegant and simple syntax, but unlike Python it will have extensive compile time
checking.

Currently, compiler outputs C code which has to be compiled further to get a
working program. This was done to cut down the number of things that have to be
implemented before having a working program.

Compiler C++ source code can be found under /src

vns source code can be found unter /test

The code has some unwanted shortcuts, such as missing memory management
(its a short lived program, so all of the memory is reclaimed on exit). This is
done to speed reduce time taken to implement new features.

Specific low level parts of the code might appear weird in some places, but the
general architecture is quite robust and resists buggy changes being made.
