#!/bin/bash

FILES="OrderDependency
Template
TypeAlias"

if [ ! -d "binary" ]; then
	mkdir binary
fi

echo ""

for FILE in ${FILES}
do
	../build/Debug/compiler "$FILE.vns" "binary/$FILE"
	echo ""
done
# 
