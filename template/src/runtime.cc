#include "binary.h"

#include <iostream>

extern "C" {

void Log_runtime_printString(struct Array_char string) {
  for (int i = 0; i < string.size; i++) {
    std::cout << (char)string.pointer[i];
  }
  std::cout << std::endl;
}

}
